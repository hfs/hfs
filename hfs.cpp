//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.




/*
This program calculates hyperfine levels and allowed tfransitions between two states.
It also gives the corresponding transition freqeuencies and intensities. A user can change the
upper and lower spin values on the fly as well as A and B, upper and lower, parameters in addition
to FWHM of individual peaks. The main hyperfine code from Bradley Cheal of University of Manchester, nuclear group

TODO:
Add a possibility to add ranges for the fit parameters
Add a better file parsing system, possibly a separate dialog

Cleanup code
    Move session management to a separate class
    Move data saving and picture saving to a separate class


Add voig and gaussian functions
Chi-square landscape

FIX:
session management


*/


#include "hfs.h"
#include "ui_hfs.h"

HFS::HFS(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HFS)
{
    ui->setupUi(this);

    //set test for QWT textboxes
    ui->JUl->setText("J upper");
    ui->JLl->setText("J lower");
    ui->I->setText("Nuclear spin");
    ui->Al->setText("A lower");
    ui->Au->setText("A upper");
    ui->Bl->setText("B lower");
    ui->Bu->setText("B upper");
    ui->widthL->setText("Width");
    ui->width2L->setText("Width2");
    ui->offsetl->setText("Offset");
    ui->intensityscalerll->setText("intensity scaling");
    ui->profileType->setText("Spectrum Profile");
    //setup profile selector Combo box
    ui->profileSelector->insertItem(0,"Lorentzian");
    ui->profileSelector->insertItem(1,"Gaussian");
    ui->profileSelector->insertItem(2,"Voigt");

    //Set scales for the sliders A an B sliders
    ui->Als->setRange(-10000, 10000);
    ui->Aus->setRange(-10000, 10000);
    ui->Bls->setRange(-10000, 10000);
    ui->Bus->setRange(-10000, 10000);
    //set start values for the sliders
    ui->Als->setValue(1);
    ui->Aus->setValue(1);
    ui->Bls->setValue(1);
    ui->Bus->setValue(1);
    //scale for the horizontal slider

    //set-up the parameter table
    HFSmodel = new setTable(this,ui->parat);

    //setup the main HFS plot
    HFS_plot = new plotSet(ui->HFS_plot);
    ui->HFS_plot->setAxisAutoScale(QwtPlot::xBottom);
    ui->HFS_plot->setAxisAutoScale(QwtPlot::yLeft);
    HFS_plot->addZoomAndPan();

    //setup the fitting plot
    fittingPlot = new plotSet(ui->fitPlot);
    ui->fitPlot->enableAxis(QwtPlot::xBottom,false);
    ui->fitPlot->setAxisAutoScale(QwtPlot::yLeft);

    //Setup residual plot
    residualPlot = new plotSet(ui->residualPlot);
    ui->residualPlot->setAxisAutoScale(QwtPlot::yLeft);
    //ui->residualPlot->setMargin(0);

    //set the qwtslider step size to 1
    ui->Als->setStep(1);
    ui->Bls->setStep(1);
    ui->Aus->setStep(1);
    ui->Bus->setStep(1);

    //setup the king plot data table
    kingPlotTable= new kingPlotDataModel(this, ui->dataTable);
    ui->dataTable->setModel(kingPlotTable);


    //Setup the session class, to be moved to its own function as it is supposed to be performed for every instance

    //currentHfsSession->onUispinSpinBoxClicked();
    currentSession="default";
    ui->currentSession->setText(currentSession);
    connectHfsSessionSlots();
    currentFittingSession=NULL;
    fittingSession =NULL;
    createSession();
    currentFittingSession=sessionStorage.value(currentSession);
    disConnectHfsSessionSlots();
    ui->currentSession->setText(currentSession);
    connectHfsSessionSlots();
    disConnectHfsSessionSlots();
    currentFittingSession->getUiData();
    connectHfsSessionSlots();
    currentFittingSession->onUispinSpinBoxClicked();
    onProfileChanged("Lorentzian");




    ////////////////////////////////
    connect(ui->save, SIGNAL(clicked()), this, SLOT(createSession()));
    connect(ui->sessions, SIGNAL(clicked(QModelIndex)), this, SLOT(restoreSession(QModelIndex)));

    //king plot datatable connections
    connect(ui->addRow, SIGNAL(clicked()),kingPlotTable, SLOT(addRow()));
    connect(ui->deleteRow, SIGNAL(clicked()),kingPlotTable, SLOT(deleteRow()));

    //setup the king plot Plot
    kingPlot = new kingPlotData(this, ui->kingPlot);
    setNucleiData();
    connect(kingPlotTable, SIGNAL(sendDataToPlot(QVector<double>,QVector<double>,QVector<double>,QVector<double>,QVector<double>,QVector<double>)),
            kingPlot, SLOT(setOriginalData(QVector<double>,QVector<double>,QVector<double>,QVector<double>,QVector<double>,QVector<double>))); //Connections sends data from the table to the plot

    //connect fit king plot button to the fitting slot in kingplotfata
    connect(ui->fitKingPlot, SIGNAL(clicked()), kingPlot, SLOT(fitKingPlot()));

    //connect signal from kingplot instance which emits a qstring
    connect(kingPlot, SIGNAL(sendFitOutPut(QString)), this, SLOT(setFitOutPut(QString)));

    //add  two first rows to king plot table in order to get a nice plot in the beginning
    kingPlotTable->addRow(1,2,1,4,5,1);
    kingPlotTable->addRow(7,8,1,10,11,1);
    kingPlot->fitKingPlot();

    //Connect load data button to an appropriate slot int the kingplottable instance
    connect(ui->loadData,SIGNAL(clicked()),kingPlotTable, SLOT(loadData()));

    //connections for if the nuclei data values are changed, refit and plot the king plot
    connect(ui->zValue,SIGNAL(valueChanged(double)),this,SLOT(setNucleiData()));
    connect(ui->refA1,SIGNAL(valueChanged(double)),this,SLOT(setNucleiData()));
    connect(ui->refA2,SIGNAL(valueChanged(double)),this,SLOT(setNucleiData()));
    connect(ui->modifyX,SIGNAL(clicked()),this, SLOT(setNucleiData()));
    connect(ui->modifyY,SIGNAL(clicked()),this, SLOT(setNucleiData()));

    //update the centroid list
    connect(ui->updateList,SIGNAL(clicked()),this,SLOT(returnCentroids()));

}


HFS::~HFS(){
    delete ui;
    delete kingPlot;
    foreach(session *oldsession, sessionStorage){
        if(oldsession){
            QFile::remove(oldsession->returnSessionPath());
            delete oldsession;
        }
    }
}

void HFS::connectHfsSessionSlots(){
    //Connect the ui signals to appropriate slots
    //If the spins of the system change, the whole HFS needs to be recalculated
    connect(ui->JLc, SIGNAL(valueChanged(double)), this, SLOT(onUispinSpinBoxClicked()));
    connect(ui->JUc, SIGNAL(valueChanged(double)), this, SLOT(onUispinSpinBoxClicked()));
    connect(ui->Ival, SIGNAL(valueChanged(double)), this, SLOT(onUispinSpinBoxClicked()));
    // If only the spactrum values change, only the shape and frequncies need to be recalculated
    connect(ui->width, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->width2, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->offset, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->bgOffset,SIGNAL(valueChanged(double)),this,SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->intensityscalerl, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->Alc, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->Blc, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->Auc, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->Buc, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->alauval, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->scaleA, SIGNAL(clicked()), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->blbuval, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->scaleB, SIGNAL(clicked()), this, SLOT(onUiOtherSpectrumValueChanged()));
    //Connect the open scan menu entry to onSpectrumloaded slot
    connect(ui->actionOpen_scan,SIGNAL(triggered()),this, SLOT(onSpectrumLoaded()));
    //Clear spectrum
    connect(ui->clearSpectrum, SIGNAL(clicked()), this, SLOT(onSpectrumCleared()));
    //When the show individual lreontzian button is toggled
    connect(ui->inPeaks, SIGNAL(toggled(bool)), this, SLOT(onShowIndividualLorentzianClicked(bool)));

    //Fitter connections
    connect(ui->fitbutton, SIGNAL(clicked()), this, SLOT(onFitButtonClicked()));
    connect(ui->resetButton, SIGNAL(clicked()),this, SLOT(onResetButtonClicked()));

    //Connect the fitresult spinbonbox changes to a redrawslot in the this class
    connect(ui->X0, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    connect(ui->Y0, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    connect(ui->AlFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    connect(ui->BlFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    connect(ui->AuFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    connect(ui->BuFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    connect(ui->wFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    connect(ui->IFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    connect(ui->w2Fit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    //connect the lockbutton clicks to the onlockbutonclicked slot
    connect(ui->X0Lock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    connect(ui->Y0Lock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    connect(ui->AlLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    connect(ui->BlLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    connect(ui->AuLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    connect(ui->BuLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    connect(ui->wLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    connect(ui->Ilock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    connect(ui->w2Lock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    //connect the change of the spectrum profile
    connect(ui->profileSelector,SIGNAL(currentIndexChanged(int)),this,SLOT(onUiOtherSpectrumValueChanged()));
    connect(ui->profileSelector,SIGNAL(currentIndexChanged(QString)),ui->fitterType,SLOT(setText(QString)));
    connect(ui->profileSelector,SIGNAL(currentIndexChanged(QString)),this, SLOT(onProfileChanged(QString)));
    //connect the fitbutton to the profileselector so that starting fitter disables the profileselector
    connect(ui->fitbutton,SIGNAL(clicked(bool)),ui->profileSelector,SLOT(setEnabled(bool)));
    connect(ui->resetButton,SIGNAL(clicked(bool)),ui->profileSelector,SLOT(setDisabled(bool)));
}

void HFS::disConnectHfsSessionSlots(){
    //Connect the ui signals to appropriate slots
    //If the spins of the system change, the whole HFS needs to be recalculated
    //Connect the ui signals to appropriate slots
    //If the spins of the system change, the whole HFS needs to be recalculated
    disconnect(ui->JLc, SIGNAL(valueChanged(double)), this, SLOT(onUispinSpinBoxClicked()));
    disconnect(ui->JUc, SIGNAL(valueChanged(double)), this, SLOT(onUispinSpinBoxClicked()));
    disconnect(ui->Ival, SIGNAL(valueChanged(double)), this, SLOT(onUispinSpinBoxClicked()));
    // If only the spactrum values change, only the shape and frequncies need to be recalculated
    disconnect(ui->width, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->width2, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->offset, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->bgOffset,SIGNAL(valueChanged(double)),this,SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->intensityscalerl, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->Alc, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->Blc, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->Auc, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->Buc, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->alauval, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->scaleA, SIGNAL(clicked()), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->blbuval, SIGNAL(valueChanged(double)), this, SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->scaleB, SIGNAL(clicked()), this, SLOT(onUiOtherSpectrumValueChanged()));
    //disconnect the open scan menu entry to onSpectrumloaded slot
    disconnect(ui->actionOpen_scan,SIGNAL(triggered()),this, SLOT(onSpectrumLoaded()));
    //Clear spectrum
    disconnect(ui->clearSpectrum, SIGNAL(clicked()), this, SLOT(onSpectrumCleared()));
    //When the show individual lreontzian button is toggled
    disconnect(ui->inPeaks, SIGNAL(toggled(bool)), this, SLOT(onShowIndividualLorentzianClicked(bool)));

    //Fitter disconnections
    disconnect(ui->fitbutton, SIGNAL(clicked()), this, SLOT(onFitButtonClicked()));
    disconnect(ui->resetButton, SIGNAL(clicked()),this, SLOT(onResetButtonClicked()));

    //disconnect the fitresult spinbonbox changes to a redrawslot in the this class
    disconnect(ui->X0, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    disconnect(ui->Y0, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    disconnect(ui->AlFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    disconnect(ui->BlFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    disconnect(ui->AuFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    disconnect(ui->BuFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    disconnect(ui->wFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    disconnect(ui->IFit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    disconnect(ui->w2Fit, SIGNAL(valueChanged(double)), this, SLOT(onFitResultValueChanged()));
    //connect the lockbutton clicks to the onlockbutonclicked slot
    disconnect(ui->X0Lock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    disconnect(ui->Y0Lock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    disconnect(ui->AlLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    disconnect(ui->BlLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    disconnect(ui->AuLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    disconnect(ui->BuLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    disconnect(ui->wLock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    disconnect(ui->Ilock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    disconnect(ui->w2Lock, SIGNAL(toggled(bool)), this, SLOT(onLockButtonClicked()));
    //disconnect the change of the spectrum profile
    disconnect(ui->profileSelector,SIGNAL(currentIndexChanged(int)),this,SLOT(onUiOtherSpectrumValueChanged()));
    disconnect(ui->profileSelector,SIGNAL(currentIndexChanged(QString)),ui->fitterType,SLOT(setText(QString)));
    disconnect(ui->profileSelector,SIGNAL(currentIndexChanged(QString)),this, SLOT(onProfileChanged(QString)));
    //starting fitter disables the profileselector
    disconnect(ui->fitbutton,SIGNAL(clicked(bool)),ui->profileSelector,SLOT(setEnabled(bool)));
    disconnect(ui->resetButton,SIGNAL(clicked(bool)),ui->profileSelector,SLOT(setDisabled(bool)));


}

//set the slider values to the counters
void HFS::on_Aus_sliderMoved(double value){
    ui->Auc->setValue(value);
    if(ui->scaleA->isChecked()){                       //make the Al slider value the same as the Au if the ratio radiobutton is pressed
        alauval=ui->alauval->value();
        if(alauval!=0){
            ui->Als->setValue(value/alauval);
            ui->Alc->setValue(value/alauval);
        }

    }
}

void HFS::on_Bus_sliderMoved(double value){
    ui->Buc->setValue(value);
    if(ui->scaleB->isChecked()){                       //make the Bl slider value the same as the Bu if the ratio radiobutton is pressed
        blbuval=ui->blbuval->value();
        if(blbuval!=0){
            ui->Bls->setValue(value/blbuval);
            ui->Blc->setValue(value/blbuval);
        }
    }
}


void HFS::on_Als_sliderMoved(double value){
    ui->Alc->setValue(value);
}

void HFS::on_Bls_sliderMoved(double value){
    ui->Blc->setValue(value);
}


void HFS::on_scaleA_toggled(bool checked){
    if(checked){
        alau=true;
        HFS::on_Aus_sliderMoved(ui->Aus->value());
    }
    else{
        alau=false;
    }
}

void HFS::on_scaleB_toggled(bool checked){
    if(checked){
        blbu=true;
        HFS::on_Bus_sliderMoved(ui->Bus->value());
    }
    else{
        blbu=false;
    }
}

//if the scaling value is changed, change he au and bu slider
void HFS::on_alauval_valueChanged(double value){
    HFS::on_Aus_sliderMoved(ui->Aus->value());
}

void HFS::on_blbuval_valueChanged(double value){
    HFS::on_Bus_sliderMoved(ui->Bus->value());
}


//Edit slots for the slider spinboxes so that when editing these the calculations are redone
void HFS::on_Auc_valueChanged(double value){
    HFS::on_Aus_sliderMoved(value);
    ui->Aus->setValue(value);
}

void HFS::on_Buc_valueChanged(double value){
    HFS::on_Bus_sliderMoved(value);
    ui->Bus->setValue(value);
}

void HFS::on_Alc_valueChanged(double value){
    ui->Als->setValue(value);
}

void HFS::on_Blc_valueChanged(double value){
    ui->Bls->setValue(value);

}



////enable copy to clipboard, causes some slowdown at the moment

void HFS::keyPressEvent(QKeyEvent *event ){
    if(event->key()==Qt::Key_Escape){
        ui->HFS_plot->setAxisAutoScale(QwtPlot::xBottom);
        ui->HFS_plot->setAxisAutoScale(QwtPlot::yLeft);
        ui->HFS_plot->replot();
    }
    if (event->matches(QKeySequence::Copy)) {
        HFSmodel->copyToCP();
    }
    if(event->key()==Qt::Key_Delete){
        if(ui->Sessionidentifier->text()!=""){
        deleteSession(ui->sessions->currentItem());
        ui->sessions->clearSelection();
        }
    }
}


//Session management slots

//Creates if no session with same name present, otherwise overwrites the existing one
void HFS::createSession(){
    QString sessionName;
    sessionName = ui->Sessionidentifier->text();

    if(sessionName==""){
        sessionName.append("default");

    }

    if(!sessionStorage.contains(sessionName) && !sessionName.isEmpty()){
        //qDebug()<<!sessionStorage.contains(sessionName);
        disConnectHfsSessionSlots();
        fittingSession= new session(this, ui, sessionName);
        fittingSession->setResultTable(HFSmodel);
        fittingSession->setPlots(HFS_plot, fittingPlot, residualPlot);
        fittingSession->setStartUpValues();
        fittingSession->writeSettings();
        fittingSession->writeArrayData();
        fittingSession->calculateFullHFSspectrum();

        connectHfsSessionSlots();

        //fittingSession->setStartUpValues();
        sessionStorage.insert(fittingSession->sessionName, fittingSession);
        //currentHfsSession=sessionStorage.value(sessionName);
        //        currentHfsSession->restoreUiData();
        //sessionStorage.value(sessionName)->onUispinSpinBoxClicked();
        fittingSession=NULL;

        ui->sessions->addItem(sessionName);
    }
    ui->sessions->selectionModel()->clear();
    ui->Sessionidentifier->clear();





}

void HFS::restoreSession(QModelIndex itemIndex ){
    QString selectedSession;
    disConnectHfsSessionSlots();
    //qDebug() << itemIndex.data(Qt::DisplayRole);
    selectedSession=ui->sessions->item(itemIndex.row())->text();
    if(currentSession==selectedSession){
        currentFittingSession->saveSession();
    }
    else{

         currentFittingSession->exitSession();
        currentFittingSession=NULL;
        currentFittingSession=sessionStorage.value(selectedSession);
        currentFittingSession->restoreSession();

        currentFittingSession->getUiData();
        currentSession=selectedSession;
        ui->currentSession->setText(currentSession);


        currentFittingSession->onUispinSpinBoxClicked();

    }

    connectHfsSessionSlots();
    //ui->sessions->selectionModel()->clear();

}

void HFS::renameSession(QListWidgetItem * item ){

}

void HFS::deleteSession(QListWidgetItem *item){

    QString selectedSession;
    selectedSession=item->text();
    //currentHfsSession=sessionStorage.value("default");
    if(sessionStorage.size()>=2){
        delete sessionStorage.value(selectedSession);

        sessionStorage.remove(selectedSession);
        delete item;
    }



}


//Slots for controlling the actibe hfsSession instance

void HFS::onUispinSpinBoxClicked(){ //This gets called when either the jl, ju or nuclear spin value changes
    currentFittingSession->onUispinSpinBoxClicked();
}
void HFS::onUiOtherSpectrumValueChanged(){ // This gets called otherwise
    currentFittingSession->onUiOtherSpectrumValueChanged();
 }
void HFS::onSpectrumLoaded(){ //loads and plot an experimental spectrum
    currentFittingSession->onSpectrumLoaded();
}
void HFS::onSpectrumCleared(){ //Clears the spectrum
    currentFittingSession->onSpectrumCleared();
}

void HFS::onShowIndividualLorentzianClicked(bool state){ //Shows and ides individual lorntzian peaks
    currentFittingSession->onShowIndividualLorentzianClicked(state);
}

//Fitting related slots
void HFS::onFitButtonClicked(){
    currentFittingSession->onFitButtonClicked();
}

void HFS::onResetButtonClicked(){
    currentFittingSession->onResetButtonClicked();
}

void HFS::onFitResultValueChanged(){
    currentFittingSession->onFitResultValueChanged();

}

void HFS::onLockButtonClicked(){
    currentFittingSession->onLockButtonClicked();
}

void HFS::onProfileChanged(QString selectedProfile){
    if(selectedProfile=="Lorentzian"){
        ui->widthL->setText("Lorentzian FWHM");
        ui->width2L->setText("----");
        ui->width2->setDisabled(true);

        ui->w2Fit->setDisabled(true);
        ui->w2Lock->setDisabled(true);
        ui->wLock->setText("wl");


    }
    else if(selectedProfile=="Gaussian"){
        ui->widthL->setText("Gaussian std");
        ui->width2L->setText("----");
        ui->width2->setDisabled(true);

        ui->w2Fit->setDisabled(true);
        ui->w2Lock->setDisabled(true);
        ui->wLock->setText("wg");
    }
    else if(selectedProfile=="Voigt"){
        ui->widthL->setText("Gaussian FWHM");
        ui->width2L->setText("Lorentzian FWHM");
        ui->width2->setDisabled(false);

        ui->w2Fit->setEnabled(true);
        ui->w2Lock->setEnabled(true);
        ui->w2Lock->setText("wg");
        ui->w2Fit->setEnabled(true);
        ui->wLock->setText("wl");
    }
}


//set Deming regression data to the ui box

void HFS::setFitOutPut(QString data){
    ui->kingPlotResults->append(data);
}
//send nuclei data to the king plot data class instance
void HFS::setNucleiData(){
    double Z=static_cast<int>(ui->zValue->value());
    double A1=static_cast<int>(ui->refA1->value());
    double A2=static_cast<int>(ui->refA2->value());
    bool modifyX=ui->modifyX->isChecked();
    bool modifyY=ui->modifyY->isChecked();
    kingPlot->setNucleiData(Z, A1 ,A2, modifyX, modifyY);
}
