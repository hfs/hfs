//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



#include "voigtfitfunctions.h"

voigtFitFunctions::voigtFitFunctions()
{

}
//The function calculates the center of gravity X0
//double xcz_fVal= x-offset, double Al_fVal= lower A, double Bl_fVal=lowerB,
//double Au_fVal=upper A, double Bu_fVal=upper B,
//double all_fVal= lower alpha (constant), double bel_fVal=lower beta (constant),
//double alu_fVal= upper alpha (constant),double beu_fVal= upper beta (constant)
double voigtFitFunctions::hfsCenterOfGravity(double xcz_fVal, double Al_fVal, double Bl_fVal, double Au_fVal, double Bu_fVal, double all_fVal, double bel_fVal, double alu_fVal, double beu_fVal){
    double cent=xcz_fVal+alu_fVal*Au_fVal+beu_fVal*Bu_fVal-all_fVal*Al_fVal-bel_fVal*Bl_fVal;
    return cent;
}

//The function calculates a point in  a Voigt profile for a single peak
//double lorentzianAmplitude= ampltiude of the peak (this is not the absolute amplitude),
//double peakPosition = peak centroid,
//double lorentzianFWHM=wl, double gaussianFWHM=wg,
//double x= the point wheret the function is evaluated,
//double yOffset=Y
double voigtFitFunctions::voigtFunction(double lorentzianAmplitude, double peakPosition, double lorentzianFWHM, double gaussianFWHM, double x, double yOffset){
    int i;
    double A[4],B[4],C[4],D[4],V=0;
    double t=sqrt(M_LN2);
    static double sqrtln2=sqrt(M_LN2);
    static double sqrtpi=sqrt(M_PI);
    double X=(x-peakPosition)*2*sqrtln2/gaussianFWHM;
    double Y=lorentzianFWHM*sqrtln2/gaussianFWHM;
    A[0]=-1.2150; B[0]= 1.2359;
    A[1]=-1.3509; B[1]= 0.3786;
    A[2]=-1.2150; B[2]=-1.2359;
    A[3]=-1.3509; B[3]=-0.3786;
    C[0]=-0.3085; D[0]= 0.0210;
    C[1]= 0.5906; D[1]=-1.1858;
    C[2]=-0.3085; D[2]=-0.0210;
    C[3]= 0.5906; D[3]= 1.1858;
    for(i=0;i <= 3;i++){
        V+=(C[i]*(Y-A[i])+D[i]*(X-B[i]))/(pow((Y-A[i]),2)+pow((X-B[i]),2));
    }
    return ((lorentzianFWHM*lorentzianAmplitude*sqrtpi*sqrtln2/gaussianFWHM)*V);

}
void voigtFitFunctions::voigtDerivatives(double t, double *a, double *y, double *dyda, int m){

    int i,j;
    static double sqrtln2=sqrt(M_LN2);
    static double sqrtpi=sqrt(M_PI);
    double A[4],B[4],C[4],D[4],alpha[4],beta[4];
    double dVdx=0.0,dVdy=0.0,V=0.0,X,Y,constant;
    A[0]=-1.2150; B[0]= 1.2359; C[0]=-0.3085; D[0]= 0.0210;
    A[1]=-1.3509; B[1]= 0.3786; C[1]= 0.5906; D[1]=-1.1858;
    A[2]=-1.2150; B[2]=-1.2359; C[2]=-0.3085; D[2]=-0.0210;
    A[3]=-1.3509; B[3]=-0.3786; C[3]= 0.5906; D[3]= 1.1858;
    y[0]=0.0;
    for(i=1;i<=m-1;i+=4)
    {
        X=(t-a[i+1])*2*sqrtln2/a[i+3];
        Y=a[i+2]*sqrtln2/a[i+3];
        constant=a[i+2]*a[i]*sqrtpi*sqrtln2/a[i+3];
        for(j=0;j<=3;j++)
        {
            alpha[j]=C[j]*(Y-A[j])+D[j]*(X-B[j]);
            beta[j]=pow((Y-A[j]),2)+pow((X-B[j]),2);
            V+=alpha[j]/beta[j];
            dVdx+=D[j]/beta[j]-2.0*(X-B[j])*alpha[j]/pow((beta[j]),2);
            dVdy+=C[j]/beta[j]-2.0*(Y-A[j])*alpha[j]/pow((beta[j]),2);
        }
        y[0]=y[0]+constant*V;
        dyda[i]=constant*V/a[i];                                        //Lorentzian amplitude
        dyda[i+1]=-constant*dVdx*2*sqrtln2/a[i+3];                      //peak position
        dyda[i+2]=constant*(V/a[i+2]+dVdy*sqrtln2/a[i+3]);              //Lorentzian FWHM
        dyda[i+3]=-constant*(V+(sqrtln2/a[i+3])*                        //Gaussian FWHM
                             (2.0*(t-a[i+1])*dVdx+a[i+2]*dVdy))/a[i+3];
    }
}

int voigtFitFunctions::voigt_f(const gsl_vector *x, void *data, gsl_vector *f){
    //get fit constants and other non-fitted parameters from the data struct
    size_t n = ((struct voigtFitFunctions::data*)data)->n;
    double *y = ((struct voigtFitFunctions::data*)data)->y;
    double *sigma = ((struct voigtFitFunctions::data*)data)->sigma;
    int peaks=((struct voigtFitFunctions::data*)data)->peaks;
    double *xdata=((struct voigtFitFunctions::data*)data)->xdata;

    //fetch fit constants
    double *alphal=((struct voigtFitFunctions::data*)data)->alphal;
    double *betal=((struct voigtFitFunctions::data*)data)->betal;
    double *alphau=((struct voigtFitFunctions::data*)data)->alphau;
    double *betau=((struct voigtFitFunctions::data*)data)->betau;
    double *I_const=((struct voigtFitFunctions::data*)data)->I;

    // shared fitparameters ar stored in a gsl_vector
    double x0= gsl_vector_get (x, 0);
    double yzero= gsl_vector_get (x, 1);
    double Al= gsl_vector_get (x, 2);
    double Bl= gsl_vector_get (x, 3);
    double Au= gsl_vector_get (x, 4);
    double Bu= gsl_vector_get (x, 5);
    double wg=gsl_vector_get (x, 6);    //gaussian width
    double I=gsl_vector_get (x, 7);
    double wl=gsl_vector_get (x, 8);    //lorenztian width

    size_t i;           //size of the data array
    //run trough the peaks, tis is the new multipeak function
    for(i=0;i<n;i++){
        double t=xdata[i];
        double Yi=0;
        for(int peak=0; peak<peaks; peak++){
            double centerOfGravity=hfsCenterOfGravity(x0, Al, Bl, Au, Bu, alphal[peak], betal[peak], alphau[peak], betau[peak]);
            Yi=Yi+voigtFunction(I_const[peak]*I,centerOfGravity,wl,wg,t,yzero);

        }
        Yi=Yi+yzero; // put the background  here rather than to individual peaks
        gsl_vector_set (f, i, (Yi - y[i])/sigma[i]);
    }
    return GSL_SUCCESS;
}


int voigtFitFunctions::voigt_fd(const gsl_vector *x, void *data, gsl_matrix *J){
    //get fit constants and other non-fitted parameters from the data struct
    size_t n = ((struct voigtFitFunctions::data*)data)->n;
    double *y = ((struct voigtFitFunctions::data*)data)->y;
    double *sigma = ((struct voigtFitFunctions::data*)data)->sigma;
    int peaks=((struct voigtFitFunctions::data*)data)->peaks;
    double *xdata=((struct voigtFitFunctions::data*)data)->xdata;

    //fetch fit constants
    double *alphal=((struct voigtFitFunctions::data*)data)->alphal;
    double *betal=((struct voigtFitFunctions::data*)data)->betal;
    double *alphau=((struct voigtFitFunctions::data*)data)->alphau;
    double *betau=((struct voigtFitFunctions::data*)data)->betau;
    double *I_const=((struct voigtFitFunctions::data*)data)->I;

    //fetch parameter locks
    bool X0Lock=((struct voigtFitFunctions::data*)data)->X0Lock;
    bool yzeroLock=((struct voigtFitFunctions::data*)data)->yzeroLock;
    bool AlLock=((struct voigtFitFunctions::data*)data)->AlLock;
    bool BlLock=((struct voigtFitFunctions::data*)data)->BlLock;
    bool AuLock=((struct voigtFitFunctions::data*)data)->AuLock;
    bool BuLock=((struct voigtFitFunctions::data*)data)->BuLock;
    bool wLock=((struct voigtFitFunctions::data*)data)->wLock;
    bool ILock=((struct voigtFitFunctions::data*)data)->ILock;
    bool w2Lock=((struct voigtFitFunctions::data*)data)->w2Lock;
    // shared fitparameters ar stored in a gsl_vector
    double x0= gsl_vector_get (x, 0);
    double yzero= gsl_vector_get (x, 1);
    //double yzero= gsl_vector_get (x, 1);
    double Al= gsl_vector_get (x, 2);
    double Bl= gsl_vector_get (x, 3);
    double Au= gsl_vector_get (x, 4);
    double Bu= gsl_vector_get (x, 5);
    double wg=gsl_vector_get (x, 6);    //gaussian width
    double I=gsl_vector_get (x, 7);
    double wl=gsl_vector_get (x, 8);    //lorentzian width

    size_t i;
    // jacobian
    for(i=0;i<n;i++){   //for each datapoint
        double t = xdata[i];

        //for each shared variable namely  xcz << yzero<< AsBs
        // there are 8 shared fit parameters
        //derivate against centroid, the function is the same for all xcz, Al, Bl, Au and Bu
        double YX0=0;           //Derivative of the x-offset
        double YyZero=0;        //Derivative of the y offset
        double Yal=0;           //Derivative of the Al
        double Ybl=0;           //Derivative of the Bl
        double Yau=0;           //Derivative of the Au
        double Ybu=0;           //Derivative of the Bu
        double Ywidth=0;        //Derivative of the gaussian width
        double Yintensity=0;    //Derivative of the intensity
        double Ywidth2=0;        //Derivative of the lorentzian width
        double Yi=0;
        for(int peak=0;peak<peaks; peak++){
            double centerOfGravity=hfsCenterOfGravity(x0, Al, Bl, Au, Bu, alphal[peak], betal[peak], alphau[peak], betau[peak]);
            int  m;
            double *a;
            double *dyda;
            double *yMagnitude;


            Yi=Yi+voigtFunction(I_const[peak]*I,centerOfGravity,wl,wg,t,yzero);

            m=4;
            a= new double[m+1];
            dyda= new double[m+1];
            yMagnitude=new double[1];
            a[1]=I;
            a[2]=centerOfGravity;
            a[3]=wl;
            a[4]=wg;
            //Voigt derivatives
            voigtDerivatives(t, a, yMagnitude, dyda, m);
            //            // as the centroid parameters, xcz, AL, Bl, Au and Bu are sha
                            //red by each voigt forming the sumfunction,
            //            // the derivate of the sumfunction is a su of the derivatives. The same goes also for the zeropiont yzero,
            //            // its just the number of peaks
            YX0=YX0+dyda[2];
            YyZero=YyZero+1;
            Yal=Yal+dyda[2]*alphal[peak]*-1;
            Ybl=Ybl+dyda[2]*betal[peak]*-1;
            Yau=Yau+dyda[2]*alphau[peak];
            Ybu=Ybu+dyda[2]*betau[peak];
            Ywidth=Ywidth+dyda[3];
            Yintensity=Yintensity+dyda[1]*I_const[peak];
            Ywidth2=Ywidth2+dyda[4];
            delete[] a;
            delete[] dyda;
            delete[] yMagnitude;

        }
        //qDebug()<<YX0<<YyZero<<Yal<<Ybl<<Yau<<Ybu<<Ywidth<<Yintensity<<Ywidth2;
        //if the derivative against the parameter is zero,
        //the fit routine thinks that the parameter is already at its optimal value.
        //If now the derivative is set to zero artificially, the value of the parameter is not changed.
        if(X0Lock){
            YX0=0;
        }

        if(yzeroLock){
            YyZero=0;
        }

        if(AlLock){
            Yal=0;
        }

        if(BlLock){
            Ybl=0;
        }

        if(AuLock){
            Yau=0;
        }

        if(BuLock){
            Ybu=0;
        }
        if(wLock){
            Ywidth=0;
        }
        if(ILock){
            Yintensity=0;
        }
        if(w2Lock){

            Ywidth2=0;
        }

        gsl_matrix_set (J, i, 0,YX0/sigma[i]); // /sigma[i]
        gsl_matrix_set (J, i, 1, YyZero/sigma[i]);
        gsl_matrix_set (J, i, 2,  Yal/sigma[i]);
        gsl_matrix_set (J, i, 3,  Ybl/sigma[i]);
        gsl_matrix_set (J, i, 4,  Yau/sigma[i]);
        gsl_matrix_set (J, i, 5,  Ybu/sigma[i]);
        gsl_matrix_set (J, i, 6,  Ywidth/sigma[i]);
        gsl_matrix_set (J, i, 7,  Yintensity/sigma[i]);
        gsl_matrix_set (J, i, 8,  Ywidth2/sigma[i]);

    }


    return GSL_SUCCESS;

}

int voigtFitFunctions::voigt_fdf(const gsl_vector *x, void *data, gsl_vector *f, gsl_matrix *J){

    voigtFitFunctions::voigt_f(x,data,f);
    voigtFitFunctions::voigt_fd(x, data, J);

    return GSL_SUCCESS;
}
