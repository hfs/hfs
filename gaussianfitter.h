#ifndef GAUSSIANFITTER_H
#define GAUSSIANFITTER_H
#include <gaussianfitfunctions.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <unordered_map>
#include <string>
#include <stdio.h>
#include <sstream>
#include <vector>
class gaussianFitter
{
public:
    gaussianFitter();
    ~gaussianFitter();
    //this struct holds the information needed for the fit
    struct data {
        size_t n;               //number of functions == number of datapoints
        double *y;              //data array, contains the data to be fitted
        double *xdata;          //the x- axis data for the calculation of the functions
        double *sigma;          //standard deviation or error of each datapoint
        int peaks;              //number of peaks
        //alphas and betas as an array
        double *alphal;
        double *betal;
        double *alphau;
        double *betau;
        //peak intensities
        double *I;
        //Boolens containing information on wheter a certain parameter is locked or not
        bool X0Lock;            //centroid lock
        bool yzeroLock;         // y-offset lock
        bool AlLock;            //AL lock
        bool BlLock;            //Bl lock
        bool AuLock;            //Au lock
        bool BuLock;            //Bu lock
        bool wLock;             //width lock
        bool ILock;             //peak height lock
    };
    data constantData;              //initialize the struct
    gaussianFitFunctions functions; //Not to be used for the fitting
    //These functions need to be run before the fitting works
    //They set the data and fit the fitconstants needed by the fitter
    //The fit constant arrays are sorted as peak 1, peak2 ...
    void setData(int numberOfDatapoints, double *yData, double *xData, double *yError, int numberOfPeaks);
    void setFitConstants(double *lowerAlphas, double *lowerBetas, double *upperAlphas, double *upperBetas, double *peakIntensities);
    void setLockedParameters(bool xOffsetLock, bool yOffsetLock, bool lowerALock, bool lowerBLock, bool upperALock, bool upperBLock, bool widthLock, bool intensityLock);

    void setInitialValues(double centerOfMass, double yOffset, double lowerA, double lowerB, double upperA, double upperB, double width, double intensity);
    void setFitterControlParameters(int numberOfIterations, double absoluteError, double relativeError);

    void setSolver();
    void iterateSolver();
    void returnSolverState (size_t iter, gsl_multifit_fdfsolver * s);
    void clearSolver();
    std::vector<double> returnProfile(double X0_val, double yOffset_val, double Al_val, double Bl_val, double Au_val, double Bu_val, double w_val, double I_val);

    //This function returns the fit results
    std::unordered_map<std::string, double> returnResult(){
        return fitResults;
    }
    //This function returns the fitter status
    std::unordered_map<std::string, std::string> returnFitterStatus(){
        return fitterStatus;
    }


    size_t p;
    size_t n;
    //fitter members
    const gsl_multifit_fdfsolver_type *T;   //Type of the optimization algorithm
    gsl_multifit_fdfsolver *s;              //The solver
    gsl_multifit_function_fdf f;            //This data type defines a general system of functions with arbitrary parameters and the corresponding Jacobian matrix of derivatives,
    gsl_matrix *covar;                      //Covariant matrix for the fit results
    gsl_vector_view x;                      //A vector that holds initially the initial values and the then fitted values
    int status;                             //The fit status
    unsigned int i, iter;                   //Fitter iteration counter

    //fitter control parameters
    int iterations;                      //maximum number of fit iterations
    double epsabs;                          //Absolute error epsabs.
    double epsrel;                          //Relative error epsrel

    //Initial values for the fitter
    double *initialValues;

    //fit results;
    std::unordered_map<std::string, double> fitResults;
    std::unordered_map<std::string, std::string> fitterStatus;
};

#endif // GAUSSIANFITTER_H
