//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.




#include "session.h"

session::session(QObject *parent, Ui_HFS *ui, QString name) :
    QObject(parent)
{
    //class constructor
    setStartUpValues();
    sessionName=name;
    userI=ui;
    hyperfineSpectrum = new HFSfunctions();
    ioOperations= new fileOperations(this, userI, resultTable);
    uiOperations =new miscFunctions(this, userI);
    mainFitter=NULL;
    firstFitterRun=true;
    profile="lorentzian";
    restored=false;
    //ui and other settings
    settings = new QSettings("HFS", sessionName, this);


    //arrays
    deAllocateArrays();
    deAllocateSpectrumArrays();

}


session::~session(){
    deleteArrays();
    deleteSpectrumArrays();
}

void session::setResultTable(setTable *table){
    resultTable=table;

}

void session::setPlots(plotSet *hfsPlot_p, plotSet *fitPlot_p, plotSet *residualPlot_p){
    hyperfinePlot=hfsPlot_p;
    fitPlot=fitPlot_p;
    residualPlot=residualPlot_p;
}

void session::setUiData(){
    jl=userI->JLc->value();
    ju=userI->JUc->value();
    nucSpin=userI->Ival->value();
    bgOffset=userI->bgOffset->value();
    centerOfGravity=userI->offset->value();
    width=userI->width->value();
    width2=userI->width2->value();
    intensity=userI->intensityscalerl->value();
    AuperAlbool=userI->scaleA->isChecked();
    AuperAl=userI->alauval->value();
    BuperBlbool=userI->scaleB->isChecked();
    BuperBl=userI->blbuval->value();
    showInidividualPeaks=userI->inPeaks->isChecked();
    IAl=userI->Alc->value();
    IBl=userI->Blc->value();
    IAu=userI->Auc->value();
    IBu=userI->Buc->value();
    profile=userI->profileSelector->currentText();

    X0Lock=userI->X0Lock->isChecked();
    X0=userI->X0->value();
    Y0Lock=userI->Y0Lock->isChecked();
    Y0=userI->Y0->value();
    AlLock=userI->AlLock->isChecked();
    Al=userI->AlFit->value();
    BlLock=userI->BlLock->isChecked();
    Bl=userI->BlFit->value();
    AuLock=userI->AuLock->isChecked();
    Au=userI->AuFit->value();
    BuLock=userI->BuLock->isChecked();
    Bu=userI->BuFit->value();
    wLock=userI->wLock->isChecked();
    w=userI->wFit->value();
    ILock=userI->Ilock->isChecked();
    I=userI->IFit->value();
    w2Lock=userI->w2Lock->isChecked();
    w2=userI->w2Fit->value();
    numberOfIterations=userI->IterCounter->value();
    relativeError=userI->epsrel->value();
    absoluteError=userI->epsabs->value();

}

void session::getUiData(){
    userI->JLc->setValue(jl);
    userI->JUc->setValue(ju);
    userI->Ival->setValue(nucSpin);
    userI->bgOffset->setValue(bgOffset);
    userI->offset->setValue(centerOfGravity);
    userI->width->setValue(width);
    userI->width2->setValue(width2);
    userI->intensityscalerl->setValue(intensity);
    userI->scaleA->setChecked(AuperAlbool);
    userI->alauval->setValue(AuperAl);
    userI->scaleB->setChecked(BuperBlbool);
    userI->blbuval->setValue(BuperBl);
    userI->inPeaks->setChecked(showInidividualPeaks);
    userI->Alc->setValue(IAl);
    userI->Blc->setValue(IBl);
    userI->Auc->setValue(IAu);
    userI->Buc->setValue(IBu);
    if(profile =="Lorentzian"){
        userI->profileSelector->setCurrentIndex(0);
    }
    else if(profile=="Gaussian"){
        userI->profileSelector->setCurrentIndex(1);
    }

    userI->X0Lock->setChecked(X0Lock);
    userI->X0->setValue(X0);
    userI->Y0Lock->setChecked(Y0Lock);
    userI->Y0->setValue(Y0);
    userI->AlLock->setChecked(AlLock);
    userI->AlFit->setValue(Al);
    userI->BlLock->setChecked(BlLock);
    userI->BlFit->setValue(Bl);
    userI->AuLock->setChecked(AuLock);
    userI->AuFit->setValue(Au);
    userI->BuLock->setChecked(BuLock);
    userI->BuFit->setValue(Bu);
    userI->wLock->setChecked(wLock);
    userI->wFit->setValue(w);
    userI->Ilock->setChecked(ILock);
    userI->IFit->setValue(I);
    userI->w2Lock->setChecked(w2Lock);
    userI->w2Fit->setValue(w2);
    userI->IterCounter->setValue(numberOfIterations);
    userI->epsrel->setValue(relativeError);
    userI->epsabs->setValue(absoluteError);
}

void session::writeSettings(){
    settings->beginGroup("Spectrumparameters");
    settings->setValue("Jlower", userI->JLc->value());
    settings->setValue("Jupper", userI->JUc->value());
    settings->setValue("NuclearSpin", userI->Ival->value());
    settings->setValue("bgOffset", userI->bgOffset->value());
    settings->setValue("Centerofgravity", userI->offset->value());
    settings->setValue("Width",userI->width->value());
    settings->setValue("Width2",userI->width2->value());
    settings->setValue("Intesityscaler",userI->intensityscalerl->value());
    settings->setValue("Profile", profile);
    settings->setValue("Au/Al_isToggled",userI->scaleA->isChecked());
    settings->setValue("Au/Al_value",userI->alauval->value());
    settings->setValue("Bu/Bl_isToggled",userI->scaleB->isChecked());
    settings->setValue("Bu/Bl_value",userI->blbuval->value());
    settings->setValue("Invidualpeaks",userI->inPeaks->isChecked());
    settings->setValue("Alower", userI->Alc->value());
    settings->setValue("Blower", userI->Blc->value());
    settings->setValue("Aupper", userI->Auc->value());
    settings->setValue("Bupper", userI->Buc->value());
    settings->setValue("Profile", profile);
    settings->endGroup();

    settings->beginGroup("Fitterparameters");
    settings->setValue("X0Lock",userI->X0Lock->isChecked());
    settings->setValue("X0",userI->X0->value());
    settings->setValue("Y0Lock",userI->Y0Lock->isChecked());
    settings->setValue("Y0",userI->Y0->value());
    settings->setValue("AlLock",userI->AlLock->isChecked());
    settings->setValue("Al",userI->AlFit->value());
    settings->setValue("BlLock",userI->BlLock->isChecked());
    settings->setValue("Bl",userI->BlFit->value());
    settings->setValue("AuLock",userI->AuLock->isChecked());
    settings->setValue("Au",userI->AuFit->value());
    settings->setValue("BuLock",userI->BuLock->isChecked());
    settings->setValue("Bu",userI->BuFit->value());
    settings->setValue("WLock",userI->wLock->isChecked());
    settings->setValue("W",userI->wFit->value());
    settings->setValue("ILock",userI->Ilock->isChecked());
    settings->setValue("I",userI->IFit->value());
    settings->setValue("W2Lock",userI->w2Lock->isChecked());
    settings->setValue("W2",userI->w2Fit->value());
    settings->setValue("Iterations", userI->IterCounter->value());
    settings->setValue("Relativeerror", userI->epsrel->value());
    settings->setValue("Absoluteerror", userI->epsabs->value());
    settings->setValue("firstrun",firstFitterRun);
    settings->setValue("restored",restored);
    settings->setValue("output",fitterOutputString);
    settings->endGroup();
}

void session::writeArrayData(){
    QVariant ySpectrum_v=QListToQVariantList(ySpectrum);               //x spectrum
    QVariant xSpectrum_v=QListToQVariantList(xSpectrum);               //y spectrum
    QVariant fitCurve_v=QListToQVariantList(fitCurve);            //fitted curve
    QVariant residualCurve_v=QListToQVariantList(residualCurve);       //residualCurve


    settings->beginGroup("FitterData");
    settings->setValue("ySpectrum",ySpectrum_v);
    settings->setValue("xSpectrum",xSpectrum_v);
    settings->setValue("fitCurve",fitCurve_v);
    settings->setValue("residualCurve",residualCurve_v);
    settings->endGroup();
}

void session::readSettings(){
    jl=settings->value("Spectrumparameters/Jlower").toDouble();
    ju=settings->value("Spectrumparameters/Jupper").toDouble();
    nucSpin=settings->value("Spectrumparameters/NuclearSpin").toDouble();
    bgOffset=settings->value("pectrumparameters/bgOffset").toDouble();
    centerOfGravity=settings->value("Spectrumparameters/Centerofgravity").toDouble();
    width=settings->value("Spectrumparameters/Width").toDouble();
    width2=settings->value("Spectrumparameters/Width2").toDouble();
    intensity=settings->value("Spectrumparameters/Intesityscaler").toDouble();
    profile= settings->value("Spectrumparameters/Profile").toString();
    AuperAlbool=settings->value("Spectrumparameters/Au/Al_isToggled").toBool();
    AuperAl=settings->value("Spectrumparameters/Au/Al_value").toDouble();
    BuperBlbool=settings->value("Spectrumparameters/Bu/Bl_isToggled").toBool();
    BuperBl=settings->value("Spectrumparameters/Bu/Bl_value").toDouble();
    showInidividualPeaks=settings->value("Spectrumparameters/Invidualpeaks").toBool();
    IAl=settings->value("Spectrumparameters/Alower").toDouble();
    IBl=settings->value("Spectrumparameters/Blower").toDouble();
    IAu=settings->value("Spectrumparameters/Aupper").toDouble();
    IBu=settings->value("Spectrumparameters/Bupper").toDouble();
    profile=settings->value("Spectrumparameters/Profile").toString();

    X0Lock=settings->value("Fitterparameters/X0Lock").toBool();
    X0=settings->value("Fitterparameters/X0").toDouble();
    Y0Lock=settings->value("Fitterparameters/Y0Lock").toBool();
    Y0=settings->value("Fitterparameters/Y0").toDouble();
    AlLock=settings->value("Fitterparameters/AlLock").toBool();
    Al=settings->value("Fitterparameters/Al").toDouble();
    BlLock=settings->value("Fitterparameters/BlLock").toBool();
    Bl=settings->value("Fitterparameters/Bl").toDouble();
    AuLock=settings->value("Fitterparameters/AuLock").toBool();
    Au=settings->value("Fitterparameters/Au").toDouble();
    BuLock=settings->value("Fitterparameters/BuLock").toBool();
    Bu=settings->value("Fitterparameters/Bu").toDouble();
    wLock=settings->value("Fitterparameters/WLock").toBool();
    w=settings->value("Fitterparameters/W").toDouble();
    ILock=settings->value("Fitterparameters/ILock").toBool();
    I=settings->value("Fitterparameters/I").toDouble();
    w2Lock=settings->value("Fitterparameters/W2Lock").toBool();
    w2=settings->value("Fitterparameters/W2").toDouble();
    numberOfIterations=settings->value("Fitterparameters/Iterations").toInt();
    absoluteError=settings->value("Fitterparameters/Relativeerror").toDouble();
    relativeError=settings->value("Fitterparameters/Absoluteerror").toDouble();
    firstFitterRun=settings->value("Fitterparameters/firstrun").toBool();
    restored=settings->value("Fitterparameters/restored").toBool();
    fitterOutputString=settings->value("Fitterparameters/output").toString();
}

void session::readArrayData(){
    QVariantList ySpectrum_v=settings->value("FitterData/ySpectrum").toList();
    QVariantList xSpectrum_v=settings->value("FitterData/xSpectrum").toList();
    QVariantList fitCurve_v=settings->value("FitterData/fitCurve").toList();
    QVariantList residualCurve_v=settings->value("FitterData/residualCurve").toList();

    ySpectrum=QVariantListToQList(ySpectrum_v);
    xSpectrum=QVariantListToQList(xSpectrum_v);
    fitCurve=QVariantListToQList(fitCurve_v);
    residualCurve=QVariantListToQList(residualCurve_v);

}

void session::restoreSession(){
    readSettings();
    readArrayData();
    getUiData();
    calculateFullHFSspectrum();
    plotCalculatedHFS();

    if(!xSpectrum.isEmpty()){
        dataSize=xSpectrum.size();

        allocateSpectrumArrays();
        setSpectrumDataToArrayFormat();
        plotSpectrum();
    }



    if(firstFitterRun==false){
        fitPlot->mainPlot->setSamples(xSpectrum, fitCurve);
        userI->fitPlot->replot();
        residualPlot->mainPlot->setSamples(xSpectrum, residualCurve);
        userI->residualPlot->replot();
        userI->profileSelector->setDisabled(true);
        userI->FitResultProgress->appendPlainText(fitterOutputString);

    }
    else if(firstFitterRun==true){
        userI->profileSelector->setEnabled(true);
    }

}

void session::saveSession(){
    setUiData();
    writeSettings();
    writeArrayData();

}

void session::exitSession(){
    restored=true;
    setUiData();
    writeSettings();
    writeArrayData();
    onSpectrumCleared();
    onResetButtonClicked();
    deleteArrays();
    deAllocateArrays();


}
void session::setStartUpValues(){
    jl=1;
    ju=0;
    nucSpin=1;
    centerOfGravity=0;
    width=50;
    width2=20;
    intensity=300;
    AuperAlbool=false;
    AuperAl=1;
    BuperBlbool=false;
    BuperBl=1;
    showInidividualPeaks=false;
    IAl=0;
    IBl=0;
    IAu=0;
    IBu=0;
    profile="Lorentzian";

    X0Lock=false;
    X0=0;
    Y0Lock=false;
    Y0=0;
    AlLock=false;
    Al=0;
    BlLock=false;
    Bl=0;
    AuLock=false;
    Au=0;
    BuLock=false;
    Bu=0;
    wLock=false;
    w=0;
    ILock=false;
    I=0;
    w2Lock=false;
    w2=0;
    numberOfIterations=500;
    relativeError=0.00001;
    absoluteError=0.00001;

}

//Theoretical hyperfine spectrum
//calculates allowed transitions from the given spins
void session::calculateAllowedTransitions(){
    transitionData.clear();
    transitionData=hyperfineSpectrum->transitions(jl,ju, nucSpin);
    Alphal.clear();
    Betal.clear();
    Alphau.clear();
    Betau.clear();
    Is.clear();
    Is90.clear();
    foreach(QList<double> transition, transitionData){
        Alphal.append(transition.at(2));                 //lower alphas
        Betal.append(transition.at(3));                  //lower betas
        Alphau.append(transition.at(4));                 //upper alphas
        Betau.append(transition.at(5));                  //upper betas
        Is.append(transition.at(6));
        Is90.append(transition.at(7));

    }
    resultTable->replace(transitionData);
}

//Calculates the transition frequencies using the A and B values stores within the class
void session::calculateTransitionFrequencies(){
    QList<QList<double> >  transitionFrequenciesFull; //transition frequencies are on index 5 of eahc QList
    transitionFrequencies.clear();

    if(!transitionData.isEmpty()){
        transitionFrequenciesFull=hyperfineSpectrum->transfreqs(transitionData, IAu, IBu, IAl, IBl,centerOfGravity);
        foreach(QList<double> transition, transitionFrequenciesFull){
            transitionFrequencies.append(transition.at(5));
            //Is.append(transition.at(2));
            //Is90.append(transition.at(3));

        }
        numberOfPeaks=transitionFrequenciesFull.size();
    }
    else{
        //qDebug()<<"no allowed transitions available";
    }
}
void session::calculateSpectrumProfile(){

    calculatedSpectrumProfile=hyperfineSpectrum->returnSpectrumProfile(profile, xRange, width,intensity,bgOffset,Is,transitionFrequencies,width2);
}

//calculates the spectrum from the beginning
void session::calculateFullHFSspectrum(){
    calculateAllowedTransitions();
    calculateTransitionFrequencies();
    calculateXRange();
    calculateSpectrumProfile();
}
//calculates the spectrum from the store transitiondata
void session::calculateSpectrum(){
    if(!transitionFrequencies.isEmpty()){
        calculateTransitionFrequencies();
    }
    calculateXRange();
    calculateSpectrumProfile();
}

void session::calculateXRange(){
    xRange.clear();
    double high=0;
    double low=0;
    double xminus;  // for the plotting and calculation range
    double xplus;
    double interval;
    //search for first and the last transition
    for(int i=0;i<transitionFrequencies.size();i++){
        if(transitionFrequencies[i]>=high){
            high=transitionFrequencies[i];
        }
        else if(transitionFrequencies[i]<=low){
            low=transitionFrequencies[i];
        }
    }
    interval=fabs(low-high)*2+sqrt(pow(width,2)+pow(width2,2))*10; //total width of the peak range plus fwhm times 10
    xminus=-interval+centerOfGravity;
    xplus=interval+centerOfGravity;
    for(double i=xminus; i<=xplus; i=i+interval/1000){           //calculates unnecessarily fine range if the FWHM is small and A's and B's are large

        xRange<<i;
    }
}
void session::setInitialValues(){
    if(!firstFitterRun && restored){
        X0=userI->X0->value();
        Y0=userI->Y0->value();
        Al=userI->AlFit->value();
        Bl=userI->BlFit->value();
        Au=userI->AuFit->value();
        Bu=userI->BuFit->value();
        w=userI->wFit->value();
        I=userI->IFit->value();
        w2=userI->w2Fit->value();

    }
    else if(firstFitterRun){
        X0=userI->offset->value();
        Y0=userI->bgOffset->value();
        Al=userI->Alc->value();
        Bl=userI->Blc->value();
        Au=userI->Auc->value();
        Bu=userI->Buc->value();
        w=userI->width->value();
        I=userI->intensityscalerl->value();
        w2=userI->width2->value();

    }
    else if(!firstFitterRun){
        X0=userI->X0->value();
        Y0=userI->Y0->value();
        Al=userI->AlFit->value();
        Bl=userI->BlFit->value();
        Au=userI->AuFit->value();
        Bu=userI->BuFit->value();
        w=userI->wFit->value();
        I=userI->IFit->value();
        w2=userI->w2Fit->value();
    }
}

void session::setLockedParameters(){
    X0Lock=userI->X0Lock->isChecked();           //centroid lock
    Y0Lock=userI->Y0Lock->isChecked();         // y-offset lock
    AlLock=userI->AlLock->isChecked();            //AL lock
    BlLock=userI->BlLock->isChecked();             //Bl lock
    AuLock=userI->AuLock->isChecked();            //Au lock
    BuLock=userI->BuLock->isChecked();            //Bu lock
    wLock=userI->wLock->isChecked();             //width lock
    ILock=userI->Ilock->isChecked();             //peak height lock
    w2Lock=userI->w2Lock->isChecked();             //width lock
}

void session::setFitterControlParameters(){
    numberOfIterations=userI->IterCounter->value();
    absoluteError=userI->epsabs->value();
    relativeError=userI->epsrel->value();
}
//allocates arrays for storinf the spectrum daa and the calculated HFs spectrum parameters in c++ array
//The othermemebers here delete and deallocate these arrays:
void session::allocateArrays(){
    lowerAlphas=new double[numberOfPeaks];
    lowerBetas=new double[numberOfPeaks];
    upperAlphas=new double[numberOfPeaks];
    upperBetas=new double[numberOfPeaks];
    peakIntensities=new double[numberOfPeaks];
}
void session::allocateSpectrumArrays(){
    xarray = new double[dataSize];
    yarray = new double[dataSize];
    sigma=   new double[dataSize];
}

void session::deleteArrays(){
    delete [] lowerAlphas;
    delete [] lowerBetas;
    delete [] upperAlphas;
    delete [] upperBetas;
    delete [] peakIntensities;
    deAllocateArrays();
}
void session::deleteSpectrumArrays(){
    delete [] xarray;
    delete [] yarray;
    delete [] sigma;
}

void session::deAllocateArrays(){
    lowerAlphas=NULL;
    lowerBetas=NULL;
    upperAlphas=NULL;
    upperBetas=NULL;
    peakIntensities=NULL;
}

void session::deAllocateSpectrumArrays(){
    xarray=NULL;
    yarray=NULL;
    sigma=NULL;
}
///////////
//Open a experimental spectrum
//At the moment requires a two column .dat file with tab as an separator
//The errors are taken as square root of the counts
void session::openSpectrum(){
    QVector<QVector<double> > openSpectrum;
    openSpectrum= ioOperations->openSpectrum();
    xSpectrum.clear();
    ySpectrum.clear();
    if(!openSpectrum.isEmpty()){
        xSpectrum=openSpectrum.at(0);
        ySpectrum=openSpectrum.at(1);
    }
}

//data conversion members

QVector<double> session::QVariantListToQList(QVariantList A){
    QVector<double> data;
    foreach(QVariant i, A){
        data.append(i.toDouble());
    }
    return data;
}

QVariant session::QListToQVariantList(QVector<double> A){
    QList<QVariant> list;
    foreach(double i, A){
        QVariant val(i);
        list.append(val);

    }
    QVariant listOfDoubles(list);
    return listOfDoubles;

}
//sets the calculated spectrum peak parameters to array format
void session::setDataToArrayFormat(){
    int i=0;
    foreach (QList<double> peak, transitionData) {
        lowerAlphas[i]=peak.at(2);
        lowerBetas[i]=peak.at(3);
        upperAlphas[i]=peak.at(4);
        upperBetas[i]=peak.at(5);
        peakIntensities[i]=peak.at(6);
        i++;

    }
}
//same for measured spectrum
void session::setSpectrumDataToArrayFormat(){
    for(int dp=0;dp<dataSize;dp++){
        xarray[dp]=xSpectrum.at(dp);
        yarray[dp]=ySpectrum.at(dp);
        if(ySpectrum.at(dp)!=0){
            sigma[dp]=sqrt(ySpectrum.at(dp));
        }
        else{
            sigma[dp]=1;
        }
    }
}
//////////
void session::setFitResultsToUi(std::unordered_map<std::string, double> results){


    userI->X0->blockSignals(true);
    userI->Y0->blockSignals(true);
    userI->AlFit->blockSignals(true);
    userI->BlFit->blockSignals(true);
    userI->AuFit->blockSignals(true);
    userI->BuFit->blockSignals(true);
    userI->wFit->blockSignals(true);
    userI->IFit->blockSignals(true);
    userI->w2Fit->blockSignals(false);



    userI->X0->setValue(X0);
    userI->Y0->setValue(Y0);
    userI->AlFit->setValue(Al);
    userI->BlFit->setValue(Bl);
    userI->AuFit->setValue(Au);
    userI->BuFit->setValue(Bu);
    userI->wFit->setValue(w);
    userI->IFit->setValue(I);
    userI->w2Fit->setValue(w2);

    userI->X0->blockSignals(false);
    userI->Y0->blockSignals(false);
    userI->AlFit->blockSignals(false);
    userI->BlFit->blockSignals(false);
    userI->AuFit->blockSignals(false);
    userI->BuFit->blockSignals(false);
    userI->wFit->blockSignals(false);
    userI->IFit->blockSignals(false);
    userI->w2Fit->blockSignals(false);




}

//SLOTS
void session::onUispinSpinBoxClicked(){
    setUiData();
    calculateFullHFSspectrum();
    if(showInidividualPeaks){
        if(profile=="Lorentzian"){
            hyperfinePlot->allocate(transitionFrequencies.size());
            hyperfinePlot->setInd(hyperfineSpectrum->lorentzians(), xRange);
        }
        else if(profile=="Gaussian"){
            hyperfinePlot->allocate(transitionFrequencies.size());
            hyperfinePlot->setInd(hyperfineSpectrum->gaussians(), xRange);
        }
        else if(profile=="Voigt"){
            hyperfinePlot->allocate(transitionFrequencies.size());
            hyperfinePlot->setInd(hyperfineSpectrum->voigts(), xRange);
        }
    }
    plotCalculatedHFS();
}
void session::onUiOtherSpectrumValueChanged(){
    setUiData();
    calculateSpectrum();
    if(showInidividualPeaks){
        if(profile=="Lorentzian"){
            hyperfinePlot->setInd(hyperfineSpectrum->lorentzians(), xRange);
        }
        else if(profile=="Gaussian"){
            hyperfinePlot->setInd(hyperfineSpectrum->gaussians(), xRange);
        }
        else if(profile=="Voigt"){
            hyperfinePlot->setInd(hyperfineSpectrum->voigts(), xRange);
        }
    }
    plotCalculatedHFS();
}

void session::onSpectrumLoaded(){
    QVector<QVector<double> > openSpectrum;
    openSpectrum= ioOperations->openSpectrum();
    xSpectrum.clear();
    ySpectrum.clear();
    if(!openSpectrum.isEmpty()){
        xSpectrum=openSpectrum.at(0);
        ySpectrum=openSpectrum.at(1);
        dataSize=xSpectrum.size();
        if(xarray==NULL){
            allocateSpectrumArrays();
            setSpectrumDataToArrayFormat();
        }
        else{
            deleteSpectrumArrays();
            deAllocateArrays();
            allocateSpectrumArrays();
            setSpectrumDataToArrayFormat();
        }
    }
    plotSpectrum();
}
void session::onSpectrumCleared(){
    deleteSpectrumArrays();
    deAllocateSpectrumArrays();
    xSpectrum.clear();
    ySpectrum.clear();
    plotSpectrum();
}

void session::onShowIndividualLorentzianClicked(bool state){
    if(state){
        QList<QVector<double> > singlePeaks;
        if(profile=="Lorentzian"){
            singlePeaks=hyperfineSpectrum->lorentzians();
        }
        else if(profile=="Gaussian"){
            singlePeaks=hyperfineSpectrum->gaussians();
        }
        else if(profile=="Voigt"){
            singlePeaks=hyperfineSpectrum->voigts();
        }
        hyperfinePlot->allocate(transitionFrequencies.size());
        hyperfinePlot->setInd(singlePeaks,xRange);
        showInidividualPeaks=state;
    }
    else{
        QVector<double> nullv;
        QList<QVector<double> > nullvl;
        hyperfinePlot->allocate(0);
        hyperfinePlot->setInd(nullvl,nullv);
        showInidividualPeaks=state;
    }
    userI->HFS_plot->replot();
}



void session::onFitButtonClicked(){
    if(xSpectrum.size()!=0){
        int n;                  //number of datapoints to be fitted
        n=dataSize;
        int peaks;              //number of peaks
        peaks=numberOfPeaks;
        int p;  //number of variables
        p=8;    // number of variables is fixed as all the peaks are fitted with same set of variables, namely the A's and B's ,
        if(firstFitterRun || restored){
            setInitialValues();
            setLockedParameters();
            setFitterControlParameters();
            allocateArrays();
            setDataToArrayFormat();
            if(mainFitter==NULL){
            mainFitter= new fitter(profile.toStdString());
            }
            mainFitter->setConstants(lowerAlphas,lowerBetas,upperAlphas,upperBetas,peakIntensities);
            mainFitter->setData(n,xarray,yarray,sigma,peaks);
            mainFitter->setFitterControlParameters(numberOfIterations, absoluteError,relativeError);
            mainFitter->setInitialValues(X0,Y0,Al,Bl,Au,Bu,w,I,w2);
            mainFitter->setLockedParameters(X0Lock,Y0Lock,AlLock,BlLock,AuLock,BuLock,wLock,ILock,w2Lock);
            mainFitter->setSolver();
            mainFitter->iterateSolver();
            firstFitterRun=false;
        }
        else{
            setLockedParameters();
            setFitterControlParameters();
            mainFitter->setLockedParameters(X0Lock,Y0Lock,AlLock,BlLock,AuLock,BuLock,wLock,ILock,w2Lock);
            mainFitter->setFitterControlParameters(numberOfIterations, absoluteError,relativeError);
            mainFitter->iterateSolver();
        }

        //get results from the fitter
        std::unordered_map<std::string, double> results=mainFitter->returnResult();
        std::unordered_map<std::string, std::string> fitterState=mainFitter->returnFitterStatus();
        chiSq=results["reducedChiSquare"];
        X0=results["centerOfMass"];
        X0Err=results["centerOfMassError"];
        Y0=results["yOffset"];
        Y0Err=results["yOffSetError"];
        Al=results["lowerA"];
        AlErr=results["lowerAError"];
        Bl=results["lowerB"];
        BlErr=results["lowerBError"];
        Au=results["upperA"];
        AuErr=results["upperAError"];
        Bu=results["upperB"];
        BuErr=results["upperBError"];
        w=results["width"];
        wErr=results["widthError"];
        I=results["intensity"];
        IErr=results["intensityError"];
        w2=results["width2"];
        w2Err=results["width2Error"];
        //put the results to the outoutbox
        QString resultString;
        resultString="Reduced ChiSquare "+QString::number(chiSq) +"\n"
                +"X0 "+QString::number(X0) + "+-"+QString::number(X0Err)+"\n"
                +"Y0 "+QString::number(Y0) + "+-"+QString::number(Y0Err)+"\n"
                +"Al "+QString::number(Al) + "+-"+QString::number(AlErr)+"\n"
                +"Bl "+QString::number(Bl) + "+-"+QString::number(BlErr)+"\n"
                +"Au "+QString::number(Au) + "+-"+QString::number(AuErr)+"\n"
                +"Bu "+QString::number(Bu) + "+-"+QString::number(BuErr)+"\n"
                +"w "+QString::number(w) + "+-"+QString::number(wErr)+"\n"
                +"I "+QString::number(I) + "+-"+QString::number(IErr)+"\n"
                +"w2 "+QString::number(w2) + "+-"+QString::number(w2Err)+"\n";

        userI->FitResultProgress->appendPlainText(resultString);
        fitterOutputString.append(resultString+"\n");
        QString fstat="Iterations= " +QString::fromStdString(fitterState["Iteratations"])+ " Error status= " +QString::fromStdString(fitterState["errorStatus"]);
        userI->FitResultProgress->appendPlainText(fstat);
        //set the results to the ui and plot
        setFitResultsToUi(results);
        plotFitPlot();
        plotResidual();

    }
}

void session::onResetButtonClicked(){
    firstFitterRun=true;
    fitterOutputString.clear();
    userI->FitResultProgress->clear();
    if(xSpectrum.size()!=0){            //program crashes if the fitting has bee run once, hen the spectrum is cleared and then the rest fit button is clicked
        uiOperations->resetSpinboxValues(); //the crash only occurs if no spectrum is present
    }

    QVector<double> zeroData;
    zeroData<<0;
    fitPlot->mainPlot->setSamples(zeroData, zeroData);
    userI->fitPlot->replot();
    residualPlot->mainPlot->setSamples(zeroData, zeroData);
    userI->residualPlot->replot();
    if(mainFitter!=NULL){
        delete mainFitter;
        mainFitter=NULL;
    }

}

void session::onFitResultValueChanged(){

    if(mainFitter!=NULL){
        setInitialValues();
        mainFitter->setInitialValues(X0,Y0,Al,Bl,Au,Bu,w,I,w2);
        mainFitter->setSolver();
    plotFitPlot();
    }

}
void session::onLockButtonClicked(){
    if(mainFitter!=NULL){
    setInitialValues();
    mainFitter->setInitialValues(X0,Y0,Al,Bl,Au,Bu,w,I,w2);
    setLockedParameters();
    mainFitter->setLockedParameters(X0Lock,Y0Lock,AlLock,BlLock,AuLock,BuLock,wLock,ILock,w2Lock);
    mainFitter->setSolver();
    }
}

//SLOTS end
//Plotting
//Plot the calculated spectrum
void session::plotCalculatedHFS(){
    hyperfinePlot->mainPlot->setSamples(xRange, calculatedSpectrumProfile);
    userI->HFS_plot->replot();
}

void session::plotFitPlot(){
    std::vector<double> fitCurveVector=mainFitter->returnProfile(X0, Y0, Al, Bl, Au, Bu, w, I, w2);
    fitCurve.clear();
    for(int i=0; i<dataSize;i++){
        fitCurve.append(fitCurveVector[i]);
    }
    fitPlot->mainPlot->setSamples(xSpectrum, fitCurve);
    userI->fitPlot->replot();

}

//Plot the fit residual
void session::plotResidual(){
    residualCurve.clear();
    int i=0;
    foreach(double val, fitCurve){
        double res=val-ySpectrum.at(i);
        residualCurve.append(res);
        i++;
    }
    residualPlot->mainPlot->setSamples(xSpectrum, residualCurve);
    userI->residualPlot->replot();
}

//Plot the loaded spectrum
void session::plotSpectrum(){
    hyperfinePlot->spectrum->setSamples(xSpectrum, ySpectrum);
    userI->HFS_plot->replot();

    fitPlot->spectrum->setSamples(xSpectrum, ySpectrum);
    first=xSpectrum.first();
    last=xSpectrum.last();
    if(first>=last){
        first=xSpectrum.last();
        last=xSpectrum.first();
    }
    userI->fitPlot->setAxisScale(QwtPlot::xBottom,first,last);
    userI->residualPlot->setAxisScale(QwtPlot::xBottom,first,last);
    userI->residualPlot->setAxisAutoScale(QwtPlot::yRight);
    userI->fitPlot->replot();
}
