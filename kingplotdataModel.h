#ifndef KINGPLOTDATAMODEL_H
#define KINGPLOTDATAMODEL_H

#include <QObject>
#include <QTableView>
#include <QStandardItemModel>
#include <QString>
#include <QDebug>
#include <QPersistentModelIndex>
#include <QFileDialog>
#include <QFile>


class kingPlotDataModel : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit kingPlotDataModel(QObject *parent = 0, QTableView *dataTable=0);
    QTableView *kpTable;
    ~kingPlotDataModel();

    int ROWS;  //number of rows

    QVector<double> returnA1Data();
    QVector<double> returnXData();
    QVector<double> returnDXData();
    QVector<double> returnA2Data();
    QVector<double> returnYData();
    QVector<double> returnDYData();




private:
     QTableView *kingPlotTable_p; //the table widget
     QStandardItemModel * KingPlotDataTable;    //the table layout
     QStandardItem *item_p;   //single cell
     QList<QList<QStandardItem*> > tableItems; // all the table items

     QList<double> *row;
     QList<QList<double>* > storedData;  //holds the data stored in the table storedData[rows][column];

     int COLS;  //number of columns

     QVector<double> returnColumn(int column);
     bool ok; //boolen to see if double conevrsion worked

signals:
     void sendDataToPlot(const QVector<double> &,const QVector<double> &,const QVector<double> &, const QVector<double> &, const QVector<double> &,const QVector<double> &);

public slots:
     void addRow(double A1=1, double x=1, double dx=1, double A2=1, double y=1, double dy=1);
     void deleteRow();
     void plotSlot();
     void addData(QStandardItem* selection);
     void loadData();


};

#endif // KINGPLOTDATAMODEL_H
