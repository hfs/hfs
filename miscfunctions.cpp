
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "miscfunctions.h"

miscFunctions::miscFunctions(QObject *parent,Ui_HFS *ui) :
    QObject(parent)
{
    userI=ui;

    //set QList of QSpinboxes in the HFS tab

    HFSTabQWTCounters = userI->HFStab->findChildren<QwtCounter *>();

    //set Qlist of QWTsliders in the HFS tab

    HFSTabQWTSliders =userI->HFStab->findChildren<QwtSlider *>();

    //set Qlist of QRadiobuttons in the HFS tab

    HFSTabQRadiobuttons =userI->HFStab->findChildren<QRadioButton *>();

    //set QList  Qspinboxes in the fitting tab

    lorentzianFittingTabQWTCounters = userI->fitChoice->findChildren<QwtCounter *>();


}
miscFunctions::~miscFunctions(){
    HFSTabQWTCounters.clear();
    HFSTabQWTSliders.clear();
    lorentzianFittingTabQWTCounters.clear();
}

//set results to spinboxes
void miscFunctions::setSpinBoxValues(QList<double> results){

    userI->X0->setValue(results.at(0));
    userI->Y0->setValue(results.at(1));  //start the fitting wwith Y0 as zero
    userI->AlFit->setValue(results.at(2));
    userI->BlFit->setValue(results.at(3));
    userI->AuFit->setValue(results.at(4));
    userI->BuFit->setValue(results.at(5));
    userI->wFit->setValue(results.at(6));
    userI->IFit->setValue(results.at(7));



}

//resets spinboxvalues from the intial peak adjustment
void miscFunctions::resetSpinboxValues(){
    userI->X0->setValue(userI->offset->value());
    userI->Y0->setValue(0);  //start the fitting wwith Y0 as zero
    userI->AlFit->setValue(userI->Alc->value());
    userI->BlFit->setValue(userI->Blc->value());
    userI->AuFit->setValue(userI->Auc->value());
    userI->BuFit->setValue(userI->Buc->value());
    userI->wFit->setValue(userI->width->value());
    userI->IFit->setValue(userI->intensityscalerl->value());

}

//blocks and unblocks the ui updates in the HFS tab
void miscFunctions::blockHFSUiUpdates(bool block){

    foreach (QwtCounter *counter, HFSTabQWTCounters) {
        counter->blockSignals(block);

    }
    foreach (QwtSlider *slider, HFSTabQWTSliders) {
        slider->blockSignals(block);
    }




}

void miscFunctions::blockLorentzianFittingUpdates(bool block){
    foreach(QwtCounter *counter, lorentzianFittingTabQWTCounters){
        counter->blockSignals(block);
        counter->setEnabled(!block);
    }
}

//returns a QList of QLists containing the calculated alpha, beta and intensity values.
// the list is composed in order, alpha lower, beta lower, alpha upper, beta upper, intensities
// Each sub-QList e.g. alphal contains as many values as there are peaks in the dataset
QList<QList<double> > miscFunctions::setFitConstants(QList<QList<double> > peakData){

    QList<QList<double> >  fitConstants;
    QList<double> Alphal;  //lower alpha
    QList<double> Betal;   //lower beta
    QList<double> Alphau;  //upper alpha
    QList<double> Betau;   //upper beta
    QList<double> Is;      //intensity scals


    //alphas and betas
    foreach (QList<double> peak, peakData) {
        Alphal<<peak.at(2);
        Betal<<peak.at(3);
        Alphau<<peak.at(4);
        Betau<<peak.at(5);
        Is<<peak.at(6);

    }

    fitConstants  <<Alphal<< Betal<< Alphau<< Betau<<Is; //alphas, betas and intensities
    return fitConstants;

}

//Returns a QList of QLists containing initial values for the fitter
//The QLists are sorted in order, centroid, y-offset, A lower, B lower, A upper, B upper, HWHM, intensity scaler
QList<QList<double> > miscFunctions::setLorentzianFitParameterInitialValues(){

    QList<QList<double> > fitParameters;

    QList<double> xcz;   //centroid
    QList<double> AsBs;  //A and B avlues in order, A lower, B lower, Aupper, B upper
    QList<double> yzero; //y-offset
    QList<double> initI; //Intensity scaler
    QList<double> initW; //linewidth


    // set the initial values

    xcz<< userI->offset->value();  //centroid

    AsBs<< userI->Alc->value()<<userI->Blc->value()<<userI->Auc->value()<<userI->Buc->value();

    yzero<<userI->Y0->value(); //zerolevel

    initI<<userI->intensityscalerl->value();

    initW<<userI->width->value();

    fitParameters<< xcz << yzero<< AsBs<< initW<<initI ;   //initial values for the fit parametres, keep the intesities here just in case



    return fitParameters;

}

QList<QList<double> > miscFunctions::setLorentzianFitParameterReFitValues(){
    QList<QList<double> > fitParameters;

    QList<double> xcz;   //centroid
    QList<double> AsBs;  //A and B avlues in order, A lower, B lower, Aupper, B upper
    QList<double> yzero; //y-offset
    QList<double> initI; //Intensity scaler
    QList<double> initW; //linewidth


    // set the initial values

    xcz<< userI->X0->value();  //centroid

    AsBs<< userI->AlFit->value()<<userI->BlFit->value()<<userI->AuFit->value()<<userI->BuFit->value();

    yzero<<userI->Y0->value(); //zerolevel

    initI<<userI->IFit->value();

    initW<<userI->wFit->value();

    fitParameters<< xcz << yzero<< AsBs<< initW<<initI ;   //initial values for the fit parametres, keep the intesities here just in case



    return fitParameters;
}

QHash<QString, bool> miscFunctions::setLockedLorentzianVariables(){
    QHash<QString, bool> lockedVariables;

    lockedVariables["X0"]=userI->X0Lock->isChecked();
    lockedVariables["yzero"]=userI->Y0Lock->isChecked();
    lockedVariables["Al"]=userI->AlLock->isChecked();
    lockedVariables["Bl"]=userI->BlLock->isChecked();
    lockedVariables["Au"]=userI->AuLock->isChecked();
    lockedVariables["Bu"]=userI->BuLock->isChecked();
    lockedVariables["w"]=userI->wLock->isChecked();
    lockedVariables["I"]=userI->Ilock->isChecked();

    return lockedVariables;


}
