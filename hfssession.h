#ifndef HFSSESSION_H
#define HFSSESSION_H

#include "ui_hfs.h"
#include "hfsfunctions.h"
#include "lorentzianfitter_old.h"
#include "fitter.h"
#include "miscfunctions.h"
#include "setTable.h"
#include "plotSet.h"
#include "fileoperations.h"


#include <QObject>
#include <QStringList>
#include <QVector>
#include <QString>
#include <QHash>
#include <QDebug>

class hfsSession : public QObject
{
    Q_OBJECT
public:
    explicit hfsSession(QObject *parent = 0, Ui_HFS *ui=0, QString name = 0);
    ~hfsSession();
    void hfsSetTable(setTable *table=0);
    void hfsSetPlots(plotSet *hfsPlot_p=0, plotSet *fitPlot_p=0, plotSet *residualPlot_p=0);
    QString sessionName;
    QString profile;  //The profile to be fitted
    fitter *mainFitter;
    //Parameters for calculating the HFS spectrum
    //Spins, FHWM's,, intensity
    double jl;
    double ju;
    double nucSpin;
    double FWHM;
    double intensity;
    double offset;
    //Parameters to tune the spectrum
    //A's and B's, ratios, and their booleans
    double Al;
    double Bl;
    double Au;
    double Bu;
    double AuperAl;
    bool AuperAlbool;
    double BuperBl;
    bool BuperBlbool;

    //set fitter type
    void setFitterType(QString spectrumProfile);

    //output values for HFS calculations
    QVector<double> calculatedSpectrum; //lorentzians
    QVector<double> calculatedSpectrumX; //x-range
    //The followiung data in the Qlists is orderd by peak(transition)
    QList<QList<double> >  transitionFrequencies; //transition frequencies are on index 5 of eahc QList
    QList< QList<double> > transitionData;  // from HFSfunctions // alpha lower, beta lower, alpha upper, beta upper, intesity, int at 90,, pump fraction
    QList<double> Alphal;       //lower alphas
    QList<double> Betal;        //lower betas
    QList<double> Alphau;       //upper alphas
    QList<double> Betau;        //upper betas

    QList<double> ws;    //lorentzian half width full maximum for each peak
    QList<double> Is;    //intensities for each peak
    QList<double> Is90;  //Intensities at 90 derees
    QList<double> frequencies; //transition centroids
    QList<double> IntSca; //intensity scaler for each peak

    QList<double> centroid;  //centroid
    QList<double> AsBs; //A and B values
    QList<double> yzero;//constant background


    //Experimental spectrum data
    //x and y range for the loaded spectrum
    QVector<double> xSpectrum;
    QVector<double> ySpectrum;
    //Maximum and minimum value for the x-axis of the loaded spectrum
    double first, last;
    //array size
    int dataSize;
    int numberOfPeaks;
    //same in C++ array required by the fitter
    double *xarray;
    double *yarray;
    double *sigma;          //error for y
    double *lowerAlphas;
    double *lowerBetas;
    double *upperAlphas;
    double *upperBetas;
    double *peakIntensities;
    bool X0Lock;           //centroid lock
    bool yzeroLock;         // y-offset lock
    bool AlLock;            //AL lock
    bool BlLock;             //Bl lock
    bool AuLock;            //Au lock
    bool BuLock;            //Bu lock
    bool wLock;             //width lock
    bool ILock;             //peak height lock
    //initial values
    double IcenterOfMass;
    double IyOffset;
    double IlowerA;
    double IlowerB;
    double IupperA;
    double IupperB;
    double Iwidth;
    double Iintensity;
    //fitter parameter
    double numberOfIterations;
    double absoluteError;
    double relativeError;

    //fitter related members

    bool firstFitterRun;  // tracks if the fitter is ran for the first time;
    //a compilation of the fit parameter initial values
    //first the "shared"parameters
    //LIST is in order xcs,yzero, AsBsm, ws, Is, Alphal, Betal, Alphau, Betau
    QList<QList<double> > fitParameters;
    QList<QList<double> > fitConstants;
    int noOfFitParameters;
    int  noOfFitConstants;
    //Methods for getting and setting fit related parameters and fitting
    QList<double> getFitResults();         //get the fit results and retunr them as a QList
    QList<double> getFitErrors();          //get the fit errors
    QStringList fitterOutput;               //Get the fit output and print it out in a textbox
    QHash<QString, double> fitResults; //QHash for the fitresults, includig the errors. does not containg possible per peak intensities
    //y-data from the fitter
    QVector<double> fitData;

    //Fitted spectrum data
    double X0;
    double X0Err;
    double Y0;
    double Y0Err;
    double Alfit;
    double AlfitErr;
    double Blfit;
    double BlfitErr;
    double Aufit;
    double AufitErr;
    double Bufit;
    double BufitErr;
    double wFit;
    double wFitErr;
    double IFit;
    double IFitErr;

    //save all ui data
    void saveUiData();
    //restore ui data
    void restoreUiData();

    void onUispinSpinBoxClicked(); //This gets called when either the jl, ju or nuclear spin value changes
    void onUiOtherSpectrumValueChanged(); // This gets called otherwise

    void onSpectrumLoaded(); //loads and plot an experimental spectrum
    void onSpectrumCleared(); //Clears the spectrum

    void onShowIndividualLorentzianClicked(bool state); //Shows and ides individual lorntzian peaks

    //Fitting related members
    void onFitButtonClicked();
    void onResetButtonClicked();
    void onFitResultValueChanged();


    //return centroid error and class instance name
    QString returnCentroid(){
        QString x0fS=QString::number(X0);
        QString x0fErrS=QString::number(X0Err);
        QString centroidString;
        centroidString.append(sessionName);
        centroidString.append("\t");
        centroidString.append("Centroid:");
        centroidString.append(x0fS);
        centroidString.append("+-");
        centroidString.append(x0fErrS);
        centroidString.append("\n");
        return centroidString;
    }

private:

    Ui_HFS *userI;
    HFSfunctions *hfsCalc;
    lorentzianFitter_OLD *lFitter;

    fileOperations *fileOp;
    miscFunctions *uiOperations;
    dataConvert *convert;
    setTable *resultTable;
    plotSet *hfsPlot;
    plotSet *fitPlot;
    plotSet *residualPlot;

    //Calculates a suitable x-axis for the hfs spectrum
    QVector<double> calculateXRange(QList<double> trans, double fwhm);

    //sets ui values form the values stored in this class
    void setHFSUiValues();
    //get ui values from the ui
    void getHFSUiValues();
    //set fitted values the ui
    void setFitValuestoUi();
    //get fit values, to be used as initial values, from the ui. This ise used if the values are changed in the ui
    void getInitialFitValues();
    //calculate allowed transition
    void calculateAllowedTransitions();
    //Calculate the transition frequencies
    void calculateTransitionFrequencies();
    //Calculate the full HFS spectrum from the start
    void calculateFullHFSspectrum();
    //Calculates the spectrum when the allowed transitions are known
    void calculateSpectrum();
    //Opens the experimental spectrum
    void openSpectrum();


    //plotting:
    //plot the calcukated HFS to the HFS_plot
    void plotCalculatedHFS();
    //Plot the fitted plot to the fitPlot
    void plotFitPlot();
    //Plot the residual plot to residualPlot
    void plotResidual();
    //Plot the spectrum on hte HFS_plot and fitPlot when spectrum is loaded
    void plotSpectrum();

    //fitting
    void lorentzianFitRoutine(bool firstRun, int n, int p, int peaks, double *xarray, double *yarray, double *sigma);
    void fitterTerminationConditions();
    void lockedLorentzianFitParameters();



    //new fitting routine code

    void setInitialValues();
    void setLockedParameters();
    void setFitterControlParameters();
    void allocateArrays();
    void deleteArrays();
    void deAllocateArrays();
    void setDataToArrayFormat();    //set the data that is in Qt doermat, e.g. QVector to array

signals:

public slots:


};

#endif // HFSSESSION_H
