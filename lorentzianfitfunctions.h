#ifndef LORENTZIANFITFUNCTIONS_H
#define LORENTZIANFITFUNCTIONS_H
#include <gsl/gsl_math.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_blas.h>

class lorentzianFitFunctions
{
public:
    explicit lorentzianFitFunctions();
    struct data {
        size_t n;              //number of functions == number of datapoints
        double *y;             //data array, contains the data to be fitted
        double *xdata;         //the x- axis data for the calculation of the functions
        double *sigma;         //standard deviation or error of each datapoint
        int peaks;             //number of peaks
        //alphas and betas as an array
        double *alphal;
        double *betal;
        double *alphau;
        double *betau;
        //peak intensities
        double *I;
        //Boolens containing information on wheter a certain parameter is locked or not
        bool X0Lock;           //centroid lock
        bool yzeroLock;         // y-offset lock
        bool AlLock;            //AL lock
        bool BlLock;             //Bl lock
        bool AuLock;            //Au lock
        bool BuLock;            //Bu lock
        bool wLock;             //width lock
        bool ILock;             //peak height lock
    };

    //returns the center of a peak
    double hfsCenterOfGravity(double xcz_fVal,double Al_fVal,double Bl_fVal,double Au_fVal,double Bu_fVal,double all_fVal,double bel_fVal,double alu_fVal,double beu_fVal);
    // returns a value calculated with a lorentzian with HFS parameters put in
    double lorentzianFunction(double t_fVal, double hfsCenterOfGravity_fVal, double yzero_fVal, double w_fVal, double I_fVal);
    //derivative of a lorentzian function against width w
    double lorentzianWDerivative(double t_fVal, double hfsCenterOfGravity_fVal, double w_fVal, double I_fVal);
    //derivative of a forentzian function againts intensity I
    double lorentzianIDerivative(double t_fVal, double hfsCenterOfGravity_fVal, double w_fVal);
    //derivative of a lorentzian function against centroid parameters, namely xcz, Al, Bl, Au or Bu
    double lorentzianCentDerivative(double t_fVal, double hfsCenterOfGravity_fVal,double w_fVal, double I_fVal);
    //derivative of a lorentzian function against the y-offset
    double lorentzianYZeroDerivative();
    //the function to be fitted and its operator
    int lorentzian_f(const gsl_vector *x, void *data, gsl_vector *f);
    int operator()(const gsl_vector *x, void *data, gsl_vector *f){return lorentzian_f(x, data, f);}
    //the analytical derivates of the function in a form of a Jacobian matrix
    int lorentzian_fd(const gsl_vector * x, void *data, gsl_matrix * J);
    int operator()(const gsl_vector * x, void *data, gsl_matrix * J){return lorentzian_fd(x, data, J);}

    //a function which calls both of these and its operator
    int lorentzian_fdf(const gsl_vector * x, void *data,gsl_vector * f, gsl_matrix * J);
    int operator()(const gsl_vector * x, void *data,gsl_vector * f, gsl_matrix * J){return lorentzian_fdf(x, data,f, J);}



};

#endif // LORENTZIANFITFUNCTIONS_H
