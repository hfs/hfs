
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "fileoperations.h"

fileOperations::fileOperations(QObject *parent, Ui_HFS *ui, setTable *table) :
    QObject(parent)
{
    userI=ui;
    HFSmodel=table;
    specdata=NULL;
}

void fileOperations::saveTableToFile(){
    QString fileName = QFileDialog::getSaveFileName( 0, tr("Save File As..."), QDir::homePath(), tr("txt (*.txt)"), NULL, QFileDialog::DontConfirmOverwrite);  //get the filename
    //Check whether the file is readable
    if (fileName.isEmpty())
        return;
    else {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)){
            QMessageBox::information(0, tr("Unable to open file"), file.errorString());
            return;
        }
        else{
            //save the data
            file.open(QIODevice::WriteOnly | QFile::Truncate | QFile::Text);

            QTextStream stream(&file);
            int rowCount = HFSmodel->HFSmodel->rowCount();
            int columnCount = HFSmodel->HFSmodel->columnCount();

            // add labels
            stream <<"fl\t\t"<<"fu\t\t"<<"alpha_l\t\t"<<"beta_l\t\t"<<"alpha_u\t\t"<<"beta_u\t\t"<<"int\t\t"<<"int@90\t\t"<<"pumpfrac\t\t"<<" \n";
            stream <<"==\t\t"<<"==\t\t"<<"=======\t\t"<<"====== \t\t"<<"=======\t\t"<<"======\t\t"<<"===\t\t"<<"======\t\t"<<"======== \t"<<" \n";
            for(int row = 0; row < rowCount; row++){
                for(int column = 0; column < columnCount; column++){
                    QString f;
                    f=HFSmodel->HFSmodel->item(row, column)->text();
                    stream << f.simplified().toLatin1()<<"\t";

                }
                stream << " \n";  //newline to the end of line
            }

        }

    }
}

void fileOperations::savePlotToPDF(QwtPlot *plot){
    //savefiledialog
    QString fileName = QFileDialog::getSaveFileName(0, tr("Save plot As..."), QDir::homePath(), tr("pdf (*.pdf)"), NULL, QFileDialog::DontConfirmOverwrite);  //get the filename
    //Check whether the file is readable
    if (fileName.isEmpty())
        return;
    else{
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(0, tr("Unable to open file"), file.errorString());
            return;
        }

        else{

            plot->setAxisFont(QwtPlot::xBottom,QFont(QString("Arial"), 30, 0, false));
            //image properties
            QPrinter printer( QPrinter::HighResolution );
            printer.setOutputFormat( QPrinter::PdfFormat );
            printer.setOutputFileName( fileName );

            QwtPlotRenderer renderer;
            renderer.renderDocument(plot, fileName, QSizeF(300, 200), 85);
            plot->setAxisFont(QwtPlot::xBottom,QFont(QString("Arial"), 10, 0, false));

        }

    }

}

void fileOperations::saveDatatoFile(QVector<double> x, QVector<double> y){
    //savefiledialog
    QString fileName = QFileDialog::getSaveFileName(0, tr("Save Data As..."), QDir::homePath(), tr("dat (*.dat)"), NULL, QFileDialog::DontConfirmOverwrite);  //get the filename
    //Check whether the file is readable
    if (fileName.isEmpty())
        return;
    else{
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(0, tr("Unable to open file"), file.errorString());
            return;
        }

        else{
            QTextStream stream(&file);
            for(int i = 0; i<x.size(); i++){
                stream << x.at(i) << "\t" <<y.at(i) << "\n";
            }

        }
    }
}

QVector<QVector<double> > fileOperations::openSpectrum(){
    QStringList fileNames = QFileDialog::getOpenFileNames( 0, tr("Open spectrum data"), QDir::homePath());  //get the filename

    QVector<QVector<double> > returnedSpectrum;
    if(!fileNames.isEmpty()){
        specdata = new fileParser("E",fileNames,0);
        returnedSpectrum<<specdata->data().at(0)<<specdata->data().at(1);

    }

    if(specdata!=NULL){
        delete specdata;
        specdata=NULL;
    }
    return returnedSpectrum;
}
