#ifndef HFS_H
#define HFS_H

#include <QMainWindow>

#include <QAction>
#include <hfsfunctions.h>
#include <fileparser.h>
#include <plotSet.h>
#include <setTable.h>
#include <QStringList>
#include <dataconvert.h>

#include <QFile>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QClipboard>
#include <QKeyEvent>
#include <QVector>
#include <QHash>


#include <miscfunctions.h>
#include <fileoperations.h>



#include <kingplotdataModel.h>
#include <kingplotdata.h>

#include <session.h>

namespace Ui {
class HFS;
}

class HFS : public QMainWindow
{
    Q_OBJECT

public:
    explicit HFS(QWidget *parent = 0);
    ~HFS();
    HFSfunctions functions;  //init the functions needed for calculations

    plotSet *HFS_plot;     //sets up a plot when given a qwt plot widget
    plotSet *fittingPlot;  //A plot to display the fitting progress
    plotSet *residualPlot; //A plot to display the residual between the fitted data and he spectrum

    dataConvert *convert;
    setTable *HFSmodel;           //table for the HFS data



    //new hfs session class, will replace the old one when working

    session *fittingSession;

    session *currentFittingSession;

    miscFunctions *uioperations;    //A class containing for example data conversion and signal blocking members

    fileOperations *fileOp;         //A class containing file io members

    kingPlotDataModel *kingPlotTable; //The model for the kong plot table

    kingPlotData *kingPlot;


protected:
    void keyPressEvent(QKeyEvent *event ); //copy to clipboard etc

private slots:
    //Slider slots
    void on_Aus_sliderMoved(double value);

    void on_Bus_sliderMoved(double value);

    void on_Als_sliderMoved(double value);

    void on_Bls_sliderMoved(double value);

    //Scaling radiobutton toggling slots

    void on_scaleA_toggled(bool checked);

    void on_scaleB_toggled(bool checked);

    void on_alauval_valueChanged(double value);

    void on_blbuval_valueChanged(double value);


    // void calculate();  //function to call the HFs calculation and add the data to the table
    //Function for adjusting the Al, Bl, Au and Bu values
    //  void alblaubu_Adjust();

    void on_Auc_valueChanged(double value);

    void on_Buc_valueChanged(double value);

    void on_Alc_valueChanged(double value);

    void on_Blc_valueChanged(double value);

    //session management slots

    void createSession();

    void restoreSession(QModelIndex itemIndex );

    void renameSession(QListWidgetItem * item );

    void deleteSession(QListWidgetItem *item);

    void disConnectHfsSessionSlots();

    void onUispinSpinBoxClicked(); //This gets called when either the jl, ju or nuclear spin value changes
    void onUiOtherSpectrumValueChanged(); // This gets called otherwise

    void onSpectrumLoaded(); //loads and plot an experimental spectrum
    void onSpectrumCleared(); //Clears the spectrum

    void onShowIndividualLorentzianClicked(bool state); //Shows and ides individual lorntzian peaks

    //Fitting relatd slots
    void onFitButtonClicked();
    void onResetButtonClicked();
    void onFitResultValueChanged();
    void onLockButtonClicked();
    void onProfileChanged(QString selectedProfile);
    //slot for setting the fitvalues to the output box

    void setFitOutPut(QString data);

    //SLot to set the nuclei data to the kingplotdata class instance

    void setNucleiData();

    //return the centroids soter in the session
    void returnCentroids(){
        ui->listOfFittedCentroids->clear();
        if(!sessionStorage.isEmpty()){
        foreach(session *session, sessionStorage){
            QString cent=session->returnCentroid();
            ui->listOfFittedCentroids->append(cent);
        }
        }
    }


private:
    Ui::HFS *ui;

    //Boolean values for the radiobuttons
    bool alau;
    bool blbu;
    //Al/Al and Bl/Bu scale factors
    double alauval;
    double blbuval;

    //Qhash to store pointers to different hfsSession instances

    QHash<QString, session*> sessionStorage;
    QString currentSession;

    //connect slots
    void connectHfsSessionSlots();





};

#endif // HFS_H
