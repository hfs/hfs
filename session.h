#ifndef SESSION_H
#define SESSION_H
#include "ui_hfs.h"
#include "hfsfunctions.h"
#include "fitter.h"
#include "miscfunctions.h"
#include "setTable.h"
#include "plotSet.h"
#include <QObject>
#include <QSettings>
#include <QVector>
#include <unordered_map>
#include <vector>

Q_DECLARE_METATYPE(QList<double>)


class session : public QObject
{
    Q_OBJECT
public:
    explicit session(QObject *parent = 0, Ui_HFS *ui=0, QString name = 0);                  //initializes the class
    ~session();
    void setResultTable(setTable *table=0);                                                 //attaches the pointer of the results table from the ui
    void setPlots(plotSet *hfsPlot_p=0, plotSet *fitPlot_p=0, plotSet *residualPlot_p=0);   //attaches the pointer of the plots the ui
    void setUiData();                                                                       //store the ui data to the class
    void getUiData();                                                                       //set the data from the class to ui
    void writeSettings();                                                                   //write settings to disk
    void writeArrayData();                                                                  //write arraydata to disk
    void readSettings();                                                                    //read
    void readArrayData();                                                                   //read
    void restoreSession();                                                                  //restores stored session
    void exitSession();                                                                     //exits the session when switching to another
    void saveSession();

    QString sessionName;
    QString returnName(){
        return sessionName;
    }
    QString returnSessionPath(){
        return settings->fileName();
    }

    QString returnCentroid(){
        QString cent=QString::number(X0)+" +- " + QString::number(X0Err);
        return cent;
    }

    //startup
    void setStartUpValues();
    //calculate allowed transition
    void calculateAllowedTransitions();
    //Calculate the transition frequencies
    void calculateTransitionFrequencies();
    //Calculate profile
    void calculateSpectrumProfile();
    //Calculate the full HFS spectrum from the start
    void calculateFullHFSspectrum();
    //Calculates the spectrum when the allowed transitions are known
    void calculateSpectrum();
    //Calculates a suitable x-axis for the hyperfine spectrum
    void calculateXRange();

    //open spectrum data
    void openSpectrum();

    //Plotting members
    //plot the calculated HFS
    void plotCalculatedHFS();
    //Plot the fitted curve
    void plotFitPlot();
    //Plot the residual curve
    void plotResidual();
    //Plot the spectrum on the hyperfinePlot and fitPlot when spectrum is loaded
    void plotSpectrum();

private:
    Ui_HFS *userI;
    HFSfunctions *hyperfineSpectrum;
    fileOperations *ioOperations;
    miscFunctions *uiOperations;
    setTable *resultTable;
    plotSet *hyperfinePlot;
    plotSet *fitPlot;
    plotSet *residualPlot;
    fitter *mainFitter;
    QSettings *settings;

    QString profile;                        //The profile to be fitted
    bool firstFitterRun;                    //True if the fitter has not been run, false if it has. Needs to be reet to true if fitting is reset;
    bool restored;                          //True of the session has been restored
    //Parameters for calculating the HFS spectrum
    //Spins, FHWM's,, intensity
    double jl;
    double ju;
    double nucSpin;
    double width;
    double width2;
    double intensity;
    double centerOfGravity;
    double bgOffset;

    //Experimental spectrum data
    //x and y range for the loaded spectrum
    QVector<double> xSpectrum;
    QVector<double> ySpectrum;
    int dataSize;
    int numberOfPeaks;
    //Parameters to tune the spectrum
    //A's  B'sn their ratios ratios and their booleans from the hfs tab
    double IAl;
    double IBl;
    double IAu;
    double IBu;
    double AuperAl;
    bool AuperAlbool;
    double BuperBl;
    bool BuperBlbool;
    bool showInidividualPeaks;
    //fiter parameters
    //Datat to be fitted + fit constants in C++ array required by the fitter
    double *xarray;
    double *yarray;
    double *sigma;                      //error for y
    double *lowerAlphas;
    double *lowerBetas;
    double *upperAlphas;
    double *upperBetas;
    double *peakIntensities;
    //Fit value locks
    bool X0Lock;                        //centroid lock
    bool Y0Lock;                     // y-offset lock
    bool AlLock;                        //AL lock
    bool BlLock;                        //Bl lock
    bool AuLock;                        //Au lock
    bool BuLock;                        //Bu lock
    bool wLock;                         //width lock
    bool ILock;                         //peak height lock
    bool w2Lock;                         //width lock
    //initial/fittedvalues  values s
    double chiSq;
    double X0;
    double X0Err;
    double Y0;
    double Y0Err;
    double Al;
    double AlErr;
    double Bl;
    double BlErr;
    double Au;
    double AuErr;
    double Bu;
    double BuErr;
    double w;
    double wErr;
    double I;
    double IErr;
    double w2;
    double w2Err;
    //fitter parameter
    int numberOfIterations;
    double absoluteError;
    double relativeError;
    //parameters for plotting
    double first;  //The first and the last value of an experimental spectrum
    double last;

    //Fit and spectrum data storages
    QVector<double> xData;                  //calculated x-data
    QVector<double> xRange;
    QVector<double> yData;
    QVector<double> calculatedSpectrumProfile;
    QVector<double> yError;
    QVector<double> fitCurve;
    QVector<double> residualCurve;
    QVector<double> transitionFrequencies;  //calculated transition frequencies are on index 5 of each QVector
    QVector<double> Alphal;                 //lower alphas
    QVector<double> Betal;                  //lower betas
    QVector<double> Alphau;                 //upper alphas
    QVector<double> Betau;                  //upper betas
    QVector<double> Is;                     //intensities for each peak
    QVector<double> Is90;                   //Intensities at 90 derees
    QVector<double> frequencies;            //fitted transition frequencies
    QVector<double> AsBs;                   //A and B values
    QVector<double> yzero;                  //background
    QList< QList<double> > transitionData;  // from HFSfunctions // alpha lower, beta lower, alpha upper, beta upper, intesity, int at 90,, pump fraction
    QString fitterOutputString;
    //data conversion members
    QVariant QListToQVariantList(QVector<double> A);
    QVector<double> QVariantListToQList(QVariantList A);
    void setDataToArrayFormat();    //set the data that is in Qt format, e.g. QVector to c++ array
    void setSpectrumDataToArrayFormat();    //set the data that is in Qt format, e.g. QVector to c++ array

    //fitting routine code
    void setInitialValues();
    void setLockedParameters();
    void setFitterControlParameters();
    void setFitResultsToUi(std::unordered_map<std::string, double> results);
    void allocateArrays();
    void allocateSpectrumArrays();
    void deleteArrays();
    void deleteSpectrumArrays();
    void deAllocateArrays();
    void deAllocateSpectrumArrays();





signals:

public slots:
    void onUispinSpinBoxClicked();                      //This gets called when either the jl, ju or nuclear spin value changes
    void onUiOtherSpectrumValueChanged();               // This gets called otherwise
    void onSpectrumLoaded();                            //loads and plots an experimental spectrum
    void onSpectrumCleared();                           //Clears the spectrum
    void onShowIndividualLorentzianClicked(bool state); //Shows and ides individual peaks
    //Fitting related members
    void onFitButtonClicked();                          //starts the fitting routine
    void onResetButtonClicked();                        //resets the fitter
    void onFitResultValueChanged();                     //adjusts the fitcurve accordingly and puts the new values as initial value for the next fitter run
    void onLockButtonClicked();                         //adjusts the locked fit parameters
};

#endif // SESSION_H
