
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "hfssession.h"

hfsSession::hfsSession(QObject *parent,Ui_HFS *ui, QString name) :
    QObject(parent)
{
    sessionName=name;
    userI=ui;
    transitionFrequencies.clear();
    transitionData.clear();
    hfsCalc = new HFSfunctions();
    fileOp = new fileOperations(this, userI, resultTable);
    convert = new dataConvert(this);
    uiOperations =new miscFunctions(this, userI);
    lFitter=NULL;
    firstFitterRun=true;


    //Setup initial values
    jl=0;
    ju=1;
    nucSpin=0;
    FWHM=50;
    intensity=300;
    offset=0;
    Al=10;
    Bl=0;
    Au=10;
    Bu=0;
    AuperAl=1;
    AuperAlbool=false;
    BuperBl=1;
    BuperBlbool=false;


    //Setup
    X0=0;
    Y0=0;
    Alfit=0;
    Blfit=0;
    Aufit=0;
    Bufit=0;
    wFit=0;
    IFit=0;

    profile="lorentzian";


}

void hfsSession::setFitterType(QString spectrumProfile){
    profile=spectrumProfile;
}

void hfsSession::hfsSetTable(setTable *table){
    resultTable=table;

}

void hfsSession::hfsSetPlots(plotSet *hfsPlot_p, plotSet *fitPlot_p, plotSet *residualPlot_p){
    hfsPlot=hfsPlot_p;
    fitPlot=fitPlot_p;
    residualPlot=residualPlot_p;
}

hfsSession::~hfsSession(){
    delete hfsCalc;
    delete fileOp;
    delete convert;
    delete uiOperations;
    if(lFitter!=NULL){
        delete lFitter;
    }


}

//Sets ui counter etc. values form the values stored within the class
void hfsSession::setHFSUiValues()
{
    //TODO: add ui redraw blocking
    userI->JLc->setValue(jl);
    userI->JUc->setValue(ju);
    userI->Ival->setValue(nucSpin);
    userI->width->setValue(FWHM);
    userI->offset->setValue(offset);
    userI->intensityscalerl->setValue(intensity);
    userI->Als->setValue(Al);
    userI->Alc->setValue(Al);
    userI->Bls->setValue(Bl);
    userI->Blc->setValue(Bl);
    userI->Aus->setValue(Au);
    userI->Auc->setValue(Au);
    userI->Bus->setValue(Bu);
    userI->Buc->setValue(Bu);
    userI->alauval->setValue(AuperAl);
    userI->scaleA->setChecked(AuperAlbool);
    userI->blbuval->setValue(BuperBl);
    userI->scaleB->setChecked(BuperBlbool);
    if(profile=="lorentzian"){
        userI->profileSelector->setCurrentIndex(0);
    }
    else if(profile=="gaussian"){
        userI->profileSelector->setCurrentIndex(1);
    }
}

//Gets thea counter etc. values from the ui and stores them to the class
void hfsSession::getHFSUiValues(){
    jl=userI->JLc->value();
    ju=userI->JUc->value();
    nucSpin=userI->Ival->value();
    FWHM=userI->width->value();
    intensity=userI->intensityscalerl->value();
    offset=userI->offset->value();
    Al=userI->Als->value();
    Bl=userI->Bls->value();
    Au=userI->Aus->value();
    Bu=userI->Bus->value();
    AuperAl=userI->alauval->value();
    AuperAlbool=userI->scaleA->isChecked();
    BuperBl=userI->blbuval->value();
    BuperBlbool=userI->scaleB->isChecked();
}

void hfsSession::setFitValuestoUi(){
    userI->X0->setValue(X0);
    userI->Y0->setValue(Y0);
    userI->AlFit->setValue(Alfit);
    userI->BlFit->setValue(Blfit);
    userI->AuFit->setValue(Aufit);
    userI->BuFit->setValue(Bufit);
    userI->wFit->setValue(wFit);
    userI->IFit->setValue(IFit);
}

void hfsSession::getInitialFitValues(){
    //Fitted parameter values
    X0=userI->X0->value();
    Y0=userI->Y0->value();
    Alfit=userI->AlFit->value();
    Blfit=userI->BlFit->value();
    Aufit=userI->AuFit->value();
    Bufit=userI->BuFit->value();
    wFit=userI->wFit->value();
    IFit=userI->IFit->value();

}

//calculates allowed transitions from the given spins
void hfsSession::calculateAllowedTransitions(){
    transitionData.clear();
    transitionData=hfsCalc->transitions(jl,ju, nucSpin);
}

//Calculates the transition frequencies using the A and B values stores within the class
void hfsSession::calculateTransitionFrequencies(){
    transitionFrequencies.clear();
    if(!transitionData.isEmpty()){
        transitionFrequencies=hfsCalc->transfreqs(transitionData, Au, Bu, Al, Bl,offset);
    }
    else{
        qDebug()<<"no allowed transitions available";
    }
}

QVector<double> hfsSession::calculateXRange(QList<double> frequencies, double fwhm){
    double high=0;
    double low=0;
    double xminus;  // for the plotting and calculation range
    double xplus;
    double interval;
    QVector<double> xrange;
    //search for first and the last transition
    for(int i=0;i<frequencies.size();i++){
        if(frequencies[i]>=high){
            high=frequencies[i];
        }
        else if(frequencies[i]<=low){
            low=frequencies[i];
        }
    }


    interval=fabs(low-high)+fwhm*10; //total width of the peak range plus fwhm times 10
    xminus=-interval+offset;
    xplus=interval+offset;
    for(double i=xminus; i<=xplus; i=i+0.1*fwhm){           //calculates unnecessarily fine range if the FWHM is small and A's and B's are large

        xrange<<i;
    }
    return xrange;
}

//Calculates the full spectrum,
void hfsSession::calculateFullHFSspectrum(){
    calculateAllowedTransitions();
    if(userI->inPeaks->isChecked()){
        hfsPlot->allocate(transitionData.size());   //Allocates curves for the individual peaks plots
    }
    calculateSpectrum();
    resultTable->replace(transitionData);

}

void hfsSession::calculateSpectrum(){
    calculateTransitionFrequencies();
    frequencies.clear();
    Is.clear();
    Is90.clear();
    foreach (QList<double> transition, transitionFrequencies) {
        frequencies<<transition.at(5);  //transition frequency
        Is<<transition.at(2);           //Standard intensity
        Is90<<transition.at(3);         //Intensity at 90 degree
    }
    QString currentProfile=userI->profileSelector->currentText();
    calculatedSpectrumX=calculateXRange(frequencies, FWHM);
    calculatedSpectrum=hfsCalc->returnProfile(currentProfile,calculatedSpectrumX, FWHM, intensity, Is90, frequencies);
    //The following plot the individual peaks if the inlo buton is pressed
    if(userI->inPeaks->isChecked()){

        if(currentProfile=="Lorentzian"){
            hfsPlot->setInd(hfsCalc->lorentzians(), calculatedSpectrumX);
        }
        else if(currentProfile=="Gaussian"){
            hfsPlot->setInd(hfsCalc->gaussians(), calculatedSpectrumX);
        }
    }
}
//Open a experimental spectrum
//At the moment requires a two column .dat file with tab as an separator
//The errors are taken as square root of the counts
void hfsSession::openSpectrum(){
    QVector<QVector<double> > openSpectrum;
    openSpectrum= fileOp->openSpectrum();
    xSpectrum.clear();
    ySpectrum.clear();
    if(!openSpectrum.isEmpty()){
    xSpectrum=openSpectrum.at(0);
    ySpectrum=openSpectrum.at(1);

    xarray= convert->QVectorToArray(xSpectrum);//x-axis data
    yarray=convert->QVectorToArray(ySpectrum); //data to be fitted
    sigma=convert->returnSQRTofArray(yarray, ySpectrum.size());
    }
    //Set the axis scales right for the fittingplot and and the residual plot

}


//plot the calculated HFS spectrum
void hfsSession::plotCalculatedHFS(){
    hfsPlot->mainPlot->setSamples(calculatedSpectrumX, calculatedSpectrum);
    userI->HFS_plot->replot();
}

//Plot the fitted data
void hfsSession::plotFitPlot(){

}

//Plot the fit residual
void hfsSession::plotResidual(){

}

//Plot the loaded spectrum
void hfsSession::plotSpectrum(){
    hfsPlot->spectrum->setSamples(xSpectrum, ySpectrum);
    userI->HFS_plot->replot();

    fitPlot->spectrum->setSamples(xSpectrum, ySpectrum);
    first=xSpectrum.first();
    last=xSpectrum.last();
    if(first>=last){
        first=xSpectrum.last();
        last=xSpectrum.first();
    }
    userI->fitPlot->setAxisScale(QwtPlot::xBottom,first,last);
    userI->residualPlot->setAxisScale(QwtPlot::xBottom,first,last);
    userI->fitPlot->replot();
}

void hfsSession::onUispinSpinBoxClicked(){
    calculateFullHFSspectrum();
    plotCalculatedHFS();


}
void hfsSession::onUiOtherSpectrumValueChanged(){
    calculateSpectrum();
    plotCalculatedHFS();
}

void hfsSession::onSpectrumLoaded(){
    openSpectrum();
    plotSpectrum();
}
void hfsSession::onSpectrumCleared(){
    xSpectrum.clear();
    xarray=NULL;
    ySpectrum.clear();
    yarray=NULL;
    plotSpectrum();
}

void hfsSession::onShowIndividualLorentzianClicked(bool state){
    if(state){
        calculateFullHFSspectrum();
    }
    else{
        QVector<double> nullv;
        QList<QVector<double> > nullvl;
        hfsPlot->allocate(0);
        hfsPlot->setInd(nullvl,nullv);
    }
    userI->HFS_plot->replot();
}

//Fitter slots
void hfsSession::onFitButtonClicked(){
    if(xSpectrum.size()!=0){
        int n;                  //number of datapoints to be fitted
        n=xSpectrum.size();

        //new code
        dataSize=xSpectrum.size();
        numberOfPeaks=transitionData.size();
        ///new code
        int peaks;              //number of peaks
        peaks=transitionData.size();
        int p;  //number of variables
        p=8;    // number of variables is fixed as all the peaks are fitted with same set of variables, namely the A's and B's ,
        //centroid, y offset, width and the intensity.
        noOfFitConstants=peaks;
        noOfFitParameters=p;
        lorentzianFitRoutine(firstFitterRun,n,p,peaks,xarray, yarray, sigma);

    }
}

void hfsSession::onResetButtonClicked(){
    firstFitterRun=true;
    if(xSpectrum.size()!=0){            //program crashes if the fitting has bee run once, hen the spectrum is cleared and then the rest fit button is clicked
        uiOperations->resetSpinboxValues(); //the crash only occusr if no spectrum is present
    }
    //fetch the fitted data and plot it
    QVector<double> zeroData;
    zeroData<<0;
    fitPlot->mainPlot->setSamples(zeroData, zeroData);
    userI->fitPlot->replot();
    residualPlot->mainPlot->setSamples(zeroData, zeroData);
    userI->residualPlot->replot();
    if(lFitter!=NULL && !firstFitterRun){
        delete lFitter;
        lFitter=NULL;
    }
}

void hfsSession::onFitResultValueChanged(){
    getInitialFitValues();
    QVector<double> reDrawnY;
    QVector<double> reDrawnResidual;
    int i=0;
    int arrs=transitionFrequencies.size();
    double alphal_f[arrs];
    double betal_f[arrs];
    double alphau_f[arrs];
    double betau_f[arrs];
    double I_f[arrs];

    foreach (QList<double> peak, transitionData) {
        alphal_f[i]=peak.at(2);
        betal_f[i]=peak.at(3);
        alphau_f[i]=peak.at(4);
        betau_f[i]=peak.at(5);
        I_f[i]=peak.at(6);
        i++;

    }
//    alphal_f=convert->QVectorToArray(Alphal);
//    betal_f=convert->QVectorToArray(Betal);
//    alphau_f=convert->QVectorToArray(Alphau);
//    betau_f=convert->QVectorToArray(Betau);
//    I_f=convert->QVectorToArray(Is90);

    if(lFitter!=NULL){
        reDrawnY=lFitter->lorentzianProfileCalculator(xSpectrum.size(),xarray, transitionFrequencies.size(), X0, Y0, Alfit,Blfit, Aufit,Bufit, wFit, IFit, I_f, alphal_f, betal_f, alphau_f, betau_f  );

        for(int length=0; length<reDrawnY.size();length++){
            reDrawnResidual<< (ySpectrum.at(length)-reDrawnY.at(length));
        }
        fitPlot->mainPlot->setSamples(xSpectrum,reDrawnY);            //plot the recalculated data
        userI->fitPlot->replot();

        residualPlot->mainPlot->setSamples(xSpectrum, reDrawnResidual);      //plot the recalculated residual
        userI->residualPlot->replot();

    }
}

//fitter functions
void hfsSession::lorentzianFitRoutine(bool firstRun, int n, int p, int peaks, double *xarray, double *yarray, double *sigma){
    if(firstRun){
        //new fit routine
        mainFitter = new fitter(profile.toStdString());
        mainFitter->setData(n, xarray, yarray, sigma, peaks);
        allocateArrays();
        setDataToArrayFormat();
        setInitialValues();
        setLockedParameters();
        //end new fit routine
        fitParameters=uiOperations->setLorentzianFitParameterInitialValues();               //get the initial values
        fitConstants = uiOperations->setFitConstants(transitionData);                                 //get the constant fit parameters
        //Start the fitting routine
        lFitter = new lorentzianFitter_OLD(this,n,p,peaks,yarray,xarray,sigma);           //create a new fitter, pass in the data size n, number of fit parameters p, data y, and the error sigma
        lFitter->setInitialValues(fitParameters, noOfFitParameters);                  //Pass the fit parameters and their number to the routine
        lFitter->setConstantValues(fitConstants,noOfFitConstants);                    // pass the constants to the fitter
        firstFitterRun=false;                                                           //set the firstlorentzianrun parameter to false after the first run

    }
    else{
        lFitter->setInitialValues(uiOperations->setLorentzianFitParameterReFitValues(), noOfFitParameters); //if te fitter has been run once already,, then the intiial values are fetched from the ui spinboxes

    }

    fitterTerminationConditions();                                                          //Set the fiter termination conditions
    lockedLorentzianFitParameters();                                                        //Set locked lorentzian fit parameters
    lFitter->mainFitter();                                                            //start fitting
    uiOperations->blockLorentzianFittingUpdates(true);                                      //block the counters
    uiOperations->setSpinBoxValues(lFitter->results());                               //set the results to the spinbox
    uiOperations->blockLorentzianFittingUpdates(false);                                     //unblock the counters
    fitPlot->mainPlot->setSamples(xSpectrum, lFitter->returnFitData());            //fetch the fitted dta and plot it
    userI->fitPlot->replot();

    //get the centroid data from the fitter;
    X0=lFitter->returnxc0f();
    X0Err=lFitter->returnxc0fErr();



    residualPlot->mainPlot->setSamples(xSpectrum, lFitter->returnResidualData());      //fetch the residual data and plot it
    userI->residualPlot->replot();
    fitterOutput=lFitter->fitterOutput;                                   //Set fittter output to the texteditbox
    foreach (QString line, fitterOutput) {
        userI->FitResultProgress->appendPlainText(line);
    }
}

void hfsSession::fitterTerminationConditions(){
    if(lFitter!=NULL){
        lFitter->setFitTerminationParameters(userI->IterCounter->value(), userI->epsrel->value(), userI->epsabs->value());
    }

}
void hfsSession::lockedLorentzianFitParameters(){
     if(lFitter!=NULL){
    lFitter->setLockedVariables(uiOperations->setLockedLorentzianVariables());
     }

}

void hfsSession::saveUiData(){
    getHFSUiValues();
    getInitialFitValues();
    //qDebug()<< sessionName;
}

void hfsSession::restoreUiData(){
    setHFSUiValues();
    setFitValuestoUi();
    plotSpectrum();
    if(!firstFitterRun){
        fitPlot->mainPlot->setSamples(xSpectrum, lFitter->returnFitData());            //fetch the fitted data and plot it
        userI->fitPlot->replot();

        residualPlot->mainPlot->setSamples(xSpectrum, lFitter->returnResidualData());      //fetch the residual data and plot it
        userI->residualPlot->replot();
    }
}


//New fit code

void hfsSession::setInitialValues(){
    IcenterOfMass=userI->X0->value();
    IyOffset=userI->Y0->value();
    IlowerA=userI->AlFit->value();
    IlowerB=userI->BlFit->value();
    IupperA=userI->AuFit->value();
    IupperB=userI->BuFit->value();
    Iwidth=userI->wFit->value();
    Iintensity=userI->IFit->value();
}

void hfsSession::setLockedParameters(){
    X0Lock=userI->X0Lock->isChecked();           //centroid lock
    yzeroLock=userI->Y0Lock->isChecked();         // y-offset lock
    AlLock=userI->AlLock->isChecked();            //AL lock
    BlLock=userI->BlLock->isChecked();             //Bl lock
    AuLock=userI->AuLock->isChecked();            //Au lock
    BuLock=userI->BuLock->isChecked();            //Bu lock
    wLock=userI->wLock->isChecked();             //width lock
    ILock=userI->Ilock->isChecked();             //peak height lock
}

void hfsSession::setFitterControlParameters(){
    numberOfIterations=userI->IterCounter->value();
    absoluteError=userI->epsabs->value();
    relativeError=userI->epsrel->value();
}

void hfsSession::allocateArrays(){
    xarray = new double[dataSize];
    yarray = new double[dataSize];
    sigma=new double[dataSize];
    lowerAlphas=new double[numberOfPeaks];
    lowerBetas=new double[numberOfPeaks];
    upperAlphas=new double[numberOfPeaks];
    upperBetas=new double[numberOfPeaks];
    peakIntensities=new double[numberOfPeaks];
}
void hfsSession::deleteArrays(){
    delete [] xarray;
    delete [] yarray;
    delete [] sigma;
    delete [] lowerAlphas;
    delete [] lowerBetas;
    delete [] upperAlphas;
    delete [] upperBetas;
    delete [] peakIntensities;
    deAllocateArrays();
}
void hfsSession::deAllocateArrays(){
    xarray=NULL;
    yarray=NULL;
    sigma=NULL;
    lowerAlphas=NULL;
    lowerBetas=NULL;
    upperAlphas=NULL;
    upperBetas=NULL;
    peakIntensities=NULL;
}

void hfsSession::setDataToArrayFormat(){
    for(int dp=0;dp<dataSize;dp++){
        xarray[dp]=xSpectrum.at(dp);
        yarray[dp]=ySpectrum.at(dp);
        sigma[dp]=sqrt(ySpectrum.at(dp));
    }
    int i=0;
    foreach (QList<double> peak, transitionData) {
            lowerAlphas[i]=peak.at(2);
            lowerBetas[i]=peak.at(3);
            upperAlphas[i]=peak.at(4);
            upperBetas[i]=peak.at(5);
            peakIntensities[i]=peak.at(6);
            i++;

        }

}
