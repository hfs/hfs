
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "setTable.h"
#include <QStandardItemModel>
#include <QClipboard>

setTable::setTable(QObject *parent, QTableView *hfsTable) :
    QObject(parent)
{
    //set-up the parameter table
      HFSmodel = new QStandardItemModel(0,9);
      HFSmodel->setHeaderData(0, Qt::Horizontal, QObject::tr("F lower"));
      HFSmodel->setHeaderData(1, Qt::Horizontal, QObject::tr("F upper"));
      HFSmodel->setHeaderData(2, Qt::Horizontal, QObject::tr("Alpha lower"));
      HFSmodel->setHeaderData(3, Qt::Horizontal, QObject::tr("Beta lower"));
      HFSmodel->setHeaderData(4, Qt::Horizontal, QObject::tr("Alpha upper"));
      HFSmodel->setHeaderData(5, Qt::Horizontal, QObject::tr("Beta upper"));
      HFSmodel->setHeaderData(6, Qt::Horizontal, QObject::tr("Intensity"));
      HFSmodel->setHeaderData(7, Qt::Horizontal, QObject::tr("Intensity at 90 deg"));
      HFSmodel->setHeaderData(8, Qt::Horizontal, QObject::tr("Pump fraction"));
      hfsTable->setModel(HFSmodel);

      hfsTable_d=hfsTable;

      item=NULL;
      tableItems.append(item);

}

setTable::~setTable(){
    foreach(QStandardItem *item_s, tableItems){
        if(item_s){

        delete item_s;
        }
    }
}

void setTable::replace(QList< QList<double> > data){

    foreach(QStandardItem *item_s, tableItems){
        if(item_s!=NULL){

        delete item_s;
            item_s=NULL;
        }
    }
    HFSmodel->removeRows(0,HFSmodel->rowCount());  //clear rows before inserting new
    tableItems.clear();
    int row;
    int column;
    row=0;


   foreach (QList<double> rowd, data) {
       //HFSmodel->setItem(0,0,row[0]);
       column=0;

       foreach (double item_val, rowd) {
           QString s;
           s=QString::number(item_val,'f');
           s.simplified();

           item = new QStandardItem(s);

           item->setData(s);
           item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
           tableItems.append(item);


           HFSmodel->setItem(row,column,item);

           column+=1;


       }
       row+=1;
   }

}
void setTable::copyToCP(){
    //copied from
    //Corwin Joy
    //from http://stackoverflow.com/questions/1230222/selected-rows-line-in-qtableview-copy-to-qclipboard with minor changes

    QItemSelectionModel *selectionModel =hfsTable_d->selectionModel();  //set the selectionmodel which holds the infromation about the selection
    QModelIndexList indexes = selectionModel->selectedIndexes();  //selected indexes
    clipboard = QApplication::clipboard();

    if(indexes.size() < 1)  //check if selection is empty
      return;


    //sort indexes first by row, the by column
     qSort(indexes);
    // You need a pair of indexes to find the row changes
    QModelIndex previous = indexes.first();
    indexes.removeFirst();
    QString selected_text;
    QModelIndex current;
    foreach(current, indexes)
    {
      QVariant data = HFSmodel->data(previous);
      QString text = data.toString();
      // At this point `text` contains the text in one cell
      selected_text.append(text);
      // If you are at the start of the row the row number of the previous index
      // isn't the same.  Text is followed by a row separator, which is a newline.
      if (current.row() != previous.row())
      {
        selected_text.append(QLatin1Char('\n'));
      }
      // Otherwise it's the same row, so append a column separator, which is a tab.
      else
      {
        selected_text.append(QLatin1Char('\t'));
      }
      previous = current;
    }

    // add last element
    selected_text.append(HFSmodel->data(current).toString());
    selected_text.append(QLatin1Char('\n'));
    clipboard->setText(selected_text);

}
