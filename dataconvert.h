#ifndef DATACONVERT_H
#define DATACONVERT_H

#include <QObject>
#include<QVector>
#include<QDebug>
#include <gsl/gsl_multifit_nlin.h>
class dataConvert : public QObject
{
    Q_OBJECT
public:
    explicit dataConvert(QObject *parent = 0);
    QVector<double> arrayToQVector(double *array, int arrSize);
    double *QVectorToArray(QVector<double> vector);
    double *GSLFitVectorToDouble(gsl_multifit_fdfsolver *vector);
    QList<double> GSLFitVectorToQList(gsl_multifit_fdfsolver *vector);
    QList<double> GSLMatrixToQList(gsl_matrix *matrix);
    double *returnSQRTofArray(double *array, int arrSize);

signals:

public slots:

};

#endif // DATACONVERT_H
