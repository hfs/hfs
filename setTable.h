#ifndef SETTABLE_H
#define SETTABLE_H

#include <QObject>
#include <QStandardItemModel>
 #include <QTableView>
#include <QApplication>
#include <QPrinter>
class setTable : public QObject
{
    Q_OBJECT
public:
    explicit setTable(QObject *parent = 0, QTableView *hfsTable=0);
    ~setTable();
    QStandardItemModel *HFSmodel;  //table for hfs data
    void replace(QList< QList<double> > data);
    void copyToCP();
    QTableView *hfsTable_d; //local pointer to the table widget
     QItemSelectionModel *selectionModel;
      QClipboard *clipboard;
      QStandardItem *item;

      QList<QStandardItem*> tableItems;

signals:

public slots:

};

#endif // SETTABLE_H
