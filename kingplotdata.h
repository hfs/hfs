#ifndef KINGPLOTDATA_H
#define KINGPLOTDATA_H

#include <QObject>
#include <QList>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_interval_symbol.h>
#include <qwt_symbol.h>
#include <qwt_plot_intervalcurve.h>
#include <QPointF>
#include <demingregression.h>
#include <QFile>

class kingPlotData : public QObject
{
    Q_OBJECT
public:
    explicit kingPlotData(QObject *parent = 0, QwtPlot *kingPlot=0);

    ~kingPlotData();

    void plotData();

    //set the nuclei under investigation and its first and second reference isotope mass number
    void setNucleiData(int Z_o, int A1_ov, int A2_ov, bool modifyx, bool modifyy);

private:

    void modifyData();

    void openMassTable();

    double returnMass(int Z, int A);

    double calculateReferencePair();

    double calculateMassFactor(int A_i, int A_j);

    QString masses[119][294];

    //nuclei and the reference isotopes
    int Z;
    int A1;
    int A2;

    //If true, modify data

    bool willModifyX;
    bool willModifyY;

    //original data
    QVector<double> A1_o;
    QVector<double> A2_o;
    QVector<double> x_o;
    QVector<double> dx_o;
    QVector<double> y_o;
    QVector<double> dy_o;
    //modified data
    QVector<QPointF> xy_m;
    QVector<QPointF> dxdy_m;
    QString modifiedDataOutput;


    //plot
    QwtPlot *kingPlotWidget;
    QwtPlotCurve *kingPlotCurve;
    QwtPlotCurve *fitCurve;
    QwtSymbol *symbol;
    QwtPlotIntervalCurve *kingPlotYErrorBar;
    QwtPlotIntervalCurve *kingPlotXErrorBar;
    QwtIntervalSymbol *XerrorBar;
    QwtIntervalSymbol *YerrorBar;
    QVector<QwtIntervalSample> yErrorBar;
    QVector<QwtIntervalSample> xErrorBar;

    //Demingregression

    demingRegression *fitData;

signals:
    void sendFitOutPut(QString data);
public slots:
    void setOriginalData(QVector<double> A1, QVector<double> x, QVector<double> dx, QVector<double> A2, QVector<double> y, QVector<double> dy);
    void fitKingPlot();
};

#endif // KINGPLOTDATA_H
