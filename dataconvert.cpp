

//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#include "dataconvert.h"


dataConvert::dataConvert(QObject *parent) :
    QObject(parent)
{
}

//Convert array to Qvector
QVector<double> dataConvert::arrayToQVector(double *array, int arrSize){

    QVector<double> data;

    for(int i=0;i<arrSize;i++){
        data<<array[i];

    }
    return data;
}

//Convert QVector to array

double *dataConvert::QVectorToArray(QVector<double> vector){

    double *array= new double[vector.size()];
    int i=0;
    foreach (double unit, vector) {
        array[i]=unit;
        i++;
    }
    return array;
}
//Convert gsl multifit vector to array

double *dataConvert::GSLFitVectorToDouble(gsl_multifit_fdfsolver *vector){
    int length=vector->x->size;
    double *array=new double[length];
    for(int i=0;i<length;i++){
        array[i]=gsl_vector_get(vector->x,i);

    }
    return array;

}
//Convert gsl multifit vector to QList

QList<double> dataConvert::GSLFitVectorToQList(gsl_multifit_fdfsolver *vector){
    int length=vector->x->size;
    QList<double> list;
    for(int i=0;i<length;i++){
        list<<gsl_vector_get(vector->x,i);
    }
    return list;
}

//Convert GSL covariont matrix diagonal into a QList

QList<double> dataConvert::GSLMatrixToQList(gsl_matrix *matrix){
    int dimension=matrix->size1;
    QList<double> diagonal;
    for(int i=0; i<dimension;i++){
        diagonal<<gsl_matrix_get(matrix,i,i);
    }
    return diagonal;
}

//return array with all the elements square rooted
double *dataConvert::returnSQRTofArray(double *array, int arrSize){
    double *sigma =new double[arrSize];
    for(int i=0; i<arrSize; i++){
        if(array[i]==0){
            sigma[i]=1;
        }
        else{
            sigma[i]=sqrt(array[i]);

        }
    }
    return sigma;
}
