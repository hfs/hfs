
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


/* C implementations of various lineshape functions
 *
 * Copyright (C) 2002,2003 Jochen K�pper <jochen@jochen-kuepper.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * Modifications by Mikael Reponen<mikael.reponen@gmail.com> 2011-
 */




#include "lineshapes.h"

lineShapes::lineShapes(QObject *parent) :
    QObject(parent)
{
}





#define sqr(x) (pow(x,2))


/*** C implementation ***/

void lineShapes::gauss(size_t n, double *x, double *y, double w, double xc)
    /* Evaluate normalized Gauss profile around xc with FWHM w at all x_i,
       return in y. */
{

    for(int i=0; i<n; i++)
        y[i] = 2. * sqrt(M_LN2/M_PI) / w * exp(-4.*M_LN2 * sqr((x[i]-xc)/w));
}



void lineShapes::lorentz(size_t n, double *x, double *y, double w, double xc)
    /* Evaluate normalized Lorentz profile around xc with FWHM w at all x_i,
       return in y. */
{

    for(int i=0; i<n; i++)
        y[i] = 2.*w/M_PI / (sqr(w) + 4.*(sqr(x[i]-xc)));
}



double lineShapes::humlicek_v12(double x, double y)
    /** Approximation of Voigt profile by Humlicek's 12-point formula.
     *
     * J. Humlicek, J. Quant. Spectrosc. Radiat. Transfer, 21(1978), 309.
     *
     * Voigt-Profil:
     * V(x, y) = 2/pi^(1.5) * y^2/FWHM_L * \int[-inf,+inf](e^(-y^2)/(x+y)^2+...)
     */
{
    static const double T_v12[6] = {
        0.314240376254359,    0.947788391240164,    1.597682635152605,
        2.27950708050106,     3.020637025120890,    3.889724897869782
    };
    static const double alpha_v12[6] = {
       -1.393236997981977,   -0.231152406188676,    0.155351465642094,
       -6.21836623696556e-3, -9.190829861057113e-5, 6.275259577497896e-7
    };
    static const double beta_v12[6] = {
        1.011728045548831,   -0.751971469674635,    1.255772699323164e-2,
        1.0022008145159e-2,  -2.42068134815573e-4,  5.008480613664573e-7
    };
    static const double  y0_v12 = 1.50;
    double yp, xp, xm, sum, yp2, xp2, xm2;


    sum = 0.;
    yp = y + y0_v12;
    yp2 = yp * yp;
    if((y > 0.85) || (fabs(x) < (18.1 * y + 1.65))) {
        /* Bereich I */
        for(int k=0; k<6; k++) {
            xp = x + T_v12[k];
            xm = x - T_v12[k];
            sum += ((alpha_v12[k] * xm + beta_v12[k] * yp) / (xm * xm + yp2)
                    + (beta_v12[k] * yp - alpha_v12[k] * xp) / (xp * xp + yp2));
        }
    } else {
        /* Bereich II */
        for(int k=0; k<6; k++) {
            xp = x + T_v12[k];
            xp2 = xp * xp;
            xm = x - T_v12[k];
            xm2 = xm * xm;
            sum += (((beta_v12[k] * (xm2 - y0_v12 * yp) - alpha_v12[k] * xm * (yp + y0_v12))
                     / ((xm2 + yp2) * (xm2 + y0_v12 * y0_v12)))
                    + ((beta_v12[k] * (xp2 - y0_v12 * yp) + alpha_v12[k] * xp * (yp + y0_v12))
                       / ((xp2 + yp2) * (xp2 + y0_v12 * y0_v12))));
        }
        if(fabs(x) < 100.)
            sum = y * sum + exp(-pow(x, 2));
        else
            sum *= y;
    }
    return sum;
}


 void lineShapes::voigt(size_t n, double *x, double *y, double w[2], double xc)
    /* Evaluate normalized Voigt profile at x around xc with Gaussian
     * linewidth contribution w[0] and Lorentzian linewidth
     * contribution w[1].
     */
{
    /* Transform into reduced coordinates and call Humlicek's 12 point
     * formula:
     *     x = 2 \sqrt{\ln2} \frac{\nu-\nu_0}{\Delta\nu_G}
     *     y = \sqrt{\ln2} \frac{\Delta\nu_L}{\Delta\nu_G}
     */

    double yh = sqrt(M_LN2) * w[1] / w[0];
    for(int i=0; i<n; i++) {
        double xh = 2. * sqrt(M_LN2) * (x[i]-xc) / w[0];
        y[i] = 2.*sqrt(M_LN2/M_PI)/w[0] * humlicek_v12(xh, yh);
    }
}

