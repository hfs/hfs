#ifndef DEMINGREGRESSION_H
#define DEMINGREGRESSION_H

#include <QObject>
#include <QVector>
#include <math.h>
#include <QPointF>
#include <QDebug>
#include <QString>
//Calculates general Deming regression of and straight line using the York method
//American Journal of Physics -- March 2004 -- Volume 72, Issue 3, pp. 367
//Unified equations for the slope, intercept, and standard errors of the best straight line
//Derek York1, Norman M. Evensen1, Margarita López Martínez2, and Jonás De Basabe Delgado2


class demingRegression : public QObject
{
    Q_OBJECT
public:
    explicit demingRegression(QObject *parent = 0);
    void setData(QVector<QPointF> xy, QVector<QPointF> dxdy){
        x_m.clear();
        dx_m.clear();
        y_m.clear();
        dy_m.clear();

        for(int i=0; i<xy.size();i++){
            x_m.append(xy.at(i).x());
            dx_m.append(dxdy.at(i).x());
            y_m.append(xy.at(i).y());
            dy_m.append(dxdy.at(i).y());
        }
        N=x_m.size();
        //qDebug()<< dx_m<<dy_m;
    }
    QVector<QPointF> returnFitCurve();
    QString returnFitParameters();
    int iterations;
signals:

private:
    QVector<double> x_m;
    QVector<double> dx_m;
    QVector<double> y_m;
    QVector<double> dy_m;
    int N;

    double average(QVector<double> x);
    double sxy(QVector<double> x, QVector<double> y);
    QString fitParameters;

    //y=aD+bD*x;
    //The erros on the centroids are taken as the variances of the points in both x and y directions
    //The weight of a point(xi, yi) is then calculated using those

    double initialSlopeEstimate();
    double weightedAverage(QVector<double> dataSet, QVector<double> dataSetWeights);  //calculates weighted average
    QVector<double> pointWeights(double slope);                                       //calculates weight for points
    QVector<double> B(QVector<double> weights, QVector<double> xDots                 //B factor
                       , QVector<double> yDots, double slope);
    QVector<double> dotPoints(QVector<double> dataSet,double wAve_v);                 //substraction of datapoint and the weightd average, for variance
    double bD(QVector<double> wi,QVector<double> Bi, QVector<double> ydot, QVector<double> xdot); //slope
    double iterBD;
    double aD(double slope, double yWave, double xWave);                                           //intercept
    double iterAD;

    //Error estimation for the fit
    //
    double varBD(QVector<double> weights,QVector<double> xdot);
    double iterBDErr;
    double varAD(QVector<double> weights, double slopeErr, double xWave);
    double iterADErr;
    void demingIteration();
    QVector<double> adjustedValues(double xWave, QVector<double> zeeS);

    //Goodness of the fit
    double sN(QVector<double> weights,double slope, double intercept);
    double S;

public slots:

};

#endif // DEMINGREGRESSION_H
