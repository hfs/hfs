
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "lorentzianfitfunctions.h"
lorentzianFitFunctions::lorentzianFitFunctions()
{
}
double lorentzianFitFunctions::hfsCenterOfGravity(double xcz_fVal, double Al_fVal, double Bl_fVal, double Au_fVal, double Bu_fVal, double all_fVal, double bel_fVal, double alu_fVal, double beu_fVal){
    double cent=xcz_fVal+alu_fVal*Au_fVal+beu_fVal*Bu_fVal-all_fVal*Al_fVal-bel_fVal*Bl_fVal;
    return cent;
}
double lorentzianFitFunctions::lorentzianFunction(double t_fVal, double hfsCenterOfGravity_fVal, double yzero_fVal, double w_fVal, double I_fVal)
{

    double y0=yzero_fVal;
    double a=I_fVal;
    double x=t_fVal;
    double b= hfsCenterOfGravity_fVal;
    double c=w_fVal;
    return (pow(c,2)*a / (pow(c,2) + (pow((x-b),2))));
}

double lorentzianFitFunctions::lorentzianWDerivative(double t_fVal, double hfsCenterOfGravity_fVal, double w_fVal, double I_fVal){
    double a=I_fVal;
    double x=t_fVal;
    double b= hfsCenterOfGravity_fVal;
    double c=w_fVal;
    return (2*a*pow((x-b),2)*c)/pow((pow((x-b),2) + pow(c,2)),2);
}

double lorentzianFitFunctions::lorentzianIDerivative(double t_fVal, double hfsCenterOfGravity_fVal, double w_fVal){
    double x=t_fVal;
    double c=w_fVal;
    double b= hfsCenterOfGravity_fVal;
    return pow(c,2)/(pow((x-b),2) + pow(c,2));
}

double lorentzianFitFunctions::lorentzianCentDerivative(double t_fVal, double hfsCenterOfGravity_fVal,double w_fVal, double I_fVal){
    double a=I_fVal;
    double x=t_fVal;
    double b= hfsCenterOfGravity_fVal;
    double c=w_fVal;
    //(2*I_fVal*(-c - D - E - F - G + t) w^2)/((c + D + E + F + G - t)^2 + w^2)^2;
    return (2*a*(x-b)*pow(c,2))/pow((pow((x-b),2) + pow(c,2)),2);
}
double lorentzianFitFunctions::lorentzianYZeroDerivative(){
    return 1;
}

int lorentzianFitFunctions::lorentzian_f(const gsl_vector *x, void *data, gsl_vector *f){
    //get fit constants and other non-fitted parameters from the data struct
    size_t n = ((struct lorentzianFitFunctions::data*)data)->n;
    double *y = ((struct lorentzianFitFunctions::data*)data)->y;
    double *sigma = ((struct lorentzianFitFunctions::data*)data)->sigma;
    int peaks=((struct lorentzianFitFunctions::data*)data)->peaks;
    double *xdata=((struct lorentzianFitFunctions::data*)data)->xdata;

    //fetch fit constants
    double *alphal=((struct lorentzianFitFunctions::data*)data)->alphal;
    double *betal=((struct lorentzianFitFunctions::data*)data)->betal;
    double *alphau=((struct lorentzianFitFunctions::data*)data)->alphau;
    double *betau=((struct lorentzianFitFunctions::data*)data)->betau;
    double *I_const=((struct lorentzianFitFunctions::data*)data)->I;

    // shared fitparameters ar stored in a gsl_vector
    double x0= gsl_vector_get (x, 0);
    double yzero= gsl_vector_get (x, 1);
    double Al= gsl_vector_get (x, 2);
    double Bl= gsl_vector_get (x, 3);
    double Au= gsl_vector_get (x, 4);
    double Bu= gsl_vector_get (x, 5);
    double w=gsl_vector_get (x, 6);
    double I=gsl_vector_get (x, 7);


    size_t i;           //size of the data array
    //run trough the peaks, tis is the new multipeak function
    for(i=0;i<n;i++){
        double t=xdata[i];
        double Yi=0;
        for(int peak=0; peak<peaks; peak++){
            double centerOfGravity=hfsCenterOfGravity(x0, Al, Bl, Au, Bu, alphal[peak], betal[peak], alphau[peak], betau[peak]);
            Yi=Yi+lorentzianFunction(t, centerOfGravity, yzero, w, I_const[peak]);

        }
        Yi=I*Yi+yzero; // put the intensity scaler and background here rather than to individual peaks
        gsl_vector_set (f, i, (Yi - (y[i]))/sigma[i]);
    }
    return GSL_SUCCESS;
}
int lorentzianFitFunctions::lorentzian_fd(const gsl_vector *x, void *data, gsl_matrix *J){

    //get fit constants and other non-fitted parameters from the data struct
    size_t n = ((struct lorentzianFitFunctions::data*)data)->n;

    double *sigma = ((struct lorentzianFitFunctions::data*)data)->sigma;
    int peaks=((struct lorentzianFitFunctions::data*)data)->peaks;
    double *xdata=((struct lorentzianFitFunctions::data*)data)->xdata;
    double *y = ((struct lorentzianFitFunctions::data*)data)->y;
    //fetch fit constants
    double *alphal=((struct lorentzianFitFunctions::data*)data)->alphal;
    double *betal=((struct lorentzianFitFunctions::data*)data)->betal;
    double *alphau=((struct lorentzianFitFunctions::data*)data)->alphau;
    double *betau=((struct lorentzianFitFunctions::data*)data)->betau;
    double *I_const=((struct lorentzianFitFunctions::data*)data)->I;

    //fetch parameter locks
    bool X0Lock=((struct lorentzianFitFunctions::data*)data)->X0Lock;
    bool yzeroLock=((struct lorentzianFitFunctions::data*)data)->yzeroLock;
    bool AlLock=((struct lorentzianFitFunctions::data*)data)->AlLock;
    bool BlLock=((struct lorentzianFitFunctions::data*)data)->BlLock;
    bool AuLock=((struct lorentzianFitFunctions::data*)data)->AuLock;
    bool BuLock=((struct lorentzianFitFunctions::data*)data)->BuLock;
    bool wLock=((struct lorentzianFitFunctions::data*)data)->wLock;
    bool ILock=((struct lorentzianFitFunctions::data*)data)->ILock;

    // shared fitparameters are stored in a gsl_vector
    double x0= gsl_vector_get (x, 0);
    double yzero= gsl_vector_get (x, 1);
    double Al= gsl_vector_get (x, 2);
    double Bl= gsl_vector_get (x, 3);
    double Au= gsl_vector_get (x, 4);
    double Bu= gsl_vector_get (x, 5);
    double w=gsl_vector_get (x, 6);
    double I=gsl_vector_get (x, 7);
    size_t i;
    // jacobian
    for(i=0;i<n;i++){   //for each datapoint
        double t = xdata[i];
        //for each shared variable namely  xcz << yzero<< AsBs
        // there are 8 shared fit parameters
        //derivate against centroid, the function is the same for all xcz, Al, Bl, Au and Bu
        double YX0=0;     //Derivative of the x-offset
        double YyZero=0;        //Derivative of the y offset
        double Yal=0;           //Derivative of the Al
        double Ybl=0;           //Derivative of the Bl
        double Yau=0;           //Derivative of the Au
        double Ybu=0;           //Derivative of the Bu
        double Ywidth=0;        //Derivative of the width
        double Yintensity=0;    //Derivative of the intensity
        for(int peak=0;peak<peaks; peak++){
            double centerOfGravity=hfsCenterOfGravity(x0, Al, Bl, Au, Bu, alphal[peak], betal[peak], alphau[peak], betau[peak]);
            double LCDer=lorentzianCentDerivative(t, centerOfGravity, w, I*I_const[peak]);
            // as the centroid parameters, xcz, AL, Bl, Au and Bu are sahred by each lorentzian forming the sumfunction,
            // the derivate of the sumfunction is a su of the derivatives. The same goes also for the zeropiont yzero,
            // its just the number of peaks
            YX0=YX0+LCDer;
            YyZero=YyZero+lorentzianYZeroDerivative();
            Yal=Yal+LCDer*-alphal[peak];
            Ybl=Ybl+LCDer*-betal[peak];
            Yau=Yau+LCDer*alphau[peak];
            Ybu=Ybu+LCDer*betau[peak];
            Ywidth=Ywidth+lorentzianWDerivative(t, centerOfGravity, w, I*I_const[peak]);
            Yintensity=Yintensity+lorentzianIDerivative(t, centerOfGravity, w)*I_const[peak];
        }

        //if the derivative against the parameter is zero,
        //the fit routine thinks that the parameter is already at its optimal value.
        //If now the derivative is set to zero artificially, the value of the parameter is not changed.
        if(X0Lock){
            YX0=0;
        }

        if(yzeroLock){
            peaks=0;
        }

        if(AlLock){
            Yal=0;
        }

        if(BlLock){
            Ybl=0;
        }

        if(AuLock){
            Yau=0;
        }

        if(BuLock){
            Ybu=0;
        }
        if(wLock){
           Ywidth=0;
        }
        if(ILock){
            Yintensity=0;
        }
        gsl_matrix_set (J, i, 0,YX0/sigma[i]); // /sigma[i]
        gsl_matrix_set (J, i, 1, YyZero/sigma[i]);
        gsl_matrix_set (J, i, 2,  Yal/sigma[i]);
        gsl_matrix_set (J, i, 3,  Ybl/sigma[i]);
        gsl_matrix_set (J, i, 4,  Yau/sigma[i]);
        gsl_matrix_set (J, i, 5,  Ybu/sigma[i]);
        gsl_matrix_set (J, i, 6,  Ywidth/sigma[i]);
        gsl_matrix_set (J, i, 7,  Yintensity/sigma[i]);

    }

    return GSL_SUCCESS;

}
int lorentzianFitFunctions::lorentzian_fdf(const gsl_vector *x, void *data, gsl_vector *f, gsl_matrix *J){

    lorentzianFitFunctions::lorentzian_f(x,data,f);
    lorentzianFitFunctions::lorentzian_fd(x, data, J);

    return GSL_SUCCESS;
}
