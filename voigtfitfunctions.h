#ifndef VOIGTFITFUNCTIONS_H
#define VOIGTFITFUNCTIONS_H
#include <gsl/gsl_math.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_blas.h>
#include <math.h>
//Voigt profile and derivatives adapted from
//A.B. McLean, C.E.J. Mitchell, D.M. Swanston, Implementation of an efficient analytical approximation to the Voigt function for photoemission lineshape analysis, Journal of Electron Spectroscopy and Related Phenomena, Volume 69, Issue 2, 29 September 1994, Pages 125-132, ISSN 0368-2048, 10.1016/0368-2048(94)02189-7.
//(http://www.sciencedirect.com/science/article/pii/0368204894021897)
// Keywords: Accuracy; Approximation; Lineshape analysis; Novel; PC; Voigt
class voigtFitFunctions
{
public:
    explicit voigtFitFunctions();
    struct data {
        size_t n;              //number of functions == number of datapoints
        double *y;             //data array, contains the data to be fitted
        double *xdata;         //the x- axis data for the calculation of the functions
        double *sigma;         //standard deviation or error of each datapoint
        int peaks;             //number of peaks
        //alphas and betas as an array
        double *alphal;
        double *betal;
        double *alphau;
        double *betau;
        //peak intensities
        double *I;
        //Boolens containing information on wheter a certain parameter is locked or not
        bool X0Lock;           //centroid lock
        bool yzeroLock;         // y-offset lock
        bool AlLock;            //AL lock
        bool BlLock;             //Bl lock
        bool AuLock;            //Au lock
        bool BuLock;            //Bu lock
        bool wLock;             //gaussian width lock
        bool ILock;             //peak height lock
        bool w2Lock;             //lorentzian width lock
    };

    //returns the center of a peak
    double hfsCenterOfGravity(double xcz_fVal,double Al_fVal,double Bl_fVal,double Au_fVal,double Bu_fVal,double all_fVal,double bel_fVal,double alu_fVal,double beu_fVal);
    // returns a value calculated with a voigt with HFS parameters put in
    double voigtFunction(double lorentzianAmplitude, double peakPosition, double lorentzianFWHM, double gaussianFWHM, double x,double yOffset);
    //Voigt derivative.x ins the point where the derivative is calculated.
    //a is the magnitude of the Voigt function at x
    //y contains the Voigt parameters in the order Lorentzian amplitude, position, Lorentzian FWHM,
    //Gaussian FWHM. DyDa is a one-dimensional matrix of type float which contains
    //the parameter derivatives. M is the number of peaks(not used at the moment as the derivatives for individual peaks are summed
    void voigtDerivatives(double t,double* a,double* y,double* dyda, int m);

    int voigt_f(const gsl_vector *x, void *data, gsl_vector *f);
    int operator()(const gsl_vector *x, void *data, gsl_vector *f){return voigt_f(x, data, f);}
    //the analytical derivates of the function in a form of a Jacobian matrix
    int voigt_fd(const gsl_vector * x, void *data, gsl_matrix * J);
    int operator()(const gsl_vector * x, void *data, gsl_matrix * J){return voigt_fd(x, data, J);}
    //a function which calls both of these and its operator
    int voigt_fdf(const gsl_vector * x, void *data,gsl_vector * f, gsl_matrix * J);
    int operator()(const gsl_vector * x, void *data,gsl_vector * f, gsl_matrix * J){return voigt_fdf(x, data,f, J);}
};

#endif // VOIGTFITFUNCTIONS_H
