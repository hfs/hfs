#ifndef FITTER_H
#define FITTER_H
#include "gaussianfitter.h"
#include "lorentzianfitter.h"
#include "voigtfitter.h"
#include "string"
#include <unordered_map>
#include <vector>
#include <stdio.h>
#include <iostream>
class fitter
{
public:
    explicit fitter(std::string profile);
    ~fitter();
    void setData(int dataSize, double *xarray,double *yarray, double *sigma, int numberOfPeaks);
    void setConstants(double *lowerAlphas,double *lowerBetas,double *upperAlphas,double *upperBetas,double *intensities);
    void setInitialValues(double centerOfMass, double yOffset, double AL, double BL, double AU, double BU, double width, double intensity, double width2=0);
    void setLockedParameters(bool X0Lock, bool yzeroLock, bool AlLock, bool BlLock, bool AuLock, bool BuLock, bool wLock, bool ILock, bool w2Lock);
    void setFitterControlParameters(double numberOfIterations, double absoluteError, double relativeError);
    void setSolver();
    void iterateSolver();
    void clearSolver();

    std::vector<double> returnProfile(double X0_val, double yOffset_val, double Al_val, double Bl_val, double Au_val, double Bu_val, double w_val, double I_val, double w2_val);
    std::unordered_map<std::string, double> returnResult();
    std::unordered_map<std::string, std::string> returnFitterStatus();

private:
    std::string profileType;
    void *fitProfile;
    gaussianFitter *gaussian;
    lorentzianFitter *lorentzian;
    voigtFitter *voigt;
    //Maximum and minimum value for the x-axis of the loaded spectrum
    double first, last;
    //array size
    int IdataSize;
    int InumberOfPeaks;
    //same in C++ array required by the fitter
    double *Ixarray;
    double *Iyarray;
    double *Isigma;          //error for y
    double *IlowerAlphas;
    double *IlowerBetas;
    double *IupperAlphas;
    double *IupperBetas;
    double *IIntensities;
    bool IX0Lock;           //centroid lock
    bool IyzeroLock;         // y-offset lock
    bool IAlLock;            //AL lock
    bool IBlLock;             //Bl lock
    bool IAuLock;            //Au lock
    bool IBuLock;            //Bu lock
    bool IwLock;             //width lock, gaussian width for voigt profile
    bool IILock;             //peak height lock
    bool Iw2Lock;             //width lock, lorentzian width for voigt profile
    //initial values
    double IcenterOfMass;
    double IyOffset;
    double IlowerA;
    double IlowerB;
    double IupperA;
    double IupperB;
    double Iwidth;          //gaussian width for voigt profile
    double Iintensity;
    double Iwidth2;         //lorentzian width for voigt profile
    //fitter parameter
    double InumberOfIterations;
    double IabsoluteError;
    double IrelativeError;



};

#endif // FITTER_H
