#ifndef FILEPARSER_H
#define FILEPARSER_H

#include <QObject>
#include <QFile>
#include <QIODevice>
#include <QVector>
#include <QList>
 #include <QStringList>
#include <QDebug>
#include <QMessageBox>
#include<QTextStream>

class fileParser : public QObject
{
    Q_OBJECT
public:
    explicit fileParser(QString format, QStringList  filenames , QObject *parent );
    ~fileParser();


    QString filename;  //Single file

    QList<QVector<double> > parser; //data
    QList<QVector<double> > data(){
        return parser;
    }

    //Mainz format datavectors
    QVector<double> frequency;
    QVector<double> counts;
    QVector<double> jitter;
    double max_cps; //maximum countrate
    //////////////////////////

signals:

public slots:

};

#endif // FILEPARSER_H
