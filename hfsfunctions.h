#ifndef HFSFUNCTIONS_H
#define HFSFUNCTIONS_H
#include <stdio.h>
#include <QObject>
#include <QVector>
#include <QList>
#include <QDebug>
#include <QString>
#include <gsl/gsl_sf_coupling.h>        //6j symbols


#include <math.h>
//Voigt profile and derivatives adapted from
//A.B. McLean, C.E.J. Mitchell, D.M. Swanston, Implementation of an efficient analytical approximation to the Voigt function for photoemission lineshape analysis, Journal of Electron Spectroscopy and Related Phenomena, Volume 69, Issue 2, 29 September 1994, Pages 125-132, ISSN 0368-2048, 10.1016/0368-2048(94)02189-7.
//(http://www.sciencedirect.com/science/article/pii/0368204894021897)
// Keywords: Accuracy; Approximation; Lineshape analysis; Novel; PC; Voigt


#define _USE_MATH_DEFINES
class HFSfunctions: public QObject
{
    Q_OBJECT
public:
    HFSfunctions();
    //virtual ~HFSfunctions();
    //class member declarations adapted from the Bradley Cheals racah.py program

    double sixj(double a, double b, double c,double d,double e,double f);
    QVector< QVector<float> > intensities(QList<double> fl,QList<double> fu,double nucspin,double jl,double ju);          //returns 2D array containing the peak intensities
    float alpha(double f,double i,double j);
    float beta(double f,double i,double j);
    float gamma(double f,double i,double j);
    QList< QList<double> > transitions(double jl,double ju,double ns);  //returns 2D array containing the transitions
    QList< QList<double> > transfreqs( QList< QList<double> > t, double au, double bu, double al,double bl,double w);
    double spectrum(double x,double fwhm,double intensity,QList<double> w,QList<double> i);


    //spectrum creation
    //  x range,     peak width(STD for gaussian), inenstity scaler, relative peak intensities, peak centroids
    //QVector<double> x, double fwhm, double intensity, QList<double> i, QList<double> w);
    //Lorenzian profile
    QVector<double> lorentzian(QVector<double> x, double fwhm, double intensity,double bgOffset, QList<double> i, QList<double> w);
    QList<QVector<double> > lorentzians(); //function returns separate lorentzian for the plot
    QList<QVector<double> > lortz;       //the Qlist holding the individual lorentzians
    //Gaussian profile
    QVector<double> gaussian(QVector<double> x, double fwhm, double intensity,double bgOffset, QList<double> i, QList<double> w);
    QList<QVector<double> > gaussians(); //function returns separate lorentzian for the plot
    QList<QVector<double> > gauss;       //the Qlist holding the individual gaussians
    //Voigt profile
    double voigtProfile(double lorentzianAmplitude, double peakPosition, double loreantzianFWHM, double gaussianFWHM, double x);
    QVector<double> voigt(QVector<double> x, double gausw, double lorw, double intensity,double bgOffset, QList<double> i, QList<double> w);
    QList<QVector<double> > voigts(); //function returns separate lorentzian for the plot
    QList<QVector<double> > voig;       //the Qlist holding the individual voigts

    QVector<double> returnProfile(QString profile,QVector<double> x, double fwhm, double intensity_d,double bgOffset, QList<double> i, QList<double> w, double fwhm2=0);
    QVector<double> returnSpectrumProfile(QString profile,QVector<double> x, double fwhm, double intensity_d, double bgOffset, QVector<double> i, QVector<double> w, double fwhm2=0);


private:
    // needed mainly in function "transitions"
    double ns;  //nuclear spin
    double jl;  //spin for the lower state
    double ju;  //upper state spin

};




#endif // HFSFUNCTIONS_H
