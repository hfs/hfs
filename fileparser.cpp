
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#include "fileparser.h"

fileParser::fileParser(QString format, QStringList filenames, QObject *parent) :
    QObject(parent)

{
    if(format=="M"){  //Mainz dataformat

        foreach(const QString &str, filenames){
            QFile file(str);
            if (file.open(QIODevice::ReadWrite)){

                max_cps=0.1;
                while (!file.atEnd()) {
                    QString line = file.readLine();

                    QStringList split=line.split("\t");
                    QStringList split2=split.at(0).split(",");
                    QLocale::setDefault(QLocale::Finnish);
                    //qDebug()<< split2[0].simplified().toDouble();
                    //qDebug()<< split2.size();
                    if(split2.size()==3 ){

                        frequency<<split2[0].simplified().toDouble();
                        counts<<split2[1].simplified().toDouble();
                        jitter<<split2[2].simplified().toDouble();
                        if(split2[1].simplified().toDouble()>max_cps){    //find out the maximum value in the count rate array
                            max_cps=split2[1].simplified().toDouble();
                        }

                    }
                    else if (split2.size()==2 ){

                        frequency<<split2[0].simplified().toDouble();
                        counts<<split2[1].simplified().toDouble();
                        if(split2[1].simplified().toDouble()>max_cps){    //find out the maximum value in the count rate array
                            max_cps=split2[1].simplified().toDouble();
                        }

                    }
                }

            }

        }

        //Normalize countrate to 1

        QVector<double>::iterator i;
        for (i = counts.begin(); i != counts.end(); ++i){
            *i = (*i)/max_cps;
        }

        parser.append(frequency);
        parser.append(counts);
        parser.append(jitter);

    }
    else if (format=="E") {
        foreach(const QString &str, filenames){
            QFile file(str);
            if (file.open(QIODevice::ReadWrite)){

                max_cps=0.1;
                while (!file.atEnd()) {
                    QString line = file.readLine();
                    //qDebug()<<line;
                    //QStringList split=line.split("    ",QString::SkipEmptyParts);
                    QStringList split=line.split("\t",QString::SkipEmptyParts);
                   // QStringList split2=split.at(0).split(",");
                   // QLocale::setDefault(QLocale::Finnish);
//                    QString tetsii=split.at(0);
//                    double tetsi=tetsii.toDouble();
//                    qDebug()<< tetsi;
                    //qDebug()<< split.size();
                    if(split.size()==2 ){

                        frequency<<split[0].simplified().toDouble();
                        counts<<split[1].simplified().toDouble();
                       if(split[1].simplified().toDouble()>max_cps){    //find out the maximum value in the count rate array
                            max_cps=split[1].simplified().toDouble();
                        }

                    }
    }
            }}

        parser.append(frequency);
        parser.append(counts);
    }

}
fileParser::~fileParser(){


}

