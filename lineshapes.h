#ifndef LINESHAPES_H
#define LINESHAPES_H

#include <QObject>
#include <math.h>
class lineShapes : public QObject
{
    Q_OBJECT
public:
    explicit lineShapes(QObject *parent = 0);
    static  void gauss(size_t n, double *x, double *y, double w, double xc);
    static void lorentz(size_t n, double *x, double *y, double w, double xc);
    static double humlicek_v12(double x, double y);
    static void voigt(size_t n, double *x, double *y, double w[2], double xc);

signals:

public slots:

};

#endif // LINESHAPES_H
