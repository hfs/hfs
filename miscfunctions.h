#ifndef MISCFUNCTIONS_H
#define MISCFUNCTIONS_H

#include <QObject>
#include <QSpinBox>
#include <QRadioButton>
#include <QDebug>
#include <QString>
#include "ui_hfs.h"


class miscFunctions : public QObject
{
    Q_OBJECT
public:
    explicit miscFunctions(QObject *parent = 0,Ui_HFS *ui=0);
    ~miscFunctions();
    QList<QwtCounter *> HFSTabQWTCounters;

    QList<QwtSlider *> HFSTabQWTSliders;

    QList<QRadioButton *> HFSTabQRadiobuttons;

    QList<QwtCounter *> lorentzianFittingTabQWTCounters;

    void setSpinBoxValues(QList<double> results);

    void resetSpinboxValues();

    void blockHFSUiUpdates(bool block);

    void blockLorentzianFittingUpdates(bool block);



    Ui_HFS *userI;

    QList<QList<double> > setFitConstants(QList<QList<double> > peakData);

    QList<QList<double> > setLorentzianFitParameterInitialValues();

    QList<QList<double> > setLorentzianFitParameterReFitValues();

    // returns a Qhash containing information on wheter a certain lorentzian fit parameter is locked or not

    QHash<QString, bool> setLockedLorentzianVariables();





signals:

public slots:

};

#endif // MISCFUNCTIONS_H
