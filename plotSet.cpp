
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


//Plotting related class
#include "plotSet.h"

plotSet::plotSet(QwtPlot *plot){
    //setup the plot ui->HFS_plot->setCanvasBackground(Qt::white);
    mainPlot = new QwtPlotCurve("HFS_plot");

    plot_d=plot;
    plot->setAxisScale(QwtPlot::xBottom,-800,800);
    plot->setAxisScale(QwtPlot::yLeft,0,1);
    plot->enableAxis(0,false);

    //fitter for the HFSplot
    QwtWeedingCurveFitter *fitter = new QwtWeedingCurveFitter();
    fitter->setTolerance(0.1);
    mainPlot->setCurveFitter(fitter);
    mainPlot->setCurveAttribute(QwtPlotCurve::Fitted, true);


    mainPlot->setPen(QPen(Qt::red,1));

    plot->setAxisFont(QwtPlot::xBottom,QFont(QString("Arial"), 10, 0, false));
    plot->setCanvasBackground(Qt::white);
    mainPlot->setRenderHint(QwtPlotItem::RenderAntialiased);
    mainPlot->attach(plot);
    plot->autoReplot();


    //grid
    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(Qt::gray, 0.0, Qt::DotLine));
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(plot);

    //setup spectrum plot
    spectrum=new QwtPlotCurve("Spectrum");
    spectrum->attach(plot);

    //Setup popupmenu

    plot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(plot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ShowPlotContextMenu(QPoint)));

    //init fileoperations class

    fileOp = new fileOperations;



}
plotSet::~plotSet(){
    delete mainPlot;
    delete fitter;
    delete zoom;
    delete pan;
    delete picker;
    delete grid;
    delete spectrum;
    delete fileOp;
    if(curves.size()!=0){
        foreach(QwtPlotCurve *c, curves){
            delete c;
        }
    }
}
//A context menu for the plots
void plotSet::ShowPlotContextMenu( const QPoint &pos){
    QPoint globalPos=plot_d->mapToGlobal(pos);
    QMenu popUp;
    popUp.addAction("Save as PDF");
    popUp.addAction("Save Data");
    QAction* selectedItem=popUp.exec(globalPos);

    if(selectedItem){
        if(selectedItem->text()=="Save as PDF"){
            fileOp->savePlotToPDF(plot_d);

        }
        if(selectedItem->text()=="Save Data"){
            //qDebug() << mainPlot->data()->sample(2);

            QVector<double> x, y;
            for(int i=0; i<mainPlot->data()->size(); i++){
                QPointF data=mainPlot->data()->sample(i);
                x<<data.x();
                y<<data.y();
            }

            fileOp->saveDatatoFile(x,y);
        }
    }

}


//sets the plot curves for individual lorentzian during each HFS calculation
//not needed when the A or B values are adjusted as the amount of peaks stays the same
void plotSet::allocate(int size){
    //    QColor color;
    //    color.setRgb(qrand() % 256, qrand() % 256, qrand() % 256);
    //    curv->setPen(QPen(color, 1));
    foreach(QwtPlotCurve *curv, curves){        //Detach(remove) any existing curve from the plot

        curv->detach();
        delete curv;
        curv=NULL;
    }
    curves.clear();                             //Cleart the List containing the individual curves
    for(int i=0;i<size;i++){
        curves.append(new QwtPlotCurve);        //Fill the list with new curves
    }
    foreach(QwtPlotCurve *curv, curves){        //Attach these curves to the plot
        curv->attach(plot_d);
        curv->setPen(QPen(Qt::black, 1, Qt::DashDotLine));
        curv->setRenderHint(QwtPlotItem::RenderAntialiased);
    }


}



void plotSet::setInd(QList<QVector<double> > ipeaks, QVector<double> xr){
    ////    setup plot for individual peaks
    int indx=0;
    foreach(QwtPlotCurve *curv, curves){            //Set data to these plots,
        //Notice that x and y are the oppisite way around

        curv->setSamples(xr,ipeaks[indx]);
        indx++;
    }

}

void plotSet::showHideinLo(bool button){
    if(button){
        foreach(QwtPlotCurve *curv, curves){
            curv->show();
        }
    }
    else{
        foreach(QwtPlotCurve *curv, curves){
            curv->hide();
        }

    }
}

//Function to clear the plot
void plotSet::reset(double xminus, double xplus){
    plot_d->setAxisAutoScale(0);
    plot_d->setAxisScale(QwtPlot::xBottom,xminus,xplus);
    plot_d->replot();
}

void plotSet::saveImage(QString fileName){
    //Some tricks to make the image look nicer
    mainPlot->setPen(QPen(Qt::red, 3));
    plot_d->setAxisFont(QwtPlot::xBottom,QFont(QString("Arial"), 30, 0, false));
    //image properties
    QPrinter printer( QPrinter::HighResolution );
    printer.setOutputFormat( QPrinter::PdfFormat );
    printer.setOutputFileName( fileName );

    QwtPlotRenderer renderer;
    renderer.renderDocument(plot_d, fileName, QSizeF(300, 200), 85);
    //and revert the changes
    mainPlot->setPen(QPen(Qt::red, 1));
    plot_d->setAxisFont(QwtPlot::xBottom,QFont(QString("Arial"), 10, 0, false));

}

void plotSet::addZoomAndPan(){

    //zoom
    QwtPlotMagnifier *zoom = new QwtPlotMagnifier(plot_d->canvas());
    zoom->setAxisEnabled(0,true);
    zoom->setMouseButton(Qt::WheelFocus);

    //Pan
    QwtPlotPanner *pan=new QwtPlotPanner(plot_d->canvas());
    pan->setMouseButton(Qt::LeftButton);

    QwtPlotPicker *picker=new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yRight,
                                            QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
                                            plot_d->canvas());
    picker->setStateMachine(new QwtPickerTrackerMachine());
    picker->setRubberBand(QwtPicker::CrossRubberBand);
}

