
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "demingregression.h"

demingRegression::demingRegression(QObject *parent) :
    QObject(parent)
{
    iterations=10000; //iterations for the deming iterator
}

double demingRegression::average(QVector<double> x){
    int n=x.size();
    double sum=0;
    for(int index=0; index<n; index++){
        sum=sum+x.at(index);
    }
    double averageValue;
    averageValue=sum/n;
    //qDebug()<<averageValue<<"average";
    return averageValue;
}

double demingRegression::sxy(QVector<double> x,QVector<double> y){
    double n=x.size();
    double xa=average(x);
    double ya=average(y);
    double factor=1/(n-1);
    double sum=0;
    for(int index=0; index<n; index++){
        double xd;
        double yd;
        xd=x.at(index)-xa;
        yd=y.at(index)-ya;
        sum=sum+xd*yd;
    }
    //qDebug()<< factor*sum<<"factor*sum";
    return factor*sum;
}








QVector<QPointF> demingRegression::returnFitCurve(){
    demingIteration();
    QPointF fittedPoint;
    QVector<QPointF> fittedData;
    for(int i=0; i<N; i++){
        double x=x_m.at(i);
        double y=iterAD+iterBD*x;
        fittedPoint.setX(x);
        fittedPoint.setY(y);
        fittedData.append(fittedPoint);
    }
    //set fit parameters to the string
    fitParameters.clear();
    fitParameters.append("y=AD+BD*x ");
    fitParameters.append(" BD: ");
    fitParameters.append( QString::number(iterBD));
    fitParameters.append(" +- ");
    fitParameters.append(QString::number(iterBDErr));
    fitParameters.append("  AD: ");
    fitParameters.append(QString::number(iterAD));
    fitParameters.append(" +- ");
    fitParameters.append(QString::number(iterADErr));
    fitParameters.append("\n");
    fitParameters.append("Goodnes of the fit G=S/(n-2): ");
    fitParameters.append(QString::number(S));
    fitParameters.append("\n");
    fitParameters.append("Errors scaled with the square root G:");
    fitParameters.append("\n");
    fitParameters.append("BD: ");
    fitParameters.append( QString::number(iterBD));
    fitParameters.append(" +- ");
    fitParameters.append(QString::number(iterBDErr*sqrt(S)));
    fitParameters.append("  AD: ");
    fitParameters.append(QString::number(iterAD));
    fitParameters.append(" +- ");
    fitParameters.append(QString::number(iterADErr*sqrt(S)));
    //<<"+- "<<b0Error()<< " b1: " << bone << "+-" << b0Error();
    //qDebug() << fitParameters;

    return fittedData;
}

QString demingRegression::returnFitParameters(){
    return fitParameters;
}

//general Deming regression using York method.
//Initial slope estimate using standard linear regression
double demingRegression::initialSlopeEstimate(){
    double slope;
    double sxy_v=sxy(x_m, y_m);
    double sxx_v=sxy(x_m, x_m);
    slope=sxy_v/sxx_v;
    return slope;
}

//weighted average
double demingRegression::weightedAverage(QVector<double> dataSet, QVector<double> dataSetWeights){
    double sum=0;
    double sumWeights=0;
    for(int iter=0; iter<N; iter++){
        sum=sum+dataSet.at(iter)*dataSetWeights.at(iter);
        sumWeights=sumWeights+dataSetWeights.at(iter);
    }

    double wAve;
    wAve=sum/sumWeights;
    //qDebug()<<wAve;
    return wAve;
}
//Calculate weights, assume that the errors are uncorrelated
QVector<double> demingRegression::pointWeights(double slope){
    QVector<double> weights;

    for(int iter=0; iter<N;iter++){
        double nonInv;
        double dy=dy_m.at(iter);    //x and y errors
        double dx=dx_m.at(iter);
        double wy=1/pow(dy,2);      //x and y weights as inverse squared
        double wx=1/pow(dx,2);
        nonInv=(wx+pow(slope, 2)*wy);
        double inv=(wx*wy)/nonInv;
        weights.append(inv);
    }
    return weights;
}

// measured point minus the average of the points
QVector<double> demingRegression::dotPoints(QVector<double> dataSet,double wAve_v){
    QVector<double>dP;
    for(int iter=0; iter<N;iter++){
        double val;
        val=dataSet.at(iter)-wAve_v;
        dP.append(val);
    }
    return dP;
}

//B factors
QVector<double> demingRegression::B(QVector<double> weights,QVector<double> xDots, QVector<double> yDots, double slope){
    QVector<double> zeeS;
    //qDebug()<<weights<<xDots<<yDots<<slope;
    for(int iter=0;iter<N; iter++){
        double wi=weights.at(iter);
        double yErr=dy_m.at(iter); //x and y errors
        double xErr=dx_m.at(iter);
        double yw=1/pow(yErr,2);    //x and y weights
        double xw=1/pow(xErr,2);
        double xDot=xDots.at(iter);
        double yDot=yDots.at(iter);
        double zi;
        //qDebug()<<wi<<yErr<<xErr<<xDot<<yDot;
        zi=wi*(xDot/yw+slope*yDot/xw);
        zeeS.append(zi);
    }
    //qDebug()<<zeeS;
    return zeeS;
}
//Slope
double demingRegression::bD(QVector<double> wi, QVector<double> Bi, QVector<double> ydot, QVector<double> xdot){
    double slope=0;
    double sumUp=0;
    double sumDown=0;
    for(int iter=0;iter<N;iter++){
        sumUp=sumUp+wi.at(iter)*Bi.at(iter)*ydot.at(iter);
        sumDown=sumDown+wi.at(iter)*Bi.at(iter)*xdot.at(iter);
    }
    slope=sumUp/sumDown;
    return slope;
}
//Intercept
double demingRegression::aD(double slope, double yWave, double xWave){
    double intercept=0;
    intercept=yWave-slope*xWave;
    return intercept;
}
//Adjusted values xi=Xi+Bi
QVector<double> demingRegression::adjustedValues(double xWave, QVector<double> zeeS){
    QVector<double> adVal;

    foreach(double val, zeeS){
            adVal.append(xWave+val);
    }
    return adVal;
}

//Error formulas for the Deming equations:
//Error for the slope
double demingRegression::varBD(QVector<double> weights, QVector<double> xdot){
    double sumai=0;
    double suma=0;
    for(int iter=0;iter<N; iter++){
        double xd=xdot.at(iter);
        double wi=weights.at(iter);

        sumai=sumai+wi*pow(xd,2);
    }
    suma=1/sumai;
     return sqrt(suma);
}
//Error for the intercept
double demingRegression::varAD(QVector<double> weights, double slopeErr, double xWave){
    double sum=0;
    double sumInv=0;
    for(int iter=0;iter<N; iter++){
        double wi=weights.at(iter);
        sum=sum+wi;
    }
    sumInv=1/sum;
    double altrest=0;
    double powef;
    powef=xWave;

    altrest=sumInv+pow(powef, 2)*pow(slopeErr,2);
    return sqrt(altrest);
    //return sumInv+rest;
}

double demingRegression::sN(QVector<double> weights, double slope, double intercept){
    double sum=0;
    for(int iter=0;iter<N; iter++){
        double wi=weights.at(iter);
        double Y=y_m.at(iter);
        double X=x_m.at(iter);
        double deviation=Y-slope*X-intercept;
        sum=sum+wi*pow(deviation,2);
    }
    return sum/(N-2);
}


//an iterative approach, York 2004,  is needed as the weights and hence the x and y dots, weighted average and z factor depend on the slope(BD)
void demingRegression::demingIteration(){
    iterBD=initialSlopeEstimate();                          //calculate first slope estimate by simple linear regression
    iterAD=0;                                               //set intercept to zero
    //qDebug() <<"Bd: " << iterBD;
    iterBDErr=0;
    iterADErr=0;
    S=0;
    for(int iter=0; iter<iterations;iter++){                //iterate for "iterations"
        QVector<double> weights=pointWeights(iterBD);       //calculate weights
        //qDebug()<<"weights"<<weights;
        double xWave_v=weightedAverage(x_m, weights);       //calculate weighted averages
        double yWave_v=weightedAverage(y_m, weights);
        //qDebug()<<"x and y ave" << xWave_v<<yWave_v;
        QVector<double> xDot=dotPoints(x_m, xWave_v);       //calculate x and y dots
        QVector<double> yDot=dotPoints(y_m, yWave_v);
        //qDebug()<<"x and y dot"<<xDot<<yDot;

        QVector<double> Bs=B(weights, xDot,yDot, iterBD); //calculate z factor
        //qDebug()<<"zi "<< zs;
        iterBD=bD(weights, Bs, yDot,xDot);                 //calculate slope
        iterAD=aD(iterBD, yWave_v, xWave_v);               //calculate intercept

        //calculate error

        double zWave_v=weightedAverage(Bs, weights);
        QVector<double> zDot=dotPoints(Bs, zWave_v);


        QVector<double> adVal=adjustedValues(xWave_v, Bs);          //calculate adjusted x-values, x=Xi+Bi;
        double smallXAverage=weightedAverage(adVal, weights);       //calculate weighted average of the adjusted x-values
        QVector<double> smallXDot=dotPoints(adVal, smallXAverage);  //calculate the difference betweenan adjysted points and the avergae
        iterBDErr =varBD(weights, smallXDot);                       //calculate error for the slope
        iterADErr =varAD(weights ,iterBDErr, smallXAverage);        //and for the intercept
        S=sN(weights, iterBD, iterAD);
    }

    //qDebug() <<"Bd: " << iterBD << " aD: "<<iterAD;
}
