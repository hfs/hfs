
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

//Based on Bradley Cheals code

#include "hfsfunctions.h"

HFSfunctions::HFSfunctions()
{
}

double HFSfunctions::sixj(double a, double b, double c, double d, double e, double f){
    //    7.8.2 6-j Symbols
    //    — Function: double gsl_sf_coupling_6j (int two_ja, int two_jb, int two_jc, int two_jd, int two_je, int two_jf)
    //    — Function: int gsl_sf_coupling_6j_e (int two_ja, int two_jb, int two_jc, int two_jd, int two_je, int two_jf, gsl_sf_result * result)

    //        These routines compute the Wigner 6-j coefficient,

    //                  {ja jb jc
    //                   jd je jf}

    //        where the arguments are given in half-integer units, ja = two_ja/2, ma = two_ma/2, etc.
    //     qDebug()<<"AAAAAAAAAAaa";
    //     qDebug() <<a<<b<<c<<d<<e<<f;
    //     qDebug()<<gsl_sf_coupling_6j(a, b,c, d, e, f);
    //     qDebug()<<"BBBB";
    return gsl_sf_coupling_6j(a, b,c, d, e, f);
}


QVector< QVector<float> > HFSfunctions::intensities(QList<double> fl,QList<double> fu,double nucspin,double jl,double ju){

    //    sum0=0.0; sum2=0.0; gf0=zeros(len(fl),float); fg=zeros(len(fl),float); gf2=zeros(len(fl),float); intensities=zeros([len(fl),3],float)
    //    for peak in range(len(fl)):
    //            flower=fl[peak]
    //            fupper=fu[peak]
    //            w2=(sixj(int(2*ju),int(2*fupper),int(2*nucspin),int(2*flower),int(2*jl),int(2)))**2
    //            gf0[peak]=(2*flower+1)*(2*fupper+1)*(2*jl+1)*w2
    //            fg[peak]=1.0-float((2*ju+1)*gf0[peak])/float((2*fupper+1)*(2*jl+1))
    //            wf=sixj(int(2*flower),int(2*fupper),2,4,2,int(2*fupper))
    //            wg=sixj(int(2*nucspin),int(2*fupper),int(2*ju),4,int(2*ju),int(2*fupper))
    //            wj=sixj(int(2*jl),int(2*ju),2,4,2,int(2*ju))*(2*flower+1)*(2*fupper+1)*(2*fupper+1)*(2*jl+1)*(2*ju+1)
    //            phase=(-1.0)**(nucspin-flower-jl)
    //            gf2[peak]=1.5*phase*wf*wg*wj*w2
    //            sum0 += gf0[peak]
    //            sum2 += gf0[peak] + gf2[peak]
    //    for peak in range(len(fl)):
    //            gf2[peak]=(gf0[peak]+gf2[peak])*sum0/sum2
    //            intensities[peak][0]=gf0[peak]/sum0
    //            intensities[peak][1]=gf2[peak]/sum0
    //            intensities[peak][2]=fg[peak]
    //    return intensities

    //needed mainly in function "intensities", I dont yet know how this function works so I don't know the meaning of these variables

    double sum0;
    double sum2;
    QVector<float> gf0;
    QVector<float> fg;
    QVector<float> gf2;
    QVector<QVector<float> > intensities_vector;
    double peaks;

    double flower;
    double fupper;
    //sixj value
    double w2;
    double wf;
    double wg;
    double wj;
    double phase;
    int pi;


    pi =0;

    sum0=0;
    sum2=0;

    gf0.clear();
    gf0.resize(fl.size());
    fg.clear();
    fg.resize(fl.size());
    gf2.clear();
    gf2.resize(fl.size());
    intensities_vector.clear();
    intensities_vector.resize(fl.size());

    //iterate trough the F spins in fl
    foreach (peaks, fl){


        flower=peaks;
        //qDebug() << flower<<"Flower" ;
        fupper=fu[pi];
        //        qDebug()<<pi<<"pi";
        w2=pow((HFSfunctions::sixj(2*ju,2*fupper, 2*nucspin, 2*flower, 2*jl,2)),2);
        //qDebug() << w2;

        gf0[pi]=(2*flower+1)*(2*fupper+1)*(2*jl+1)*w2;
        //qDebug() << w2<<"w2";
        fg[pi]=1-((2*ju+1)*gf0[pi])/((2*fupper+1)*(2*jl+1));
        //qDebug() << fg<<"fg";
        wf=HFSfunctions::sixj(2*flower, 2*fupper, 2,4,2,2*fupper);
        //qDebug() << wf<<"wf";
        wg=HFSfunctions::sixj(2*nucspin, 2*fupper,2*ju,4,2*ju,2*fupper);
        //qDebug() << wg<<"wg";
        wj=HFSfunctions::sixj(2*jl,2*ju,2,4,2,2*ju)*(2*flower+1)*(2*fupper+1)*(2*fupper+1)*(2*jl+1)*(2*ju+1);  //(2*fupper+1) is twice??
        //qDebug() << wj<<"wj";
        phase=pow(-1,(nucspin-flower-jl));

        //qDebug() << phase<<"phase";
        gf2[pi]=(1.5*phase*wf*wg*wj*w2);
        //qDebug() << gf2<<"gf2";
        sum0 += gf0[pi];
        sum2 += gf0[pi]+gf2[pi];
        pi+=1;
    }

    pi=0;
    foreach (peaks, fl){
        gf2[pi]=(gf0[pi]+gf2[pi])*sum0/sum2;
        intensities_vector[pi].append(gf0[pi]/sum0);
        intensities_vector[pi].append(gf2[pi]/sum0);
        intensities_vector[pi].append(fg[pi]);
        pi+=1;
    }

    return intensities_vector;

}


// alpha and beta parts of
//delta v=A*(f*(f+1) - i*(i+1) - j*(j+1))/2.0 + beta*(3*k*(k+1) - 4*i*(i+1)*j*(j+1))/float(8*i*(2*i-1)*j*(2*j-1))
//delta v=A*HFSfunctions::alpha+B*HFSfunctions::beta


float HFSfunctions::alpha(double f, double i, double j){
    //    if i==0 or j==0: return 0
    //    else: return float(f*(f+1) - i*(i+1) - j*(j+1))/2.0
    if(i==0 || j==0){
        return 0;
    }
    else{
        //qDebug()<<HFSfunctions::gamma(f,i,j)/2.0;;
        return HFSfunctions::gamma(f,i,j)/2.0;
    }
}

float HFSfunctions::beta(double f, double i, double j){
    //    k = f*(f+1) - i*(i+1) - j*(j+1)
    //    if i==0 or i==0.5 or j==0 or j==0.5: return 0
    //    else: return float(3*k*(k+1) - 4*i*(i+1)*j*(j+1))/float(8*i*(2*i-1)*j*(2*j-1))

    if(i==0 || i==0.5 || j==0 || j==0.5){
        return 0;
    }
    else{
        float C=HFSfunctions::gamma(f, i, j);
        return (3*C*(C+1) - 4*i*(i+1)*j*(j+1))/(8*i*(2*i-1)*j*(2*j-1));
    }
}

//this funtion not expicitly present in the original python code. I separated it here for clarity
float HFSfunctions::gamma(double f, double i, double j){
    return (f*(f+1) - i*(i+1) - j*(j+1));
}

QList< QList<double> > HFSfunctions::transitions(double jl,double ju,double ns){
    //    lowerfs=[]; lowerf=abs(jl-ns)
    //            while lowerf <= jl+ns :
    //                    lowerfs.append(lowerf)
    //                    lowerf = lowerf + 1
    //            upperfs=[]; upperf=abs(ju-ns)
    //            while upperf <= ju+ns :
    //                    upperfs.append(upperf)
    //                    upperf = upperf + 1
    //            transitions=[]
    //            for fl in lowerfs:
    //                    if fl-1 in upperfs: transitions.append([fl,fl-1,alpha(fl,ns,jl),beta(fl,ns,jl),alpha(fl-1,ns,ju),beta(fl-1,ns,ju),0,0,0])
    //                    if fl in upperfs and fl != 0: transitions.append([fl,fl,alpha(fl,ns,jl),beta(fl,ns,jl),alpha(fl,ns,ju),beta(fl,ns,ju),0,0,0])
    //                    if fl+1 in upperfs: transitions.append([fl,fl+1,alpha(fl,ns,jl),beta(fl,ns,jl),alpha(fl+1,ns,ju),beta(fl+1,ns,ju),0,0,0])
    //            fl=[row[0] for row in transitions]
    //            fu=[row[1] for row in transitions]
    //            for peak in range(len(fl)):
    //                    transitions[peak][6]=intensities(fl,fu,ns,jl,ju)[peak][0]
    //                    transitions[peak][7]=intensities(fl,fu,ns,jl,ju)[peak][1]
    //                    transitions[peak][8]=intensities(fl,fu,ns,jl,ju)[peak][2]
    //            return transitions


    //transition parameters;

    QList<double>  lowerfs;  // lower HF states
    double lowerf;          //lower F start value
    QList<double>  upperfs; //upper HF states
    double upperf;          //upper f start value
    QList< QList<double> > transitions_array; //2D array containing the transitions

    double fl; //single lower f state, used for selecting allowed transitions
    QList<double> fu_allowed; //spins for allowed upper F transitions
    QList<double> fl_allowed; //the corresponding lower F spins
    QList<double> flfu; //QList containing the upper and lower F state spins and their corresponding alpha and beta parameters. remember to clear (before)after use
    QList<double> peak; //peaks


    lowerfs.clear();
    upperfs.clear();
    transitions_array.clear();
    fl_allowed.clear();
    fu_allowed.clear();

    if ((((ju-jl)!=0 && (ju-jl)!=1 && (jl-ju)!=1)) || (jl==0 and ju==0)){
        return transitions_array;
    }
    else{


        //calculate the possible spins for lower F states  (jl-ns)<Jf<(jl+ns)
        lowerf=fabs(jl-ns);
        while(lowerf<=jl+ns){
            lowerfs.append(lowerf);
            lowerf=lowerf+1;
        }
        //calculate the possible spins for upper F states |(ju-ns)|<Jf<(ju+ns)
        upperf=fabs(ju-ns);
        while(upperf<=ju+ns){
            upperfs.append(upperf);
            upperf=upperf+1;
        }

        //select allowed transitions between the lower F states to the upper F states. Allowed transitions are those which delta spin is 1 or 0(fl=!0).
        // each transition_array element is a QList<double> containine the spins fl and fl+-1/0 and the alpha and beta factors for both spins.
        //These spins correspond to the lower and upper spins.

        foreach (fl, lowerfs){

            if(upperfs.contains(fl-1)){
                flfu.clear();
                flfu <<fl<<fl-1<<HFSfunctions::alpha(fl,ns,jl)<<HFSfunctions::beta(fl,ns,jl)<<HFSfunctions::alpha(fl-1,ns,ju)<<HFSfunctions::beta(fl-1,ns,ju)<<0<<0<<0;
                transitions_array.append(flfu);
            }

            if(upperfs.contains(fl) && fl!=0){
                flfu.clear();
                flfu <<fl<<fl<<HFSfunctions::alpha(fl,ns,jl)<<HFSfunctions::beta(fl,ns,jl)<<HFSfunctions::alpha(fl,ns,ju)<<HFSfunctions::beta(fl,ns,ju)<<0<<0<<0;
                transitions_array.append(flfu);
            }

            if(upperfs.contains(fl+1)){
                flfu.clear();
                flfu << fl<<fl+1<<alpha(fl,ns,jl)<<beta(fl,ns,jl)<<alpha(fl+1,ns,ju)<<beta(fl+1,ns,ju)<<0<<0<<0;
                transitions_array.append(flfu);
            }

        }
        //make two arrays containing the F state spins for the allowed transitions
        foreach (QList<double> element, transitions_array){
            fl_allowed.append(element[0]);
            fu_allowed.append(element[1]);
        }
        int pi2=0;
        //fetch the intensitiens for each of the transitions
        QVector< QVector<float> > temp_int;
        temp_int.clear();
        temp_int=HFSfunctions::intensities(fl_allowed,fu_allowed,ns,jl,ju);

        foreach(peak, transitions_array){
            transitions_array[pi2][6]=temp_int[pi2][0];  //Intensity
            transitions_array[pi2][7]=temp_int[pi2][1];  //Intensity at 90 degrees
            transitions_array[pi2][8]=temp_int[pi2][2];  //pump fraction
            pi2+=1;

        }

        return transitions_array; // alpha lower, beta lower, alpha upper, beta upper, intesity, int at 90,, pump fraction
    }

}
///spectrum functions

QVector<double> HFSfunctions::lorentzian(QVector<double> x, double fwhm, double intensity_d,double bgOffset, QList<double> i, QList<double> w){
    //calculates a sum of lorentzian functions corresponding to the peaks and sums them
    //arguments are x=x-range, fwhm=Full width half maximum for a lorentzian, intensity_d=peak intensity scaler, i=calculated intensity
    //w calculated transitions frequencies,


    //    y=0.0
    //    for peak in range(len(w)):
    //            y=y+float(i[peak])*intensity*(fwhm**2/(4*((x-float(w[peak]))**2+(fwhm/2.0)**2)))
    //    return y
    QVector<double> data;
    lortz.clear();
    if(!x.isEmpty() && !i.isEmpty() && !w.isEmpty()){

        //calculates individual lorentzians forthe whole range
        data.resize(x.size());
        data.fill(0);
        for(int peak=0;peak<w.size();peak++){

            QVector<double> single;
            foreach(double value, x){
                single<<(i[peak])*intensity_d*(pow(fwhm,2)/(4*(pow((value-(w[peak])),2)+pow((fwhm/2.0),2))))+bgOffset;
            }
            lortz.append(single);
            for(int i=0; i<data.size(); i++){
                double i_val=data[i]+single[i];
                data.replace(i,i_val);
            }

        }
        //sum the lorentzians


    }
    return data;
}

QList<QVector<double> > HFSfunctions::lorentzians(){


    return lortz;
}


QList<QList<double> > HFSfunctions::transfreqs( QList< QList<double> > t, double au, double bu, double al,double bl,double w){
    //    transfreqs=[]
    //    for peak in range(shape(t)[0]):
    //            frequency = w + au*t[peak][4] + bu*t[peak][5] - al*t[peak][2] - bl*t[peak][3]
    //            transfreqs.append()
    //    return transfreqs

    //t is the transition data from HFSfunctions::transitions
    //au,bu,al,al are the corresponding  slider values

    //w is an offset
    QList<QList<double> > transfreqs_data; //full data array with the transition frequencies

    QList<double> freqdat; //array holding data of a single loop
    double frequency;
    //    if(!t.isEmpty()){

    //    }

    if(!t.isEmpty()){
        for(int peak=0; peak<t.size();peak++){
            freqdat.clear();
            frequency = w + au*t[peak][4] + bu*t[peak][5] - al*t[peak][2] - bl*t[peak][3];
            freqdat << t[peak][0]<<t[peak][1]<< t[peak][6]<< t[peak][7]<< t[peak][8]<< frequency;
            transfreqs_data.append(freqdat);

        }
    }

    return transfreqs_data;
}
QVector<double> HFSfunctions::gaussian(QVector<double> x, double fwhm, double intensity_d,double bgOffset, QList<double> i, QList<double> w){

    QVector<double> data;
    gauss.clear();
    if(!x.isEmpty() && !i.isEmpty() && !w.isEmpty()){

        //calculates individual gaussians for the whole range
        data.resize(x.size());
        data.fill(0);
        for(int peak=0;peak<w.size();peak++){

            QVector<double> single;
            foreach(double value, x){
                double innerExp=-pow((value-(w[peak])),2)/(2*pow(fwhm,2));
                double gaussian= (i[peak])*intensity_d*exp(innerExp)+bgOffset;
                single<<gaussian;
            }
            gauss.append(single);
            for(int i=0; i<data.size(); i++){
                double i_val=data[i]+single[i];
                data.replace(i,i_val);
            }

        }

    }
    return data;
}

QList<QVector<double> > HFSfunctions::gaussians(){


    return gauss;
}

double HFSfunctions::voigtProfile(double lorentzianAmplitude, double peakPosition, double lorentzianFWHM, double gaussianFWHM, double x){
    int i;
    double A[4],B[4],C[4],D[4],V=0,Vamp=0;
    static double sqrtln2=sqrt(M_LN2);
    static double sqrtpi=sqrt(M_PI);
//    static double sqrtln2=0.832554611;
//    static double sqrtpi=1.772453851;
    double X=(x-peakPosition)*2*sqrtln2/gaussianFWHM;
    double Y=lorentzianFWHM*sqrtln2/gaussianFWHM;
    A[0]=-1.2150; B[0]= 1.2359;
    A[1]=-1.3509; B[1]= 0.3786;
    A[2]=-1.2150; B[2]=-1.2359;
    A[3]=-1.3509; B[3]=-0.3786;
    C[0]=-0.3085; D[0]= 0.0210;
    C[1]= 0.5906; D[1]=-1.1858;
    C[2]=-0.3085; D[2]=-0.0210;
    C[3]= 0.5906; D[3]= 1.1858;
    for(i=0;i <= 3;i++){
        V+=(C[i]*(Y-A[i])+D[i]*(X-B[i]))/(pow((Y-A[i]),2)+pow((X-B[i]),2));
        Vamp+=(C[i]*(Y-A[i])+D[i]*(B[i]))/(pow((Y-A[i]),2)+pow((B[i]),2));
    }

    double voigtAmp;
    voigtAmp=(lorentzianFWHM*sqrtpi*sqrtln2/gaussianFWHM)*Vamp;
    return ((lorentzianFWHM*lorentzianAmplitude*sqrtpi*sqrtln2/gaussianFWHM)*V);
    //return(lorentzianAmplitude*sqrtpi*sqrtln2)*V/Vamp;


}

QVector<double> HFSfunctions::voigt(QVector<double> x, double gausw, double lorw, double intensity,double bgOffset, QList<double> i, QList<double> w){
    QVector<double> data;
    voig.clear();
    if(!x.isEmpty() && !i.isEmpty() && !w.isEmpty()){
        //calculates individual gaussians for the whole range
        data.resize(x.size());
        data.fill(0);
        for(int peak=0;peak<w.size();peak++){

            QVector<double> single;
            foreach(double value, x){
                double voigtPoint= voigtProfile(i[peak]*intensity,w[peak],lorw, gausw,value)+bgOffset;
                single<<voigtPoint;
            }
            voig.append(single);
            for(int i=0; i<data.size(); i++){
                double i_val=data[i]+single[i];
                data.replace(i,i_val);
            }

        }
    }
    return data;
}
QList<QVector<double> > HFSfunctions::voigts(){


    return voig;
}


QVector<double> HFSfunctions::returnProfile(QString profile,QVector<double> x, double fwhm, double intensity_d,double bgOffset, QList<double> i, QList<double> w, double fwhm2){
    if(profile=="Lorentzian"){
        return lorentzian(x,fwhm, intensity_d,bgOffset, i, w);
    }
    else if(profile=="Gaussian"){
        return gaussian(x,fwhm, intensity_d,bgOffset, i, w);
    }
    else if(profile=="Voigt"){
        return voigt(x, fwhm, fwhm2, intensity_d,bgOffset, i,w);
    }

    else{
        QVector<double> empty;
        return empty;
    }
}

QVector<double> HFSfunctions::returnSpectrumProfile(QString profile,QVector<double> x, double fwhm, double intensity_d,double bgOffset, QVector<double> i, QVector<double> w,double fwhm2){
    QList<double> w_temp;
    QList<double> i_temp;
    foreach(double val, w){
        w_temp.append(val);
    }
    foreach(double val, i){
        i_temp.append(val);
    }
    if(profile=="Lorentzian"){
        //qDebug()<<"lorentzian";
        return lorentzian(x,fwhm, intensity_d, bgOffset, i_temp, w_temp);
    }
    else if(profile=="Gaussian"){
        //qDebug()<<"gaussian";
        return gaussian(x,fwhm, intensity_d, bgOffset, i_temp, w_temp);
    }
    else if(profile=="Voigt"){
          return voigt(x, fwhm, fwhm2, intensity_d, bgOffset, i_temp,w_temp);
    }
    else{
        QVector<double> empty;
        //qDebug()<<"empty";
        return empty;
    }
}
