//A container to hold all the data of a single session.
//It will contain all the theoretical data, measured data
//and the fit data.
#ifndef CONTAINER_H
#define CONTAINER_H

#include <QObject>
#include <QVector>
#include <QHash>
#include <QDebug>
#include <QVector>
#include <QStringList>
#include "lorentzianfitter.h"
#include "plotSet.h"
class container : public QObject
{
    Q_OBJECT
public:
    explicit container(QObject *parent=0, QString setName=NULL);
    ~container();
    void saveSession(QHash<QString, double>  ui_settings,QHash<QString, QVector<double> > curveData,QHash<QString, QList<QList<double> > > transitionData);
    //deprecated
    QHash<QString, double>  savedUiSettings;
    QHash<QString, QVector<double> > savedCurveData;
    QHash<QString, QList<QList<double> > > savedTransitionData;
    QHash<QString, QString>   savedOtherData;


    //Qhashes containing the retrieved data
    QHash<QString, double> HFSTabQWTCounters_hash;
    QHash<QString, double> HFSTabQWTSliders_hash;
    QHash<QString, bool> HFSTabQRadiobuttons_hash;
    //fitting tab widget values
    QHash<QString, double> FittingTabQWTCounters_hash;
    QHash<QString, bool> FittingTabQCheckboxes_hash;
    //non ui data
    bool fitterState;
    //qwtplotdata
    QVector<QPointF> HFSplotMain;
    QVector<QPointF> HFSplotSpec;
    QVector<QPointF>fitPlotMain;
    QVector<QPointF> fitPlotSpec;
    QVector<QPointF> residualPlotMain;

    //data members
    double first_d;
    double last_d;
    QVector<double> xspectrum_d;
    QVector<double> yspectrum_d;

    //fit parameters
    double *xarray_f;
    double *yarray_f;
    QList<QList<double> > fitParameters_f;
    QList<QList<double> > fitConstants_f;
    QStringList fitterOutput_f;



private:
    QString name;


signals:

public slots:

};

#endif // CONTAINER_H
