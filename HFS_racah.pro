#-------------------------------------------------
#
# Project created by QtCreator 2011-04-02T12:37:30
#
#-------------------------------------------------

QT       += core gui
CONFIG += qwt
TARGET = HFS_racah
TEMPLATE = app
FORMS += hfs.ui
QMAKE_CXXFLAGS += -std=c++0x -U__STRICT_ANSI__

win32:QMAKE_LFLAGS = -enable-stdcall-fixup -Wl,-enable-auto-import -Wl,-enable-runtime-pseudo-reloc
SOURCES += main.cpp\
        hfs.cpp \
    hfsfunctions.cpp \
    fileparser.cpp \
    plotSet.cpp \
    setTable.cpp \
    container.cpp \
    lineshapes.cpp \
    dataconvert.cpp \
    miscfunctions.cpp \
    fileoperations.cpp \
    kingplotdata.cpp \
    kingplotdataModel.cpp \
    demingregression.cpp \
    gaussianfitfunctions.cpp \
    gaussianfitter.cpp \
    lorentzianfitfunctions.cpp \
    lorentzianfitter.cpp \
    fitter.cpp \
    session.cpp \
    voigtfitfunctions.cpp \
    voigtfitter.cpp

HEADERS  += hfs.h \
    hfsfunctions.h \
    fileparser.h \
    plotSet.h \
    setTable.h \
    container.h \
    lineshapes.h \
    dataconvert.h \
    miscfunctions.h \
    fileoperations.h \
    kingplotdata.h \
    kingplotdataModel.h \
    demingregression.h \
    gaussianfitfunctions.h \
    gaussianfitter.h \
    lorentzianfitfunctions.h \
    lorentzianfitter.h \
    fitter.h \
    session.h \
    voigtfitfunctions.h \
    voigtfitter.h

FORMS    += hfs.ui




#QWT libraries for , a QWT enviroment variable needed
unix|win32: LIBS += -L$(QWT)\lib -lqwt

unix|win32: INCLUDEPATH += $(QWT)\include
unix|win32: DEPENDPATH += $(QWT)\include


#GSL libraries for unix and windows, if found on path

unix: LIBS += -lgsl

unix: LIBS += -lgslcblas

unix: LIBS += -lm


win32: INCLUDEPATH += $(GSL)\include
win32: LIBS +=-L$(GSL)\lib-shared -lgsl -lgslcblas -lm
win32:QMAKE_POST_LINK += copy /Y $(QWT)\lib\qwt.dll $(DESTDIR) &
win32:QMAKE_POST_LINK += copy /Y $(QTDIR)\bin\QtCore4.dll $(DESTDIR) &
win32:QMAKE_POST_LINK  += copy /Y $(QTDIR)\bin\QtGui4.dll $(DESTDIR) &
win32:QMAKE_POST_LINK  += copy /Y $(QTDIR)\bin\QtSvg4.dll $(DESTDIR) &
win32:QMAKE_POST_LINK  += copy /Y  $(GSL)\bin\libgsl-0.dll $(DESTDIR) &
win32:QMAKE_POST_LINK  += copy /Y  $(GSL)\bin\libgslcblas-0.dll $(DESTDIR)&



win32:install_it.path = $$(USERPROFILE)
win32:install_it.files += $$PWD\massData.dat
win32:INSTALLS += install_it

OTHER_FILES += \
    README.txt \
    LICENSE.txt \
    massData.dat













































