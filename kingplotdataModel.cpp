
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "kingplotdataModel.h"

kingPlotDataModel::kingPlotDataModel(QObject *parent, QTableView *dataTable) :
    QStandardItemModel(parent)
{
    kpTable=dataTable;

    COLS=6;
    ROWS=0;
    connect(this, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(plotSlot()));
    connect(this, SIGNAL(itemChanged(QStandardItem*)), this,SLOT(addData(QStandardItem*)));
    this->setRowCount(ROWS);
    this->setColumnCount(COLS);
    //setup the table

    this->setHeaderData(0, Qt::Horizontal, QObject::tr("A1"));
    this->setHeaderData(1, Qt::Horizontal, QObject::tr("X"));
    this->setHeaderData(2, Qt::Horizontal, QObject::tr("dX"));
    this->setHeaderData(3, Qt::Horizontal, QObject::tr("A2"));
    this->setHeaderData(4, Qt::Horizontal, QObject::tr("Y"));
    this->setHeaderData(5, Qt::Horizontal, QObject::tr("dY"));

}
kingPlotDataModel::~kingPlotDataModel(){
    // delete KingPlotDataTable;

}

void  kingPlotDataModel::addRow(double A1, double x, double dx, double A2, double y, double dy){
    QList<QStandardItem*> rowItems;
    QVector<double> data;
    data<<A1<<x<<dx<<A2<<y<<dy;
    for(int col=0; col<COLS; col++){
        item_p= new QStandardItem();
        item_p->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
        item_p->setText(QString::number(data.at(col)));
        rowItems.append(item_p);
    }

    ROWS++;
    appendRow(rowItems);
    row=new QList<double>;
    row->append(A1);
    row->append(x);
    row->append(dx);
    row->append(A2);
    row->append(y);
    row->append(dy);

    storedData.append(row);
    tableItems.append(rowItems);


    plotSlot();
    //qDebug() << storedData.size();


}


void  kingPlotDataModel::deleteRow(){

    QModelIndexList selection =kpTable->selectionModel()->selectedRows();
    int selectionSize=selection.size();

    QList<QPersistentModelIndex> persistentSelection;
    QList<int> indexes;

    foreach(QModelIndex index, selection){
        persistentSelection.append(QPersistentModelIndex(index));
        indexes.append(index.row());
        //qDebug()<<index.row();
    }
    //qDebug()<<indexes;

    if(!selection.isEmpty()){
        // qDebug()<< selection.size();
        foreach(QPersistentModelIndex index, persistentSelection){
            int row_i;
            row_i=index.row();
            //qDebug() << row_i;
            removeRow(row_i);
        }
        //qDebug()<<storedData.size();

        for(int index_int=indexes.size()-1;index_int>=0; index_int--){
            //qDebug()<<index_int;
            //delete storedData.at(index_int);
            storedData.removeAt(index_int);
            tableItems.removeAt(index_int);
        }
        ROWS=ROWS-selectionSize;
    }
//        qDebug()<<storedData.size();
//        qDebug()<<tableItems.size();

    kpTable->selectionModel()->clearSelection();
    plotSlot();

}

QVector<double>  kingPlotDataModel::returnColumn(int column){
    QVector<double> rowData;
    for(int row=0; row<storedData.size(); row++){

        rowData.append(storedData.at(row)->at(column));
    }
    return rowData;
}
QVector<double> kingPlotDataModel::returnA1Data(){
    return returnColumn(0);
}
QVector<double> kingPlotDataModel::returnXData(){
    return returnColumn(1);
}
QVector<double> kingPlotDataModel::returnDXData(){
    return returnColumn(2);
}
QVector<double> kingPlotDataModel::returnA2Data(){
    return returnColumn(3);
}
QVector<double> kingPlotDataModel::returnYData(){
    return returnColumn(4);
}
QVector<double> kingPlotDataModel::returnDYData(){
    return returnColumn(5);
}

void kingPlotDataModel::plotSlot(){
    QVector<double> A1;
    QVector<double> x;
    QVector<double> dx;
    QVector<double> A2;
    QVector<double> y;
    QVector<double> dy;

    A1=returnA1Data();
    x=returnXData();
    dx=returnDXData();
    A2=returnA2Data();
    y=returnYData();
    dy=returnDYData();
    //qDebug()<< x<<"size "<<x.size();
    //qDebug()<<ROWS;
    //emit sendDataToPlot(returnXData(),returnDXData(),returnYData(), returnDYData());
    emit sendDataToPlot(A1, x, dx, A2, y, dy);

}

void kingPlotDataModel::addData(QStandardItem* selection){
    int row=selection->row();
    int column=selection->column();
    double data=selection->text().toDouble();
    //qDebug()<<row<< data;
    storedData.at(row)->replace(column, data);

}

//Loads the ing plot data in format A1 x dx A2 y Dy. Columnes must be separated by tab
void kingPlotDataModel::loadData(){
    QVector<double> A1;
    QVector<double> x;
    QVector<double> dx;
    QVector<double> A2;
    QVector<double> y;
    QVector<double> dy;
    bool rightColumns=true;

    //open datafile and store the columns to the QVectros above
    QString fileName = QFileDialog::getOpenFileName( 0, tr("Open King Plot data"), QDir::homePath());  //get the filename
    if(fileName!=0){
        QFile file(fileName);
        if (file.open(QIODevice::ReadWrite)){
            while (!file.atEnd()) {
                QString line = file.readLine();
                QStringList split=line.split("\t");
                //qDebug()<< split;
                if(split.size()==COLS){
                    A1.append(split.at(0).toDouble());
                    x.append(split.at(1).toDouble());
                    dx.append(split.at(2).toDouble());
                    A2.append(split.at(3).toDouble());
                    y.append(split.at(4).toDouble());
                    dy.append(split.at(5).toDouble());
                }
                else{
                    rightColumns=false;
                }
            }
        }
        //Add needed amount of columns along with th data
        int rowsNeeded=A1.size();
        if(rightColumns){
            for(int r=0; r<rowsNeeded; r++){
                addRow(A1.at(r), x.at(r),dx.at(r),A2.at(r),y.at(r),dy.at(r));
            }


        }
    }
    //qDebug() << A1;
}
