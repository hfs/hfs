#ifndef FILEOPERATIONS_H
#define FILEOPERATIONS_H

#include <QObject>
#include <QFile>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QVector>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_renderer.h>
#include "setTable.h"
#include "ui_hfs.h"
#include <QTextStream>
#include <fileparser.h>
#include <QDebug>
#include <QVector>


class fileOperations : public QObject
{
    Q_OBJECT
public:
    explicit fileOperations(QObject *parent = 0,Ui_HFS *ui=0, setTable *table=0);

    Ui_HFS *userI;                  //The user interface

    setTable *HFSmodel;             //table for the HFS data

    fileParser *specdata;   // spectum data parser, loads data from files

signals:

public slots:

    void saveTableToFile();

    void savePlotToPDF(QwtPlot *plot);

    void saveDatatoFile(QVector<double> x, QVector<double> y);

    QVector<QVector<double> > openSpectrum();

};

#endif // FILEOPERATIONS_H
