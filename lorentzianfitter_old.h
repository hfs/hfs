#ifndef LORENTZIANFITTER_OLD_H
#define LORENTZIANFITTER_OLD_H

#include <QStringList>
#include <QObject>
#include <lorentzianfitfunctions_old.h>
#include <dataconvert.h>

class lorentzianFitter_OLD : public QObject
{
    Q_OBJECT
public:
    explicit lorentzianFitter_OLD(QObject *parent = 0, size_t no_of_functions=0,const size_t no_of_ind_variables=0, int no_of_peaks=1, double *hfsData=0, double *xAxis=0,double *hfsErrorData=0);
    ~lorentzianFitter_OLD();
    void mainFitter(); //main fitter method
    dataConvert *convert;
    lorentzianFitFunctions_old *functions; //lorentzian functions, not to be used for fitting!
    //this struct holds the static information needed for the fit
    //needs to be identical with the one in lorentzianfitfunction.h
    //the order of elements here is important due to the way the sruct is initialised
    struct data {
        size_t n;              //number of functions == number of datapoints
        size_t p;              // number of fit parameters
        double *y;             //data array, contains the data to be fitted
        double *xdata;
        double *sigma;         //standard deviation or error of each datapoint
        int peaks;             //number of peaks
        double *alphal; //alphas and betas as an array
        double *betal;
        double *alphau;
        double *betau;
        double *I;  //peak intensities

        //A Qhash containing information on wheter a certain parameter is locked or not
        QHash<QString, bool> lockedVariables;


    };

    //parameters related to the function properties
    size_t n;              //number of functions == number of datapoints
    size_t p;              // number of fit parameters
    double *y;             //data array, contains the data to be fitted
    double *xdata;             //ada array for the x-axis
    double *sigma;         //standard deviation or error of each datapoint
    int peaks;             //number of peaks
    double *centroids;     //peak centroids
    double *initialValues;
    //alphas and betas as an array
    double *alphal;
    double *betal;
    double *alphau;
    double *betau;
    double *I;


    //fitter control parameters

    double iterations;      //maximum number of fit iterations
    //Absolute error epsabs and relative error epsrel to the current position x.
    double epsabs;
    double epsrel;

    const gsl_multifit_fdfsolver_type *T; //type of the optimization algorithm
    gsl_multifit_fdfsolver *s;            //the solver
    gsl_multifit_function_fdf f;        //This data type defines a general system of functions with arbitrary parameters and the corresponding Jacobian matrix of derivatives,
    gsl_matrix *covar;                  //covariant matris for the fit results
    gsl_vector_view x;
    int status;
    unsigned int i, iter;

    //temporary printer for fit output
    void print_state (size_t iter, gsl_multifit_fdfsolver * s);

    // return the fit results;
    QList<double>  results();
    QList<double>  error();


    //set fitting parameters
    void setInitialValues(QList<QList<double> >initialValuesList, int number_of_variables);
    void setConstantValues(QList<QList<double> > constants, int number_of_constants);

    //set fitter termination parameters
    void setFitTerminationParameters(double iters, double rel, double abs);

    //return fitted data for plotting

    QVector<double> returnFitData();
    QVector<double> lorentzianProfileCalculator(int n_f, double *xdata_f,int peaks_f, double x0, double yzero,double Al,double Bl, double Au, double Bu, double w,double Ic, double *I_f,double *alphal_f,double* betal_f,double *alphau_f,double *betau_f);
    QVector<double> yFit; //QVector for the fit y-data

    //Qstringlist of fit routine output
    QStringList returnFitterOutput();

    QStringList fitterOutput;


    //Return residual data

    QVector<double> returnResidualData();

    //Set A Qhash containing information on wheter a certain parameter is locked or not
    void setLockedVariables(QHash<QString, bool> loVar);

    QHash<QString, bool> lockedVariables;

    //return fit results as double



    double returnxc0f(){
        return xc0f;
    }
    double returnxc0fErr(){
        return xc0fErr;
    }

    double returnResult(QString parameter){
        if(fittedParameters.contains(parameter)){
            double value=fittedParameters.value(parameter);
            return value;
        }
    }

private:
    //fit results;

    double chisqDoff;
    double xc0f;
    double xc0fErr;
    double yzerof;
    double yzerofErr;
    double Alf;
    double AlfErr;
    double Blf;
    double BlfErr;
    double Auf;
    double AufErr;
    double Buf;
    double BufErr;
    double wf;
    double wfErr;
    double Iff;
    double IffErr;
    void replaceFittedParameters();
    QHash<QString, double> fittedParameters;



signals:

public slots:



};

#endif // LORENTZIANFITTER_OLD_H
