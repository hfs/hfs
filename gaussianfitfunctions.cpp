
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "gaussianfitfunctions.h"
gaussianFitFunctions::gaussianFitFunctions()
{
}

double gaussianFitFunctions::hfsCenterOfGravity(double xcz_fVal, double Al_fVal, double Bl_fVal, double Au_fVal, double Bu_fVal, double all_fVal, double bel_fVal, double alu_fVal, double beu_fVal){
    double cent=xcz_fVal+alu_fVal*Au_fVal+beu_fVal*Bu_fVal-all_fVal*Al_fVal-bel_fVal*Bl_fVal;
    return cent;
}

double gaussianFitFunctions::gaussianFunction(double t_fVal, double hfsCenterOfGravity_fVal, double yzero_fVal, double w_fVal, double I_fVal){
    double y0=yzero_fVal;
    double a=I_fVal;
    double x=t_fVal;
    double b= hfsCenterOfGravity_fVal;
    double c=w_fVal;
    double gaussian=0;
    double innerExp=0;
    innerExp=-pow((x-b),2)/(2*pow(c,2));
    gaussian= a*exp(innerExp);
    return gaussian;
}

double gaussianFitFunctions::gaussianCentDerivative(double t_fVal, double hfsCenterOfGravity_fVal,double w_fVal, double I_fVal){
    //      (d)/(db)(y+a exp(-(x-b)^2/(2 c^2))) = (a (x-b) e^(-(b-x)^2/(2 c^2)))/c^2
    double a=I_fVal;
    double x=t_fVal;
    double b= hfsCenterOfGravity_fVal;
    double c=w_fVal;
    double gaussian=gaussianFunction(x,b,0, c, a);

    double der=((x-b))/pow(c,2);
    return der*gaussian;

}

double gaussianFitFunctions::gaussianWDerivative(double t_fVal, double hfsCenterOfGravity_fVal, double w_fVal, double I_fVal){
    //      (d)/(dc)(y+a exp(-(x-b)^2/(2 c^2))) = (a (b-x)^2 e^(-(b-x)^2/(2 c^2)))/c^3
    double a=I_fVal;
    double x=t_fVal;
    double b= hfsCenterOfGravity_fVal;
    double c=w_fVal;
    double gaussian=gaussianFunction(x,b,0, c, a);
    double der=(pow((x-b),2))/pow(c,3);
    return der*gaussian;
}

double gaussianFitFunctions::gaussianIDerivative(double t_fVal, double hfsCenterOfGravity_fVal, double w_fVal){
    return gaussianFunction(t_fVal, hfsCenterOfGravity_fVal, 0, w_fVal, 1);
}

double gaussianFitFunctions::gaussianYZeroDerivative(){
    return 1;
}


int gaussianFitFunctions::gaussian_f(const gsl_vector *x, void *data, gsl_vector *f){

    //get fit constants and other non-fitted parameters from the data struct
    size_t n = ((struct gaussianFitFunctions::data*)data)->n;

    double *y = ((struct gaussianFitFunctions::data*)data)->y;
    double *sigma = ((struct gaussianFitFunctions::data*)data)->sigma;
    int peaks=((struct gaussianFitFunctions::data*)data)->peaks;
    double *xdata=((struct gaussianFitFunctions::data*)data)->xdata;

    //fetch fit constants
    double *alphal=((struct gaussianFitFunctions::data*)data)->alphal;
    double *betal=((struct gaussianFitFunctions::data*)data)->betal;
    double *alphau=((struct gaussianFitFunctions::data*)data)->alphau;
    double *betau=((struct gaussianFitFunctions::data*)data)->betau;
    double *I_const=((struct gaussianFitFunctions::data*)data)->I;

    // shared fitparameters ar stored in a gsl_vector
    double x0= gsl_vector_get (x, 0);
    double yzero= gsl_vector_get (x, 1);
    double Al= gsl_vector_get (x, 2);
    double Bl= gsl_vector_get (x, 3);
    double Au= gsl_vector_get (x, 4);
    double Bu= gsl_vector_get (x, 5);
    double w=gsl_vector_get (x, 6);
    double I=gsl_vector_get (x, 7);
    //Calculations for the fitter
    //calculate the multipeak gaussian spectrum
    size_t i;           //size of the data array
    for(i=0;i<n;i++){
        double t=xdata[i];//a point in the x-axis
        double Yi=0;      //the corresponding y-asix value
        for(int peak=0; peak<peaks; peak++){
            double centerOfGravity=hfsCenterOfGravity(x0, Al, Bl, Au, Bu, alphal[peak], betal[peak], alphau[peak], betau[peak]);
            Yi=Yi+gaussianFunction(t, centerOfGravity, yzero, w, I_const[peak]);

        }
        Yi=I*Yi+yzero; // put the intensity scaler and background here rather than to individual peaks
        gsl_vector_set (f, i, (Yi - y[i])/sigma[i]);
    }
    return GSL_SUCCESS;
}


int gaussianFitFunctions::gaussian_fd(const gsl_vector *x, void *data, gsl_matrix *J){

    //get fit constants and other non-fitted parameters from the data struct
    size_t n = ((struct gaussianFitFunctions::data*)data)->n;

    double *sigma = ((struct gaussianFitFunctions::data*)data)->sigma;
    int peaks=((struct gaussianFitFunctions::data*)data)->peaks;
    double *xdata=((struct gaussianFitFunctions::data*)data)->xdata;

    //fetch fit constants
    double *alphal=((struct gaussianFitFunctions::data*)data)->alphal;
    double *betal=((struct gaussianFitFunctions::data*)data)->betal;
    double *alphau=((struct gaussianFitFunctions::data*)data)->alphau;
    double *betau=((struct gaussianFitFunctions::data*)data)->betau;
    double *I_const=((struct gaussianFitFunctions::data*)data)->I;

    //fetch parameter locks
    bool X0Lock=((struct gaussianFitFunctions::data*)data)->X0Lock;
    bool yzeroLock=((struct gaussianFitFunctions::data*)data)->yzeroLock;
    bool AlLock=((struct gaussianFitFunctions::data*)data)->AlLock;
    bool BlLock=((struct gaussianFitFunctions::data*)data)->BlLock;
    bool AuLock=((struct gaussianFitFunctions::data*)data)->AuLock;
    bool BuLock=((struct gaussianFitFunctions::data*)data)->BuLock;
    bool wLock=((struct gaussianFitFunctions::data*)data)->wLock;
    bool ILock=((struct gaussianFitFunctions::data*)data)->ILock;

    // shared fitparameters ar stored in a gsl_vector
    double x0= gsl_vector_get (x, 0);
    //double yzero= gsl_vector_get (x, 1);
    double Al= gsl_vector_get (x, 2);
    double Bl= gsl_vector_get (x, 3);
    double Au= gsl_vector_get (x, 4);
    double Bu= gsl_vector_get (x, 5);
    double w=gsl_vector_get (x, 6);
    double I=gsl_vector_get (x, 7);

    size_t i;
    // jacobian
    for(i=0;i<n;i++){   //for each datapoint
        double t = xdata[i];

        //for each shared variable namely  xcz << yzero<< AsBs
        // there are 8 shared fit parameters
        //derivate against centroid, the function is the same for all xcz, Al, Bl, Au and Bu
        double YX0=0;     //Derivative of the x-offset
        double YyZero=0;        //Derivative of the y offset
        double Yal=0;           //Derivative of the Al
        double Ybl=0;           //Derivative of the Bl
        double Yau=0;           //Derivative of the Au
        double Ybu=0;           //Derivative of the Bu
        double Ywidth=0;        //Derivative of the width
        double Yintensity=0;    //Derivative of the intensity
        for(int peak=0;peak<peaks; peak++){
            double centerOfGravity=hfsCenterOfGravity(x0, Al, Bl, Au, Bu, alphal[peak], betal[peak], alphau[peak], betau[peak]);
            double GCDer=gaussianCentDerivative(t, centerOfGravity, w, I*I_const[peak]);
            YX0=YX0+GCDer;
            YyZero=+YyZero+gaussianYZeroDerivative();
            Yal=Yal+GCDer*-alphal[peak];
            Ybl=Ybl+GCDer*-betal[peak];
            Yau=Yau+GCDer*alphau[peak];
            Ybu=Ybu+GCDer*betau[peak];
            Ywidth=Ywidth+gaussianWDerivative(t, centerOfGravity, w, I*I_const[peak]);
            Yintensity=Yintensity+gaussianIDerivative(t, centerOfGravity, w);
            // as the centroid parameters, xcz, AL, Bl, Au and Bu are shared by each gaussian forming the sumfunction,
            // the derivate of the sumfunction is a sum of the derivatives. The same goes also for the zeropoint yzero,
            // its just the number of peaks
        }
        //if the derivative against the parameter is zero,
        //the fit routine thinks that the parameter is already at its optimal value.
        //If now the derivative is set to zero artificially, the value of the parameter is not changed.
        if(X0Lock){
            YX0=0;
        }

        if(yzeroLock){
            peaks=0;
        }

        if(AlLock){
            Yal=0;
        }

        if(BlLock){
            Ybl=0;
        }

        if(AuLock){
            Yau=0;
        }

        if(BuLock){
            Ybu=0;
        }
        if(wLock){
           Ywidth=0;
        }
        if(ILock){
            Yintensity=0;
        }
        gsl_matrix_set (J, i, 0,YX0/sigma[i]);
        gsl_matrix_set (J, i, 1, YyZero/sigma[i]);
        gsl_matrix_set (J, i, 2,  Yal/sigma[i]);
        gsl_matrix_set (J, i, 3,  Ybl/sigma[i]);
        gsl_matrix_set (J, i, 4,  Yau/sigma[i]);
        gsl_matrix_set (J, i, 5,  Ybu/sigma[i]);
        gsl_matrix_set (J, i, 6,  Ywidth/sigma[i]);
        gsl_matrix_set (J, i, 7,  Yintensity/sigma[i]);

    }
    return GSL_SUCCESS;
}

int gaussianFitFunctions::gaussian_fdf(const gsl_vector *x, void *data, gsl_vector *f, gsl_matrix *J){
    gaussianFitFunctions::gaussian_f(x,data,f);
    gaussianFitFunctions::gaussian_fd(x, data, J);
    return GSL_SUCCESS;
}
