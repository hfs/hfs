//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



#include "fitter.h"

fitter::fitter(std::string profile)
{

    profileType=profile;
    lorentzian=NULL;
    gaussian=NULL;
    voigt=NULL;
    if(profileType=="Lorentzian"){
        lorentzian=new lorentzianFitter();
    }
    if(profileType=="Gaussian"){
        gaussian=new gaussianFitter();
    }
    if(profileType=="Voigt"){
        voigt=new voigtFitter();
    }

}

fitter::~fitter(){
    if(gaussian!=NULL){
        delete gaussian;
    }
    if(lorentzian!=NULL){
        delete lorentzian;
    }
    if(voigt!=NULL){
        delete voigt;
    }
}

void fitter::setData(int dataSize, double *xarray, double *yarray, double *sigma, int numberOfPeaks){
    if(profileType=="Lorentzian"){
        lorentzian->setData(dataSize, yarray, xarray, sigma, numberOfPeaks);
    }
    if(profileType=="Gaussian"){
        gaussian->setData(dataSize, yarray, xarray, sigma, numberOfPeaks);
    }
    if(profileType=="Voigt"){
        voigt->setData(dataSize, yarray, xarray, sigma, numberOfPeaks);
    }

}


void fitter::setConstants(double *lowerAlphas, double *lowerBetas, double *upperAlphas, double *upperBetas, double *intensities){
    if(profileType=="Lorentzian"){
        lorentzian->setFitConstants(lowerAlphas, lowerBetas, upperAlphas, upperBetas, intensities);
    }
    if(profileType=="Gaussian"){
        gaussian->setFitConstants(lowerAlphas, lowerBetas, upperAlphas, upperBetas, intensities);
    }
    if(profileType=="Voigt"){
        voigt->setFitConstants(lowerAlphas, lowerBetas, upperAlphas, upperBetas, intensities);
    }

}


void fitter::setInitialValues(double centerOfMass, double yOffset, double AL, double BL, double AU, double BU, double width, double intensity,double width2){
    if(profileType=="Lorentzian"){
        lorentzian->setInitialValues(centerOfMass,yOffset, AL, BL, AU, BU, width, intensity);
    }
    if(profileType=="Gaussian"){
        gaussian->setInitialValues(centerOfMass,yOffset, AL, BL, AU, BU, width, intensity);
    }
    if(profileType=="Voigt"){
        voigt->setInitialValues(centerOfMass,yOffset, AL, BL, AU, BU, width, intensity,width2);
    }
}

void fitter::setLockedParameters(bool X0Lock, bool yzeroLock, bool AlLock, bool BlLock, bool AuLock, bool BuLock, bool wLock, bool ILock, bool w2Lock){
    if(profileType=="Lorentzian"){
        lorentzian->setLockedParameters(X0Lock, yzeroLock, AlLock,BlLock, AuLock, BuLock, wLock, ILock );
    }
    if(profileType=="Gaussian"){
        gaussian->setLockedParameters(X0Lock, yzeroLock, AlLock,BlLock, AuLock, BuLock, wLock, ILock );
    }
    if(profileType=="Voigt"){
        voigt->setLockedParameters(X0Lock, yzeroLock, AlLock,BlLock, AuLock, BuLock, wLock, ILock,w2Lock );
    }
}

void fitter::setFitterControlParameters(double numberOfIterations, double absoluteError, double relativeError){
    if(profileType=="Lorentzian"){
        lorentzian->setFitterControlParameters(numberOfIterations, absoluteError, relativeError);
    }
    if(profileType=="Gaussian"){
        gaussian->setFitterControlParameters(numberOfIterations, absoluteError, relativeError);
    }
    if(profileType=="Voigt"){
        voigt->setFitterControlParameters(numberOfIterations, absoluteError, relativeError);
    }
}


void fitter::setSolver(){
    if(profileType=="Lorentzian"){
        lorentzian->setSolver();
    }
    if(profileType=="Gaussian"){
        gaussian->setSolver();
    }
    if(profileType=="Voigt"){
        voigt->setSolver();
    }
}

void fitter::iterateSolver(){
    if(profileType=="Lorentzian"){
        lorentzian->iterateSolver();
    }
    if(profileType=="Gaussian"){
        gaussian->iterateSolver();
    }
    if(profileType=="Voigt"){
        voigt->iterateSolver();
    }
}
void fitter::clearSolver(){
    if(profileType=="Lorentzian"){
        lorentzian->clearSolver();
    }
    if(profileType=="Gaussian"){
        gaussian->clearSolver();
    }
    if(profileType=="Voigt"){
        voigt->clearSolver();
    }
}

std::vector<double> fitter::returnProfile(double X0_val, double yOffset_val, double Al_val, double Bl_val, double Au_val, double Bu_val, double w_val, double I_val, double w2_val){
    if(profileType=="Lorentzian"){
        return lorentzian->returnProfile(X0_val, yOffset_val, Al_val, Bl_val, Au_val, Bu_val, w_val, I_val);
    }
    if(profileType=="Gaussian"){
        return gaussian->returnProfile(X0_val, yOffset_val, Al_val, Bl_val, Au_val, Bu_val, w_val, I_val);
    }
    if(profileType=="Voigt"){
        return voigt->returnProfile(X0_val, yOffset_val, Al_val, Bl_val, Au_val, Bu_val, w_val, I_val,w2_val);
    }
    else{
        std::vector<double>  nullValue;
        nullValue.push_back(0);
        return nullValue;
    }
}
std::unordered_map<std::string, double> fitter::returnResult(){
    if(profileType=="Lorentzian"){
        return lorentzian->returnResult();
    }
    if(profileType=="Gaussian"){
        return gaussian->returnResult();
    }
    if(profileType=="Voigt"){
        return voigt->returnResult();
    }
    else{
        std::unordered_map<std::string, double> nullValue;
        nullValue["null"]=0;
        return nullValue;
    }
}
std::unordered_map<std::string, std::string> fitter::returnFitterStatus(){
    if(profileType=="Lorentzian"){
        return lorentzian->returnFitterStatus();
    }
    if(profileType=="Gaussian"){
        return gaussian->returnFitterStatus();
    }
    if(profileType=="Voigt"){
        return voigt->returnFitterStatus();
    }
    else{
        std::unordered_map<std::string, std::string> nullValue;
        nullValue["null"]="null";
        return nullValue;
    }
}
