#ifndef PLOTSET_H
#define PLOTSET_H

#include <QObject>
#include <QMenu>
#include <QPoint>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_series_data.h>
#include <qwt_math.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_picker.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_dict.h>
#include <qwt_curve_fitter.h>
#include <fileoperations.h>

#include <QPrinter>

class plotSet : public QwtPlot
{
    Q_OBJECT
public:
    explicit plotSet(QwtPlot *plot=NULL);
    void reset(double xminus, double xplus);
    void saveImage(QString fileName);
    virtual ~plotSet();
    void setInd(QList<QVector<double> > ilor, QVector<double> xr);  //function to set individual plots
    void allocate(int size);
    void addZoomAndPan();

    QList<QwtPlotCurve*> curves;

    QwtPlot *plot_d;   //plot widget

    QwtPlotCurve *mainPlot;  //HFS plot

    QwtWeedingCurveFitter *fitter;  //fitter for the HFSplot curve to reduce amount of pints in the curve

    QwtPlotCurve *spectrum;  //measured spectrum

    QwtPlotCurve *lor;       //individual lorentzians

    QwtPlotMagnifier *zoom;

    QwtPlotPanner *pan;

    QwtPlotGrid *grid;

    QwtPlotPicker *picker;

    fileOperations *fileOp;
signals:

public slots:
    void showHideinLo(bool button);
    void ShowPlotContextMenu(const QPoint& pos);

};

#endif // PLOTSET_H
