
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "kingplotdata.h"

kingPlotData::kingPlotData(QObject *parent, QwtPlot *kingPlot) :
    QObject(parent)
{
    kingPlotWidget=kingPlot;
    kingPlotCurve = new QwtPlotCurve();
    kingPlotCurve->attach(kingPlotWidget);
    kingPlotWidget->setAxisAutoScale(QwtPlot::xBottom);
    kingPlotWidget->setAxisAutoScale(QwtPlot::yLeft);
    kingPlotWidget->setAxisTitle(QwtPlot::xBottom, "A1 centroid");
    kingPlotWidget->setAxisTitle(QwtPlot::yLeft, "A2 centroid");
    kingPlotWidget->setCanvasBackground(Qt::white);
    kingPlotCurve->setStyle(QwtPlotCurve::NoCurve);

    symbol = new QwtSymbol(QwtSymbol::Diamond);
    symbol->setSize(10);
    symbol->setPen(QPen(Qt::red));
    kingPlotCurve->setSymbol(symbol);

    //Error bars

    YerrorBar = new QwtIntervalSymbol(QwtIntervalSymbol::Bar);
    YerrorBar->setWidth(8); // should be something even
    YerrorBar->setPen(QPen(Qt::black));

    kingPlotYErrorBar = new QwtPlotIntervalCurve();
    kingPlotYErrorBar->setStyle(QwtPlotIntervalCurve::NoCurve);
    kingPlotYErrorBar->setSymbol(YerrorBar);
    kingPlotYErrorBar->setOrientation(Qt::Vertical);
    kingPlotYErrorBar->attach(kingPlotWidget);

    XerrorBar = new QwtIntervalSymbol(QwtIntervalSymbol::Bar);
    XerrorBar->setWidth(8); // should be something even
    XerrorBar->setPen(QPen(Qt::black));

    kingPlotXErrorBar = new QwtPlotIntervalCurve();
    kingPlotXErrorBar->setStyle(QwtPlotIntervalCurve::NoCurve);
    kingPlotXErrorBar->setSymbol(XerrorBar);
    kingPlotXErrorBar->setOrientation(Qt::Horizontal);
    kingPlotXErrorBar->attach(kingPlotWidget);

    //Set fitter
    fitData=new demingRegression(this);

    //set fit curve

    fitCurve = new QwtPlotCurve();
    fitCurve->attach(kingPlotWidget);
    fitCurve->setPen(QPen(Qt::red));

    //setup masstable
    openMassTable();

    willModifyX=false;
    willModifyY=false;

}

kingPlotData::~kingPlotData(){

    delete kingPlotCurve;
    delete kingPlotXErrorBar;
    delete kingPlotYErrorBar;

}

void kingPlotData::setOriginalData(QVector<double> A1, QVector<double> x, QVector<double> dx, QVector<double> A2, QVector<double> y, QVector<double> dy){
    A1_o.clear();
    A1_o=A1;
    x_o.clear();
    x_o=x;
    dx_o.clear();
    dx_o=dx;
    A2_o.clear();
    A2_o=A2;
    y_o.clear();
    y_o=y;
    dy_o.clear();
    dy_o=dy;
    plotData();
    QVector<double> zero;
    fitCurve->setSamples(zero,zero);
    //qDebug()<<x_o;
    //qDebug()<<A1_o << x_o<<dx_o<<A2_o<<y_o<<dy_o;
}

void kingPlotData::setNucleiData(int Z_o, int A1_ov, int A2_ov, bool modifyx, bool modifyy){
    Z=Z_o;
    A1=A1_ov;
    A2=A2_ov;
    willModifyX=modifyx;
    willModifyY=modifyy;
    plotData();

}
double kingPlotData::calculateReferencePair(){
    double ref1=returnMass(Z,A1)-returnMass(Z,A2);
    double ref2=returnMass(Z,A1)*returnMass(Z,A2);

    return ref1/ref2;


}

double kingPlotData::calculateMassFactor(int A_i, int A_j){
    double lower=returnMass(Z,A_i)-returnMass(Z,A_j);
    double upper=returnMass(Z,A_i)*returnMass(Z,A_j);
    return upper/(lower);
}

void kingPlotData::plotData(){
    modifyData();
    kingPlotCurve->setSamples(xy_m);
    kingPlotYErrorBar->setSamples(yErrorBar);
    kingPlotXErrorBar->setSamples(xErrorBar);
    kingPlotWidget->replot();
}

//Open the mass data file and store the data
void kingPlotData::openMassTable(){
    QString fileName ="./massData.dat";
    QFile file(fileName);
    //qDebug()<<file.exists();
    if (file.open(QIODevice::ReadOnly)){
        while (!file.atEnd()) {
            QString line = file.readLine();
            QStringList split=line.split("\t");
            //qDebug()<<split;
            int Z=split.at(0).toInt();
            int A=split.at(1).toInt();
            QString mass=split.at(2);
            //qDebug()<<Z<< A<< mass;
            masses[Z][A]=mass;
            //qDebug()<<masses[Z][A];
        }
    }
}

double kingPlotData::returnMass(int Z, int A){
    QString massString=masses[Z][A];
    double mass=massString.toDouble();
    return mass;
}

void kingPlotData::modifyData(){
    //temp function
    xy_m.clear();
    dxdy_m.clear();
    //String output;
    modifiedDataOutput.clear();
    modifiedDataOutput.append("\n");
    modifiedDataOutput.append("Data that was fitted");
    modifiedDataOutput.append("\n");
    modifiedDataOutput.append(" x_m \t dx_m \t y_m \t dy_m :");
    modifiedDataOutput.append("\n");

    double ref=calculateReferencePair();
    double x_o_modified;
    double y_o_modified;
    double dx_o_modified;
    double dy_o_modified;
    for(int idx=0;idx<x_o.size(); idx++){
        //mass modification factors
        int A_i=A1_o.at(idx);
        int A_j=A2_o.at(idx);

        //modify the original data
        if(willModifyX && !willModifyY){
            double massFactor=calculateMassFactor(A_i,A_j);
            x_o_modified=x_o.at(idx)*ref*massFactor;
            dx_o_modified=dx_o.at(idx)*fabs(ref*massFactor);
            y_o_modified=y_o.at(idx);
            dy_o_modified=dy_o.at(idx);
        }
        else if(willModifyY && !willModifyX){
            double massFactor=calculateMassFactor(A_i,A_j);
            y_o_modified=y_o.at(idx)*ref*massFactor;
            dy_o_modified=dy_o.at(idx)*fabs(ref*massFactor);
            x_o_modified=x_o.at(idx);
            dx_o_modified=dx_o.at(idx);
        }
        else if(willModifyX && willModifyY){
            double massFactor=calculateMassFactor(A_i,A_j);
            x_o_modified=x_o.at(idx)*ref*massFactor;
            dx_o_modified=dx_o.at(idx)*fabs(ref*massFactor);
            y_o_modified=y_o.at(idx)*ref*massFactor;
            dy_o_modified=dy_o.at(idx)*fabs(ref*massFactor);

        }
        else{
            x_o_modified=x_o.at(idx);
            y_o_modified=y_o.at(idx);
            dx_o_modified=dx_o.at(idx);
            dy_o_modified=dy_o.at(idx);
        }


        QPointF xy;
        xy.setX(x_o_modified);
        xy.setY(y_o_modified);
        xy_m.append(xy);
        QPointF dxdy;
        dxdy.setX(dx_o_modified);
        dxdy.setY(dy_o_modified);
        dxdy_m.append(dxdy);

        modifiedDataOutput.append(QString::number(x_o_modified));
        modifiedDataOutput.append("\t");
        modifiedDataOutput.append(QString::number(dx_o_modified));
        modifiedDataOutput.append("\t");
        modifiedDataOutput.append(QString::number(y_o_modified));
        modifiedDataOutput.append("\t");
        modifiedDataOutput.append(QString::number(dy_o_modified));
        modifiedDataOutput.append("\n");


    }

    //Create error bars for x and y directions
    yErrorBar.clear();
    xErrorBar.clear();
    for(int i=0;i<xy_m.size();i++){
        double yvalue=xy_m.at(i).y();
        double xvalue=xy_m.at(i).x();
        double yabsError=dxdy_m.at(i).y();
        double xabsError=dxdy_m.at(i).x();
        //qDebug()<< xabsError;
        double ymin=yvalue-yabsError;
        double ymax=yvalue+yabsError;
        double xmin=xvalue-xabsError;
        double xmax=xvalue+xabsError;
        yErrorBar.append(QwtIntervalSample(xvalue, QwtInterval(ymin, ymax)));
        xErrorBar.append(QwtIntervalSample(yvalue, QwtInterval(xmin, xmax)));
    }



}

void kingPlotData::fitKingPlot(){
    fitData->setData(xy_m, dxdy_m);
    QVector<QPointF> fittedCurveData;
    fittedCurveData=fitData->returnFitCurve();
    fitCurve->setSamples(fittedCurveData);
    //qDebug()<< fittedCurveData;
    kingPlotWidget->replot();
    QString output=fitData->returnFitParameters();
    output.append(modifiedDataOutput);
    emit sendFitOutPut(output);
}
