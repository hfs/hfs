
//Copyright (C) Mikael Reponen

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "gaussianfitter.h"

gaussianFitter::gaussianFitter()
{
    //Initialise the fit results ,ap
    //This map will store the fit results
    //and these results will also be used as initial values for
    //the fitting runs following the initial run
    fitResults["reducedChiSquare"]=0;
    fitResults["centerOfMass"]=0;
    fitResults["centerOfMassError"]=0;
    fitResults["yOffset"]=0;
    fitResults["yOffSetError"]=0;
    fitResults["lowerA"]=0;
    fitResults["lowerAError"]=0;
    fitResults["lowerB"]=0;
    fitResults["lowerBError"]=0;
    fitResults["upperA"]=0;
    fitResults["upperAError"]=0;
    fitResults["upperB"]=0;
    fitResults["upperBError"]=0;
    fitResults["width"]=0;
    fitResults["widthError"]=0;
    fitResults["intensity"]=0;
    fitResults["intensityError"]=0;

    std::ostringstream iterationsString;
    iterationsString<<iterations;
    fitterStatus["Iteratations"]=iterationsString.str();
    fitterStatus["errorStatus"]=gsl_strerror (status);


    //Initial values
    initialValues=NULL;

}

gaussianFitter::~gaussianFitter(){
    if(initialValues!=NULL){
        delete[] initialValues;
    }
}


extern "C"{
int gaussianFitFunctions_callGaussian_f(const gsl_vector *x, void *data, gsl_vector *f)
{
    gaussianFitFunctions *fnc = (gaussianFitFunctions *) data;
    return fnc->gaussian_f(x, data, f);
}

}
extern "C"{
int gaussianFitFunctions_callGaussian_fd(const gsl_vector *x, void *data, gsl_matrix *J)
{
    gaussianFitFunctions *fnc = (gaussianFitFunctions*) data;
    return fnc->gaussian_fd(x, data, J);
}
}

extern"C"{
int gaussianFitFunctions_callGaussian_fdf(const gsl_vector *x, void *data, gsl_vector *f, gsl_matrix *J){
    gaussianFitFunctions *fnc = (gaussianFitFunctions *) data;
    return fnc->gaussian_fdf(x, data,f, J);
}
}

void gaussianFitter::setData(int numberOfDatapoints, double *yData, double *xData, double *yError, int numberOfPeaks){
    constantData.n=numberOfDatapoints;
    constantData.y=yData;
    constantData.xdata=xData;
    constantData.sigma=yError;
    constantData.peaks=numberOfPeaks;
}

void gaussianFitter::setFitConstants(double *lowerAlphas, double *lowerBetas, double *upperAlphas, double *upperBetas, double *peakIntensities){
    constantData.alphal=lowerAlphas;
    constantData.betal=lowerBetas;
    constantData.alphau=upperAlphas;
    constantData.betau=upperBetas;
    constantData.I=peakIntensities;
}

void gaussianFitter::setLockedParameters(bool xOffsetLock, bool yOffsetLock, bool lowerALock, bool lowerBLock, bool upperALock, bool upperBLock, bool widthLock, bool intensityLock){
    constantData.X0Lock=xOffsetLock;
    constantData.yzeroLock=yOffsetLock;
    constantData.AlLock=lowerALock;
    constantData.BlLock=lowerBLock;
    constantData.AuLock=upperALock;
    constantData.BuLock=upperBLock;
    constantData.wLock=widthLock;
    constantData.ILock=intensityLock;
}

void gaussianFitter::setInitialValues(double centerOfMass, double yOffset, double lowerA, double lowerB, double upperA, double upperB, double width, double intensity){
    if(initialValues=NULL){
        initialValues=new double[8];
    }
    else{
        delete[] initialValues;
        initialValues=new double[8];
    }
    initialValues[0]=centerOfMass;
    initialValues[1]=yOffset;
    initialValues[2]=lowerA;
    initialValues[3]=lowerB;
    initialValues[4]=upperA;
    initialValues[5]=upperB;
    initialValues[6]=width;
    initialValues[7]=intensity;
}

void gaussianFitter::setFitterControlParameters(int numberOfIterations, double absoluteError, double relativeError){
    iterations=numberOfIterations;
    epsabs=absoluteError;
    epsrel=relativeError;
}

void gaussianFitter::setSolver(){
    //the number of variables in a gaussian multipeak fitting is
    //8. This does not depend on the number of peaks as all the fit parameters apply for each peak
    // and some of the constants, namely Ic the alphas and the betas, differentiate the peaks
    p=8;
    n =constantData.n;                          //number of datapoints
    covar = gsl_matrix_alloc (p, p);                //setup a covariant matrix for the results according to the number of variables
    //setup the fitfunction
    f.f = &gaussianFitFunctions_callGaussian_f;     //set the function that calculates the gaussian profile
    f.df = &gaussianFitFunctions_callGaussian_fd;   //set the function which calculates the derivatives
    f.fdf = &gaussianFitFunctions_callGaussian_fdf; //set a function to call the gaussian function + its derivatives
    f.n = n;                                        //set the number of datapoints to the fitter
    f.p = p;                                        //set the number of parameters to be fitted
    f.params = &constantData;                       //set the struct constantdata as the container for the constants and other data

    //setup the solver
    x = gsl_vector_view_array (initialValues, p);   //set the initial values and the number of peaks into and GSL type array
    T = gsl_multifit_fdfsolver_lmsder;              //set the solver alogrithm to Levenberg-Marquardt
    s = gsl_multifit_fdfsolver_alloc (T, n, p);     //allocate the solver according to the parameters
    gsl_multifit_fdfsolver_set (s, &f, &x.vector);  //set the solver allocation, the functions, and the initial values to the solver

}

void gaussianFitter::iterateSolver(){
    iter=0;
    returnSolverState(iter, s);
    do{
        iter++;
        status = gsl_multifit_fdfsolver_iterate (s);

        if (status){
            break;
        }
        status = gsl_multifit_test_delta (s->dx, s->x,epsabs, epsrel);

    }
    while (status == GSL_CONTINUE &&iter < iterations);

    gsl_multifit_covar (s->J, 0, covar);
    returnSolverState(iter, s);


}

void gaussianFitter::returnSolverState(size_t iter, gsl_multifit_fdfsolver *s){
#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))

    {
        //calculate reduced chi square parameters
        double chi = gsl_blas_dnrm2(s->f);          //chi
        double dof = constantData.n - 8;                         //degrees of freedom
        double c = GSL_MAX_DBL(1, chi / sqrt(dof)); //Scaling factor sqrt(reduced-chi-square) for the erros.
        const char *fStatus=gsl_strerror (status);
        std::ostringstream iterationsString;
        iterationsString<<iter;
        fitterStatus["Iteratations"]=iterationsString.str();
        fitterStatus["errorStatus"]=fStatus;
        fitResults["reducedChiSquare"]=pow(chi, 2.0) / dof;
        fitResults["centerOfMass"]=FIT(0);
        fitResults["centerOfMassError"]=c*ERR(0);
        fitResults["yOffset"]=FIT(1);
        fitResults["yOffSetError"]=c*ERR(1);
        fitResults["lowerA"]=FIT(2);
        fitResults["lowerAError"]=c*ERR(2);
        fitResults["lowerB"]=FIT(3);
        fitResults["lowerBError"]=c*ERR(3);
        fitResults["upperA"]=FIT(4);
        fitResults["upperAError"]=c*ERR(4);
        fitResults["upperB"]=FIT(5);
        fitResults["upperBError"]=c*ERR(5);
        fitResults["width"]=FIT(6);
        fitResults["widthError"]=c*ERR(6);
        fitResults["intensity"]=FIT(7);
        fitResults["intensityError"]=c*ERR(7);

    }
}
void gaussianFitter::clearSolver(){
    gsl_multifit_fdfsolver_free (s);
    gsl_matrix_free (covar);
}

std::vector<double> gaussianFitter::returnProfile(double X0_val, double yOffset_val, double Al_val, double Bl_val, double Au_val, double Bu_val, double w_val, double I_val){
    std::vector<double> gaussianProfile;
    gaussianProfile.reserve(constantData.n);
    double X0=X0_val;
    double yOffset=yOffset_val;
    double Al=Al_val;
    double Bl=Bl_val;
    double Au=Au_val;
    double Bu=Bu_val;
    double w=w_val;
    double IScale=I_val;
    for(int xPoint=0;xPoint<constantData.n;xPoint++){
        double x=constantData.xdata[xPoint];
        double valueAtPointxPoint=0;
        for(int peak=0; peak<constantData.peaks; peak++){
            double alphal=constantData.alphal[peak];
            double betal=constantData.betal[peak];
            double alphau=constantData.alphau[peak];
            double betau=constantData.betau[peak];
            double intensity=constantData.I[peak]*IScale;
            double centerOfGravity=functions.hfsCenterOfGravity(X0,Al,Bl,Au,Bu,alphal, betal, alphau, betau);
            double gaussianPeak=functions.gaussianFunction(x, centerOfGravity, yOffset, w, intensity);
            valueAtPointxPoint=valueAtPointxPoint+gaussianPeak;
        }
        gaussianProfile[xPoint]=valueAtPointxPoint+yOffset;
    }
    return gaussianProfile;
}
